package man;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import business.IBusinessRemote;

public class ClientIoc {

    public static void main(String[] args) throws NamingException {
        System.out.println(getBusiness().hello());
    }

    /**
     * 
     * @return L'EJB session pour manipulation.
     * @throws NamingException Si l'EJB n'est pas trouv� dans l'annuaire JNDI.
     */
    private static IBusinessRemote getBusiness() throws NamingException {
        Properties jndiProperties = new Properties();
        jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
        jndiProperties.put(Context.PROVIDER_URL, "remote+http://localhost:8080");// (1)
        Context ctx = new InitialContext(jndiProperties);

        String appName = "TestIoc";
        String projectName = "TestIocEJB";
        String className = "Hello";
        String remote = IBusinessRemote.class.getName();

        String url = "ejb:" + appName + "/" + projectName + "/" + className + "!" + remote;// (2)
        System.out.println("URL : " + url);
        IBusinessRemote business = (IBusinessRemote) ctx.lookup(url);

        return business;
    }
}
