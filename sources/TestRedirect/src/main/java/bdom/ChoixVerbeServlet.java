package bdom;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ChoixVerbeServlet
 */
@WebServlet("/choixVerbe")
public class ChoixVerbeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String verbe = request.getParameter("verbe");
        if ("GET".equals(verbe)) {
            this.doGet(request, response);
        } else if ("POST".equals(verbe)) {
            this.doPost(request, response);
        } else if ("PUT".equals(verbe)) {
            this.doPut(request, response);
        } else if ("DELETE".equals(verbe)) {
            this.doDelete(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.getWriter().append("Verbe GET appel�");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.getWriter().append("Verbe POST appel�");
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.getWriter().append("Verbe PUT appel�");
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.getWriter().append("Verbe DELETE appel�");
    }
}
