package bdom;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Connexion
 */
@WebServlet("/login")
public class Connexion extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("premierFormulaire.html"); //
        dispatcher.forward(request, response); //
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String mdp = request.getParameter("mdp"); //
        System.out.println(mdp);
        if ("password".equals(mdp)) {
            request.getRequestDispatcher("ok.html").forward(request, response);
        } else {
            request.getRequestDispatcher("ko.html").forward(request, response);
        }
    }

}
