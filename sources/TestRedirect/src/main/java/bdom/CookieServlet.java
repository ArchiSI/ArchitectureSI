package bdom;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CookieServlet
 */
@WebServlet("/cookie")
public class CookieServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();
        String uid = null;
        if (null != cookies) {
            for (Cookie cookie : cookies) {
                if ("id_mon_site".equals(cookie.getName())) {
                    uid = cookie.getValue();
                    response.getWriter().print("<html><body>Encore vous !</body></html>");
                }
            }
        }
        if (null == uid) {
            uid = new java.rmi.server.UID().toString();
            Cookie cookie = new Cookie("id_mon_site", uid);
            response.addCookie(cookie);
            response.getWriter().print("<html><body>Bienvenue !</body></html>");
        }

    }

}
