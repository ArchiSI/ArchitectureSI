package edu.polytech.note.business;

import java.util.List;

import javax.ejb.Remote;

import edu.polytech.note.model.NoteBean;

@Remote
public interface NotesBusinessRemote {

    public List<NoteBean> getNotesList();

    public NoteBean getNote(Integer id);

    public void insertNote(NoteBean note);

    public void updateNote(NoteBean note);

    public Double computeMean(List<NoteBean> notesList);
}
