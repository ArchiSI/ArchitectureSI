package edu.boutique.business;

import edu.boutique.model.CartBean;

public class StoreBusinessImpl implements StoreBusiness {
    private final static Double PEN_PRICE = Double.parseDouble("1.2");
    private final static Double FELT_PRICE = Double.parseDouble("1.8");
    private final static Double RUBBER_PRICE = Double.parseDouble("2");

    @Override
    public CartBean computePrices(CartBean cart) {
        // Application de la RG1 :
        Double cartPrice = cart.getPenNumber() * PEN_PRICE + cart.getFeltNumber() * FELT_PRICE
                + cart.getRubberNumber() * RUBBER_PRICE;
        // Application de la RG2 :
        Double shippingCost = 5D;
        if (cartPrice > 15D) {
            shippingCost = 0D;
        }
        // Application de la RG3 :
        Double totalPrice = cartPrice + shippingCost;

        // Mise � jour du panier :
        cart.setCartPrice(cartPrice);
        cart.setShippingCost(shippingCost);
        cart.setTotalPrice(totalPrice);

        return cart;
    }
}
