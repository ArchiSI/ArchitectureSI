package edu.boutique.business;

import edu.boutique.model.CartBean;

public interface StoreBusiness {
    public CartBean computePrices(CartBean cart);
}