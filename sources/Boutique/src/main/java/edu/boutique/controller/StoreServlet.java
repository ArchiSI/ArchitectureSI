package edu.boutique.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.boutique.business.StoreBusiness;
import edu.boutique.business.StoreBusinessImpl;
import edu.boutique.model.CartBean;

@WebServlet(name = "controleur", urlPatterns = { "*.do", "/controleur.fr" })
public class StoreServlet extends HttpServlet {

    private static final long serialVersionUID = 5100609000254332371L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        CartBean cart = new CartBean();
        request.setAttribute("USER_CART", cart);

        RequestDispatcher dispatcher = request.getRequestDispatcher("store.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String penNumber = request.getParameter("penNb");
        String feltNumber = request.getParameter("feltNb");
        String rubberNumber = request.getParameter("rubberNb");

        CartBean cart = new CartBean();

        cart.setPenNumber(Integer.parseInt(penNumber));
        cart.setFeltNumber(Integer.parseInt(feltNumber));
        cart.setRubberNumber(Integer.parseInt(rubberNumber));

        StoreBusiness business = new StoreBusinessImpl();
        cart = business.computePrices(cart);

        request.setAttribute("USER_CART", cart);

        RequestDispatcher dispatcher = request.getRequestDispatcher("store.jsp");
        dispatcher.forward(request, response);
    }

}
