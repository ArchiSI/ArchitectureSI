<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Liste des produits</title>
</head>
<body>
    <h1>Sélection des produits</h1>

	<form action="e-store.fr" method="post">
		<fieldset>
			<legend>Liste des produits</legend>
			<table>
				<tr>
					<th>Produit</th>
					<th>Prix</th>
					<th></th>
				</tr>
				<c:forEach var="product" items="${requestScope.USER_CART.products}">
					<tr>
						<td><c:out value="${product.key.name}" /></td>
						<td><c:out value="${product.key.price}" /></td>
						<td><input type="number"
							name="nb_of_product_${product.key.id}" value="${product.value}" />
					</tr>
				</c:forEach>
			</table>
		</fieldset>
		<input type="submit" value="Valider" />
	</form>

	<fieldset>
        <legend>Récapitulatif du panier</legend>
        <table>
            <tr>
                <td>Panier :</td>
                <td>${requestScope.USER_CART.cartPrice}</td>
            </tr>
            <tr>
                <td>Frais livraison :</td>
                <td>${requestScope.USER_CART.shippingCost}</td>
            </tr>
            <tr>
                <td>Prix total :</td>
                <td>${requestScope.USER_CART.totalPrice}</td>
            </tr>
        </table>
    </fieldset>
</body>
</html>
