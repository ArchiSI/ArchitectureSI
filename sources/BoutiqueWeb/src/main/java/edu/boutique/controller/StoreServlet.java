package edu.boutique.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.boutique.ejb.BoutiqueLocal;
import edu.boutique.model.CartBean;
import edu.boutique.model.ProductBean;

@WebServlet(name = "storeServlet", urlPatterns = { "/e-store.fr" })
public class StoreServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@EJB
	private BoutiqueLocal metier;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// R�cup�ration de la liste des produits en BDD.
		List<ProductBean> products = metier.getProducts();

		// Cr�ation du panier, avec la liste des produits s�lectionnables
		CartBean cart = new CartBean();
		cart.initProducts(products);

		// On place le panier dans la requ�te pour pouvoir l'utiliser dans la JSP.
		request.setAttribute("USER_CART", cart);

		// On redirige vers la bonne vue : store.jsp
		RequestDispatcher dispatcher = request.getRequestDispatcher("store.jsp");
		dispatcher.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// R�cup�ration de la liste des produits en BDD.
		List<ProductBean> products = metier.getProducts();

		// Cr�ation du panier, avec la liste des produits s�lectionnables
		CartBean cart = new CartBean();
		cart.initProducts(products);

		// On r�cup�re tous les param�tres de la requ�te.
		Map<String, String[]> parameters = request.getParameterMap();
		for (String parameter : parameters.keySet()) {
			// On parcourt ces param�tres, en r�cup�rant tout ceux commen�ant par
			// "nb_of_product_" :
			if (parameter.startsWith("nb_of_product_")) {
				// On r�cup�re le nombre de s�lection pour l'objet courant.
				Integer numberOfProduct = Integer.parseInt(parameters.get(parameter)[0]);
				// On r�cup�re l'id du produit ...
				Integer id = Integer.parseInt(parameter.replaceAll("nb_of_product_", ""));
				// ... � partir duquel on cr�� un objet ProductBean, pour le retrouver dans la
				// Map :
				ProductBean product = new ProductBean();
				product.setId(id);
				// On ajoute ce produit dans le panier :
				cart.getProducts().replace(product, numberOfProduct);
			}
		}

		// Calcul de co�ts du panier :
		cart = metier.computePrices(cart);

		// On place le panier dans la requ�te pour pouvoir l'utiliser dans la JSP.
		request.setAttribute("USER_CART", cart);

		// On redirige vers la bonne vue : store.jsp
		RequestDispatcher dispatcher = request.getRequestDispatcher("store.jsp");
		dispatcher.forward(request, response);
	}
}
