package edu.polytech.note.client;

import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import edu.polytech.note.business.NotesBusinessImpl;
import edu.polytech.note.business.NotesBusinessRemote;
import edu.polytech.note.model.NoteBean;

public class ClientNotes {

    public static void main(String[] args) throws NamingException {
        NotesBusinessRemote business = getBusiness();

        List<NoteBean> notesList = business.getNotesList();
        System.out.println("Liste des notes :");
        for (NoteBean note : notesList) {
            System.out.println(note.getFirstName() + " " + note.getName() + " : " + note.getNote());
        }
        
        NoteBean note = new NoteBean();
        note.setFirstName("Prof.");
        note.setName("Tournesol");
        note.setNote(12.5D);
        business.insertNote(note);
        

        notesList = business.getNotesList();
        System.out.println("Liste des notes :");
        for (NoteBean note2 : notesList) {
            System.out.println(note2.getFirstName() + " " + note2.getName() + " : " + note2.getNote());
        }
    }

    /**
     * 
     * @return L'EJB session pour manipulation.
     * @throws NamingException Si l'EJB n'est pas trouv� dans l'annuaire JNDI.
     */
    private static NotesBusinessRemote getBusiness() throws NamingException {
        Properties jndiProperties = new Properties();
        jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
        jndiProperties.put(Context.PROVIDER_URL, "remote+http://localhost:8080");
        Context ctx = new InitialContext(jndiProperties);

        String appName = "GestionNotesJPAEAR/";
        String projectName = "GestionNotesJPAEJB/";
        String className = NotesBusinessImpl.class.getSimpleName();
        String remote = NotesBusinessRemote.class.getName();

        String url = "ejb:" + appName + projectName + className + "!" + remote;
        System.out.println("EJB appel� avec : " + url);

        NotesBusinessRemote business = (NotesBusinessRemote) ctx.lookup(url);

        return business;
    }
}
