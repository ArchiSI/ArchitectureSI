package edu.polytech.note.client;

import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import edu.polytech.note.business.NotesBusinessRemote;
import edu.polytech.note.model.NoteBean;

public class NoteClient {

    public static void main(String[] args) throws NamingException {
        // Cette objet business va nous permettre d'appeler les services m�tiers
        // disponibles,
        // c'est-�-dire ceux d�finis dans l'interface distante : NotesBusinessRemote.
        NotesBusinessRemote business = getBusiness();

        // 1er �tudiant :
        NoteBean bean1 = new NoteBean();
        bean1.setName("Haddock");
        bean1.setFirstName("Capitaine");
        bean1.setNote(10.5D);
        // Appel du service m�tier d'ajout d'une note :
        business.insertNote(bean1);

        // 2�me �tudiant :
        NoteBean bean2 = new NoteBean();
        bean2.setName("Tournesol");
        bean2.setFirstName("Professeur");
        bean2.setNote(20D);
        // Appel du service m�tier d'ajout d'une note :
        business.insertNote(bean2);

        // On affiche la liste des notes pr�sentes en BDD.
        // Appel du service m�tier de r�cup�ration de toutes les notes :
        List<NoteBean> notesList = business.getNotesList();
        System.out.println("Notes : ");
        for (NoteBean bean : notesList) {
            System.out.println(bean.getFirstName() + " " + bean.getName() + " : " + bean.getNote());
        }
        System.out.println("La moyenne est : " + business.computeMean(notesList));
    }

    /**
     * 
     * @return L'EJB session pour manipulation.
     * @throws NamingException Si l'EJB n'est pas trouv� dans l'annuaire JNDI.
     */
    private static NotesBusinessRemote getBusiness() throws NamingException {
        Properties jndiProperties = new Properties();
        jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
        jndiProperties.put(Context.PROVIDER_URL, "remote+http://localhost:8080");// (1)
        Context ctx = new InitialContext(jndiProperties);

        String appName = "GestionNotesJPAEAR";
        String projectName = "GestionNotesJPAEJB";
        String className = "NotesBusinessImpl";
        String remote = NotesBusinessRemote.class.getName();

        String url = "ejb:" + appName + "/" + projectName + "/" + className + "!" + remote;// (2)

        NotesBusinessRemote business = (NotesBusinessRemote) ctx.lookup(url);

        return business;
    }
}