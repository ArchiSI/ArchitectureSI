package edu.polytech.note.controller;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.polytech.note.business.NotesBusinessLocal;
import edu.polytech.note.model.NoteBean;

/**
 * Servlet implementation class ListeNoteServlet
 */
@WebServlet("/notesList")
public class NotesListServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @EJB
    private NotesBusinessLocal business;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<NoteBean> notesList = business.getNotesList();
        request.setAttribute("NOTES_LIST", notesList);
        final Double mean = business.computeMean(notesList);
        request.setAttribute("MEAN", mean);
        request.getRequestDispatcher("notesList.jsp").forward(request, response);
    }
}
