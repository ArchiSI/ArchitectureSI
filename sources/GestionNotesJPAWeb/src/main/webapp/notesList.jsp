<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Liste des notes</h1>
	<table>
		<tr>
			<th>Matricule</th>
			<th>Nom</th>
			<th>Pr�nom</th>
			<th>Note</th>
		</tr>
		<c:forEach var="note" items="${requestScope.NOTES_LIST}">
			<tr>
				<td>${note.id}</td>
				<td>${note.name}</td>
				<td>${note.firstName}</td>
				<td>${note.note}</td>
			</tr>
		</c:forEach>
	</table>
	<form action="addNote" method="get">
		<input type="submit" value="Ajouter une note"/>
	</form>
	<c:if test="${not empty requestScope.MEAN}">
		Moyenne : ${requestScope.MEAN}
	</c:if>
</body>
</html>