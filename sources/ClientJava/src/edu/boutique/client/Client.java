package edu.boutique.client;

import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import edu.boutique.ejb.BoutiqueRemote;
import edu.boutique.model.CartBean;
import edu.boutique.model.ProductBean;

public class Client {

	public static void main(String[] args) throws NamingException {
		BoutiqueRemote business = getBusiness();

		// Ajout du stylo en BDD.
		ProductBean stylo = new ProductBean();
		stylo.setName("Stylo");
		stylo.setPrice(1.2D);
		stylo = business.createProduct(stylo);

		// Ajout du feutre en BDD.
		ProductBean feutre = new ProductBean();
		feutre.setName("Feutre");
		feutre.setPrice(1.8D);
		feutre = business.createProduct(feutre);

		// Liste des produits :
		System.out.println("Liste des produits :");
		List<ProductBean> products = business.getProducts();
		for (ProductBean product : products) {
			System.out.println(product.getName() + " co�te " + product.getPrice());
		}

		// On modifie un produit :
		business.modifyProduct(1, "Stylo 4 couleurs", 3.2D);

		// Liste des produits :
		System.out.println("Liste des produits :");
		products = business.getProducts();
		for (ProductBean product : products) {
			System.out.println(product.getName() + " co�te " + product.getPrice());
		}

		// On simule un panier.
		CartBean cart = new CartBean();
		cart.getProducts().put(stylo, 2);
		cart.getProducts().put(feutre, 4);
		cart = business.computePrices(cart);
		System.out.println("Panier : " + cart.getCartPrice());
		System.out.println("Frais de livraison : " + cart.getShippingCost());
		System.out.println("Total : " + cart.getTotalPrice());
	}

	/**
	 * 
	 * @return L'EJB session pour manipulation.
	 * @throws NamingException Si l'EJB n'est pas trouv� dans l'annuaire JNDI.
	 */
	private static BoutiqueRemote getBusiness() throws NamingException {
		Properties jndiProperties = new Properties();
		jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
		jndiProperties.put(Context.PROVIDER_URL, "remote+http://localhost:8080");
		Context ctx = new InitialContext(jndiProperties);

		String appName = "BoutiqueEAR/";
		String projectName = "BoutiqueEJB/";
		String className = "BoutiqueEJBSession!";
		String remote = BoutiqueRemote.class.getName();

		String url = "ejb:" + appName + projectName + className + remote;

		BoutiqueRemote business = (BoutiqueRemote) ctx.lookup(url);

		return business;
	}
}