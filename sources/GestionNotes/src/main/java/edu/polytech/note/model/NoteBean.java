package edu.polytech.note.model;

import java.io.Serializable;

public class NoteBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;
    private String firstName;
    private Double note;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Double getNote() {
        return this.note;
    }

    public void setNote(Double note) {
        this.note = note;
    }
}
