package edu.polytech.note.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import edu.polytech.note.model.NoteBean;

public class NotesDAOImpl implements NotesDAO {

    public List<NoteBean> getNotesList() {
        Statement st = null;
        ResultSet rs = null;
        Connection connection = null;
        List<NoteBean> notesList = null;

        try {
            // Lecture de la table NOTE :
            final String sql = "SELECT * from note;";
            connection = DBConnection.createDBConnexion();
            st = connection.createStatement();
            rs = st.executeQuery(sql);
            notesList = mapResultSetToList(rs);
        } catch (Exception e) {
            // S'il y a eu un probl�me, on le fait remonter.
            throw new RuntimeException(e);
        } finally {
            try {
                // Dans tous les cas, on ferme tout ce qui doit l'�tre.
                st.close();
                connection.close();
            } catch (Exception e) {
                // S'il y a eu un probl�me, on le fait remonter.
                throw new RuntimeException(e);
            }
        }
        return notesList;
    }

    @Override
    public NoteBean getNote(Integer id) {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection connection = null;
        List<NoteBean> notesList = null;

        try {
            // Lecture de la table NOTE :
            final String sql = "SELECT * from note where id = ?;";
            connection = DBConnection.createDBConnexion();
            st = connection.prepareStatement(sql);
            st.setInt(1, id);
            rs = st.executeQuery();
            notesList = mapResultSetToList(rs);
        } catch (Exception e) {
            // S'il y a eu un probl�me, on le fait remonter.
            throw new RuntimeException(e);
        } finally {
            try {
                // Dans tous les cas, on ferme tout ce qui doit l'�tre.
                st.close();
                connection.close();
            } catch (Exception e) {
                // S'il y a eu un probl�me, on le fait remonter.
                throw new RuntimeException(e);
            }
        }
        if (notesList.size() != 1) {
            // Si on n'a pas r�cup�r� exactement 1 seul enregistrement, c'est qu'il y a eu
            // un probl�me.
            throw new RuntimeException("Probl�me � la r�cup�ration de l'enregistrement " + id);
        }
        return notesList.get(0);
    }

    public void insertNote(NoteBean note) {
        PreparedStatement st = null;
        Connection connection = null;

        try {
            connection = DBConnection.createDBConnexion();
            final String sqlInsert = "INSERT INTO NOTE (nom, prenom, note) values (?, ?, ?);";
            st = connection.prepareStatement(sqlInsert);
            st.setString(1, note.getName());
            st.setString(2, note.getFirstName());
            st.setDouble(3, note.getNote());
            st.executeUpdate();
        } catch (Exception e) {
            // S'il y a eu un probl�me, on le fait remonter.
            throw new RuntimeException(e);
        } finally {
            try {
                // Dans tous les cas, on ferme tout ce qui doit l'�tre.
                st.close();
                connection.close();
            } catch (Exception e) {
                // S'il y a eu un probl�me, on le fait remonter.
                throw new RuntimeException(e);
            }
        }
    }

    private final List<NoteBean> mapResultSetToList(final ResultSet rs) throws SQLException {
        List<NoteBean> notesList = new ArrayList<NoteBean>();
        while (rs.next()) {
            // Pour chaque ligne de la table,
            // on instancie un nouveau NotesBean.
            final NoteBean noteBean = new NoteBean();
            noteBean.setId(rs.getInt("id"));
            noteBean.setName(rs.getString("nom"));
            noteBean.setFirstName(rs.getString("prenom"));
            noteBean.setNote(rs.getDouble("note"));
            // On ajoute ce bean � la liste des r�sultats.
            notesList.add(noteBean);
        }
        return notesList;
    }

    @Override
    public void updateNote(NoteBean note) {
        // TODO Auto-generated method stub

    }
}
