package edu.polytech.note.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private static final String URL = "jdbc:mysql://localhost:3306/gestion_notes";

    private static final String LOGIN = "root";

    private static final String PASSWORD = "admin";

    /**
     * 
     * @return L'object {@link Connection} permettant de se connecter � la BDD.
     * @throws ClassNotFoundException S'il y a un probl�me au chargement du pilote
     *                                com.mysql.cj.jdbc.Driver.
     * @throws SQLException           S'il y a un probl�me � l'�tablissement de la
     *                                connexion.
     */
    public static Connection createDBConnexion() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        final Connection con = DriverManager.getConnection(URL, LOGIN, PASSWORD);
        return con;
    }
}
