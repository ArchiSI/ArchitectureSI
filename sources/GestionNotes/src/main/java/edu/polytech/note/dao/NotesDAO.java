package edu.polytech.note.dao;

import java.util.List;

import edu.polytech.note.model.NoteBean;

public interface NotesDAO {

    public List<NoteBean> getNotesList();

    public NoteBean getNote(Integer id);

    public void insertNote(NoteBean note);

    public void updateNote(NoteBean note);

}
