<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<title>Liste des notes</title>
</head>
<body>
	<h1>Liste des notes</h1>
	<table>
		<tr>
			<th>Nom</th>
			<th>Pr�nom</th>
			<th>Note</th>
		</tr>
		<c:forEach var="note" items="${requestScope.NOTES_LIST}">
			<tr>
				<td>${note.name}</td>
				<td>${note.firstName}</td>
				<td>${note.note}</td>
			</tr>
		</c:forEach>
	</table>
	<c:if test="${not empty requestScope.MEAN}">
		La moyenne est de ${requestScope.MEAN}
	</c:if>
	<form action="addNote" method="get">
		<input type="submit" value="Ajouter une note"/>
	</form>
</body>
</html>