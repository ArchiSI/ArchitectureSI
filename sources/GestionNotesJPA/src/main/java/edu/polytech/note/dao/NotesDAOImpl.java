package edu.polytech.note.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import edu.polytech.note.model.NoteBean;

public class NotesDAOImpl implements NotesDAO {

    private EntityManager em;

    public NotesDAOImpl() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestionNotesJPA");
        this.em = emf.createEntityManager();
    }

    @Override
    public List<NoteBean> getNotesList() {
        Query requete = em.createQuery("select n from NoteBean n");
        return requete.getResultList();
    }

    @Override
    public NoteBean getNote(Integer id) {
        NoteBean note = (NoteBean) em.find(NoteBean.class, id);
        if (null == note) {
            throw new RuntimeException("Cet enregistrement n'existe pas en base : " + id);
        }
        return note;
    }

    @Override
    public void insertNote(NoteBean note) {
        em.getTransaction().begin();
        em.persist(note);
        em.getTransaction().commit();
    }

    @Override
    public void updateNote(NoteBean note) {
        em.getTransaction().begin();
        em.merge(note);
        em.getTransaction().commit();
    }
}
