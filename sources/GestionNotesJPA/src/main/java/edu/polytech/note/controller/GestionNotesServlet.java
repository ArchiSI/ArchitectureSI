package edu.polytech.note.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.polytech.note.business.NotesBusiness;
import edu.polytech.note.business.NotesBusinessImpl;
import edu.polytech.note.model.NoteBean;

public class GestionNotesServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected NotesBusiness business;

    @Override
    public void init() throws ServletException {
        this.business = new NotesBusinessImpl();
    }

    protected void forwardToNotesList(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<NoteBean> notesList = business.getNotesList();
        request.setAttribute("NOTES_LIST", notesList);
        final Double mean = business.computeMean(notesList);
        request.setAttribute("MEAN", mean);
        request.getRequestDispatcher("notesList.jsp").forward(request, response);
    }
}
