package edu.polytech.note.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.polytech.note.model.NoteBean;

/**
 * Servlet implementation class AddNote
 */
@WebServlet("/addNote")
public class AddNoteServlet extends GestionNotesServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("addNote.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // R�cup�ration des donn�es saisies par l'utilisateur,
        // et enregistrement de celles-ci dans le mod�le (c'est-�-dire le bean).
        NoteBean noteBean = new NoteBean();
        String name = request.getParameter("name");
        noteBean.setName(name);
        String firstName = request.getParameter("firstName");
        noteBean.setFirstName(firstName);
        String note = request.getParameter("note");
        noteBean.setNote(Double.parseDouble(note));

        // Enregistrement en BDD :
        business.insertNote(noteBean);

        // Redirection de la requ�te.
        forwardToNotesList(request, response);
    }
}
