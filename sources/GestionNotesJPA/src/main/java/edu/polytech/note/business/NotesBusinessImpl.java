package edu.polytech.note.business;

import java.util.List;

import edu.polytech.note.dao.NotesDAO;
import edu.polytech.note.dao.NotesDAOImpl;
import edu.polytech.note.model.NoteBean;

public class NotesBusinessImpl implements NotesBusiness {

    private NotesDAO dao;

    public NotesBusinessImpl() {
        this.dao = new NotesDAOImpl();
    }

    @Override
    public List<NoteBean> getNotesList() {
        return dao.getNotesList();
    }

    @Override
    public NoteBean getNote(Integer id) {
        return dao.getNote(id);
    }

    @Override
    public void insertNote(NoteBean note) {
        dao.insertNote(note);
    }

    @Override
    public void updateNote(NoteBean note) {
        dao.updateNote(note);
    }

    @Override
    public Double computeMean(List<NoteBean> notesList) {
        if (null == notesList || notesList.isEmpty()) {
            return null;
        } else {
            Double mean = 0D;
            for (NoteBean note : notesList) {
                mean += note.getNote();
            }
            return mean / notesList.size();
        }
    }
}
