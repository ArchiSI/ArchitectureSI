package edu.polytech.note.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.polytech.note.model.NoteBean;

/**
 * Servlet implementation class ModifyNoteServlet
 */
@WebServlet("/modifyNote")
public class ModifyNoteServlet extends GestionNotesServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer id = Integer.parseInt(request.getParameter("id"));
        NoteBean bean = business.getNote(id);
        request.setAttribute("BEAN", bean);
        request.getRequestDispatcher("modifyNote.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String firstName = request.getParameter("firstName");
        Double note = Double.parseDouble(request.getParameter("note"));

        NoteBean bean = new NoteBean();
        bean.setId(id);
        bean.setName(name);
        bean.setFirstName(firstName);
        bean.setNote(note);
        business.updateNote(bean);

        // Redirection de la requ�te.
        forwardToNotesList(request, response);
    }

}
