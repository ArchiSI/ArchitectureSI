package edu.boutique.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<ProductBean, Integer> products = new HashMap<ProductBean, Integer>();
	private Double cartPrice = 0D;
	private Double shippingCost = 0D;
	private Double totalPrice = 0D;

	public Map<ProductBean, Integer> getProducts() {
		return products;
	}

	public void setProducts(Map<ProductBean, Integer> products) {
		this.products = products;
	}

	public void initProducts(List<ProductBean> productList) {
		this.products = new HashMap<ProductBean, Integer>();
		for (ProductBean product : productList) {
			this.products.put(product, 0);
		}
	}

	public Double getCartPrice() {
		return cartPrice;
	}

	public void setCartPrice(Double cartPrice) {
		this.cartPrice = cartPrice;
	}

	public Double getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(Double shippingCost) {
		this.shippingCost = shippingCost;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
}
