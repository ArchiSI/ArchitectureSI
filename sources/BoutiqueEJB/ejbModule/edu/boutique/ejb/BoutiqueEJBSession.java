package edu.boutique.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.boutique.model.CartBean;
import edu.boutique.model.ProductBean;

@Stateless
public class BoutiqueEJBSession implements BoutiqueLocal, BoutiqueRemote {

	@PersistenceContext(unitName = "BoutiqueEJB")
	private EntityManager em;

	@Override
	public ProductBean createProduct(ProductBean product) {
		em.persist(product);
		return product;
	}

	@Override
	public ProductBean getProduct(Integer id) {
		ProductBean product = em.find(ProductBean.class, id);
		if (null == product) {
			throw new RuntimeException("Ce produit n'existe pas en base.");
		}
		return product;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductBean> getProducts() {
		Query requete = em.createQuery("select p from ProductBean p");
		return requete.getResultList();
	}

	@Override
	public ProductBean modifyProduct(Integer id, String name, Double price) {
		ProductBean product = getProduct(id);
		product.setName(name);
		product.setPrice(price);
		return product;
	}

	@Override
	public CartBean computePrices(CartBean cart) {
		// Calcul du prix du panier.
		Double cartPrice = 0D;
		for (ProductBean product : cart.getProducts().keySet()) {
			cartPrice += product.getPrice() * cart.getProducts().get(product);
		}

		// Calcul des frais de livraison.
		Double shippingCost = 5D;
		if (cartPrice >= 15D) {
			shippingCost = 0D;
		}

		// Calcul du prix total.
		Double totalPrice = cartPrice + shippingCost;

		// Mise � jour du panier.
		cart.setCartPrice(cartPrice);
		cart.setShippingCost(shippingCost);
		cart.setTotalPrice(totalPrice);
		return cart;
	}

}
