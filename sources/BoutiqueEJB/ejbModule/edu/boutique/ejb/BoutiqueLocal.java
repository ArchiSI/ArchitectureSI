package edu.boutique.ejb;

import java.util.List;

import javax.ejb.Local;

import edu.boutique.model.CartBean;
import edu.boutique.model.ProductBean;

@Local
public interface BoutiqueLocal {
	// Permet de cr�er un produit en BDD.
	public ProductBean createProduct(ProductBean product);

	// Permet de r�cup�rer un produit avec sont id.
	public ProductBean getProduct(Integer id);

	// Permet de r�cup�rer la liste de tous les produits en BDD.
	public List<ProductBean> getProducts();

	// Permet de modifier un produit donn� (son nom et/ou son prix).
	public ProductBean modifyProduct(Integer id, String name, Double price);

	// Permet de calculer les dif�rents co�ts sur un panier donn�.
	public CartBean computePrices(CartBean cart);
}
