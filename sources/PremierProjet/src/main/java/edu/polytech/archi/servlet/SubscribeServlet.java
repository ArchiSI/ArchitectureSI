package edu.polytech.archi.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.polytech.archi.model.PersonBean;

public class SubscribeServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PersonBean person = new PersonBean();
        request.setAttribute("PERSON", person);
        request.getRequestDispatcher("inscription.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PersonBean person = new PersonBean();

        String name = request.getParameter("le_nom");
        String firstName = request.getParameter("le_prenom");
        Integer age = Integer.parseInt(request.getParameter("l_age"));
        person.setName(name);
        person.setFirstName(firstName);
        person.setAge(age);

        request.setAttribute("PERSON", person);
        request.getRequestDispatcher("inscription.jsp").forward(request, response);
    }
}
