package edu.polytech.archi.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ListeServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("LISTE", new ArrayList<String>());
		request.getRequestDispatcher("afficheListe.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Integer taille = Integer.parseInt(request.getParameter("taille"));
		List<String> liste = new ArrayList<String>();
		for (int i = 0; i < taille; i++) {
			liste.add("Bonjour !");
		}
		request.setAttribute("LISTE", liste);
		request.getRequestDispatcher("afficheListe.jsp").forward(request, response);
	}
}
