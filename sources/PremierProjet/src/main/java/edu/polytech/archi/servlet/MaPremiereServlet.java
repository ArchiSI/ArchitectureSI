package edu.polytech.archi.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MaPremiereServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void init() throws ServletException {
		super.init();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*
		 * response.setContentType("text/html"); PrintWriter out = response.getWriter();
		 * out.println("<html><head></head><body>Hello World !</body></html>");
		 */
		RequestDispatcher dispatcher = request.getRequestDispatcher("premierFormulaire.html");
		dispatcher.forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		// Récupération de la valeur du champ dont l'attribut "name" est "prenom".
		String prenom = request.getParameter("prenom");
		out.println("<html><head></head><body>Salut " + prenom + " !</body></html>");
	}

	public void destroy() {
		super.destroy();
	}
}