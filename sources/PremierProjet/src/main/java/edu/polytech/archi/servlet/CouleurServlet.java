package edu.polytech.archi.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CouleurServlet
 */
@WebServlet("/couleur")
public class CouleurServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("COULEUR", "#fff");
		request.getRequestDispatcher("couleur.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String couleur = request.getParameter("couleur");
		System.out.println(couleur);
		request.setAttribute("COULEUR", couleur);
		request.getRequestDispatcher("couleur.jsp").forward(request, response);
	}

}
