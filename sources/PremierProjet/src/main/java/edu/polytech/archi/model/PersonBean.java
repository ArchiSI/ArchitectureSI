package edu.polytech.archi.model;

import java.io.Serializable;

public class PersonBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String firstName;
    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}