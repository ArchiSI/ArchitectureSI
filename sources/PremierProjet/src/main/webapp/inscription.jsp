<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Formulaire d'inscription</title>
</head>
<body>
    <form action="subscribe" method="post">
        <table>
            <tr><td>Nom :</td><td><input type="text" name="le_nom" value="${requestScope.PERSON.name}"/></td></tr>
            <tr><td>Pr�nom :</td><td><input type="text" name="le_prenom" value="${requestScope.PERSON.firstName}"/></td></tr>
            <tr><td>Age :</td><td><input type="number" name="l_age" value="${requestScope.PERSON.age}"/></td></tr>
        </table>
        <input type="submit" value="S'inscrire"/>
    </form>
    <c:if test="${not empty requestScope.PERSON.name}">
        Bienvenue ${requestScope.PERSON.firstName} ${requestScope.PERSON.name}. Vous avez ${requestScope.PERSON.age} ans.
    </c:if>
</body>
</html>