package dao;

import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

@Alternative
@Stateless
public class DaoImpl1 implements IDao {

    @Override
    public String hello() {
        return "Dans dao Impl1";
    }

}
