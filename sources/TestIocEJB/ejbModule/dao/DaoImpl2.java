package dao;

import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

@Alternative
@Stateless
public class DaoImpl2 implements IDao {

    @Override
    public String hello() {
        return "Dans dao Impl2";
    }

}
