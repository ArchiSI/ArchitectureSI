package business;

import javax.ejb.Remote;

@Remote
public interface IBusinessRemote {

    public String hello();
}
