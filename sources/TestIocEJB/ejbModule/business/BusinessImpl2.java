package business;

import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import dao.IDao;

@Alternative
public class BusinessImpl2 implements IBusiness, IBusinessRemote {

    @Inject
    private IDao dao;

    @Override
    public String hello() {
        return "Dans EJB2 - " + dao.hello();
    }
}
