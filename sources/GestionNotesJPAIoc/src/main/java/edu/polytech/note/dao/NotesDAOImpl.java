package edu.polytech.note.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.polytech.note.model.NoteBean;

@Stateless
public class NotesDAOImpl implements NotesDAO {

    @PersistenceContext(unitName = "GestionNotesJPA")
    private EntityManager em;

    @Override
    public List<NoteBean> getNotesList() {
        Query requete = em.createQuery("select n from NoteBean n");
        return requete.getResultList();
    }

    @Override
    public NoteBean getNote(Integer id) {
        NoteBean note = (NoteBean) em.find(NoteBean.class, id);
        if (null == note) {
            throw new RuntimeException("Cet enregistrement n'existe pas en base : " + id);
        }
        return note;
    }

    @Override
    public void insertNote(NoteBean note) {
        em.persist(note);
    }

    @Override
    public void updateNote(NoteBean note) {
        NoteBean noteBDD = getNote(note.getId());
        noteBDD.setName(note.getName());
        noteBDD.setFirstName(note.getFirstName());
        noteBDD.setNote(note.getNote());
    }
}
