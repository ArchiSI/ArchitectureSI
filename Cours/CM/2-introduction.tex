\section{Introduction}

\subsection{Système d'information}

\begin{frame}
	\frametitlepagenumber{Système d'information}
	\textbf{Wikipedia} :\\
	\textit{Le système d'information (SI) est un ensemble organisé de ressources qui permet de collecter, stocker, traiter et distribuer de l'information, en général grâce à un réseau d'ordinateurs.\\
	Il s'agit d'un système socio-technique composé de deux sous-systèmes, l'un social et l'autre technique.}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Système d'information}
	\textbf{Sous-système technique :} 
	\begin{itemize}
		\item \uncover<2->{C'est un système complexe qui gère :}
			\begin{itemize}
				\item \uncover<3->{Grande quantité de données}
				\item \uncover<4->{Stockage de ces données}
				\item \uncover<5->{Manipulation de ces données}
				\item \uncover<6->{Visualisation de ces données}
			\end{itemize}
		\item \uncover<7->{et qui est construit à partir des processus métier}
			\begin{itemize}
				\item \uncover<8->{Il y a des points communs entre tous ces systèmes (architecture 3-tiers, paradigme MVC) ...}
				\item \uncover<9->{... mais ils sont tous différents.}
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Système d'information}
	\textbf{Sous-système social :} 
	\begin{itemize}
		\item \uncover<2->{C'est un système qui gère plusieurs rôles :}
			\begin{itemize}
				\item \uncover<3->{Consultation des données}
				\item \uncover<4->{Modification des données}
				\item \uncover<5->{Suppression des données}
				\item \uncover<6->{Droit d'accès à certaines données sensibles}
			\end{itemize}
		\item \uncover<7->{et un utilisateur peut avoir un ou plusieurs rôles}
			\begin{itemize}
				\item \uncover<8->{Par exemple, pour Airbnb :}
					\begin{itemize}
						\item \uncover<9->{autorisation de modifier les données du logement qu'on met en location, mais consultation uniquement sur les autres}
						\item \uncover<10->{autorisation de modifier les données de tous les logements à Tours, mais pas ailleurs}
					\end{itemize}
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Caractéristiques d'un système d'information}
	\begin{itemize}
		\item \uncover<1->{Données persistantes}
		\item \uncover<2->{Gros volumes de données}
		\item \uncover<3->{Accès concurrent aux données}
		\item \uncover<4->{Un grand nombre d'écrans pour l'interface utilisateur}
		\item \uncover<5->{Intégration avec d'autres applications d'entreprise}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Critères de qualité d'un système d'information}
	\begin{itemize}
		\item \uncover<1->{Pérennité}
		\item \uncover<2->{Maintenabilité}
		\item \uncover<3->{Facilité d'installation et d'utilisation}
		\item \uncover<4->{Économique par rapport à d'autres solutions}
	\end{itemize}
\end{frame}

\subsection{Architecture d'un système d'information}

\begin{frame}
	\frametitlepagenumber{Définition}
	\begin{itemize}[<+->]
		\item \textbf{Wikipedia} :\\
			\textit{Contrairement aux spécifications produites par l'analyse fonctionnelle, le modèle d'architecture, produit lors de la phase de conception, ne décrit pas ce que doit réaliser un système informatique mais plutôt comment il doit être conçu de manière à répondre aux spécifications. L'analyse décrit le \og quoi faire \fg{} alors que l'architecture décrit le \og comment le faire \fg{}.}
		\item Il n'y a pas un consensus sur la définition. Deux éléments communs :
		\begin{itemize}
			\item Répartition du système en différentes parties.
				On parle de \og tier \fg{}, pour \og étage \fg{} en anglais.
			\item Décisions prises en amont de la conception, difficiles à changer après.
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Architecture en couche}
	Une application est toujours découpée en trois couches principales :
	\begin{center}
		\begin{overlayarea}{0.7\textwidth}{0.37\textheight}
			\includegraphics<2>[scale=.6]{../docs/images/archi-couches-1}
			\includegraphics<3>[scale=.6]{../docs/images/archi-couches-2}
			\includegraphics<4>[scale=.6]{../docs/images/archi-couches-3}
		\end{overlayarea}
	\end{center}
	\begin{tabular}{m{.18\textwidth} m{.8\textwidth}}
		\includegraphics<2->[scale=.5]{../docs/images/couche-pres} &  \uncover<2->{Affichage des données et interaction avec l'utilisateur.} \\
		\includegraphics<4->[scale=.5]{../docs/images/couche-trait}\vphantom{\includegraphics[scale=.5]{../docs/images/couche-trait}} & \uncover<4->{Traitements métiers.} \\
		\includegraphics<3->[scale=.5]{../docs/images/couche-don}\vphantom{\includegraphics[scale=.5]{../docs/images/couche-don}} &   \uncover<3->{Stockage des informations.} \\
	\end{tabular}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Architecture en couche}
	\begin{itemize}[<+->]
		\item Il s'agit d'un découpage logique, pas physique.
		\item Dans une architecture centralisée, toutes les couches sont sur la même machine.
		\item Dans une architecture distribuée, les couches peuvent être sur des machines différentes. Nous verrons qu'une couche donnée est même souvent sur plusieurs machines.
		\item Ces couches sont souvent elles-mêmes sous découpées.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Architecture en couche}
	Découpage plus fin :\\~\\
	\begin{tabular}{m{.18\textwidth} m{.8\textwidth}}
		\includegraphics<2->[scale=.5]{../docs/images/couche-pres-1} &  \uncover<2->{Affichage des données.} \\
		\includegraphics<2->[scale=.5]{../docs/images/couche-pres-2} &  \uncover<2->{Gestion des actions.} \\
		& \\
		\includegraphics<3->[scale=.5]{../docs/images/couche-trait} & \uncover<3->{Traitements métiers.} \\
		& \\
		\includegraphics<4->[scale=.5]{../docs/images/couche-don-1} &   \uncover<4->{Accès aux informations.} \\
		\includegraphics<4->[scale=.5]{../docs/images/couche-don-2} &   \uncover<4->{Stockage des informations.} \\
	\end{tabular}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Architecture client/serveur}
	\begin{center}
		\includegraphics[scale=.5]{../docs/images/archi-client-serveur}
	\end{center}
	\begin{itemize}
		\item 2 programmes et 1 protocole :
			\begin{itemize}
				\item Programme serveur : il fournit un service.
				\item Programme client : il consomme ce service.
				\item Protocole : c'est le moyen de communication.
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Architecture client/serveur}
	\begin{center}
		\begin{overlayarea}{0.3\textwidth}{0.45\textheight}
			\includegraphics<1>[scale=0.5]{../docs/images/network-Server-based}
			\includegraphics<2>[scale=0.5]{../docs/images/network-P2P}
		\end{overlayarea}
	\end{center}
	\begin{itemize}
		\item C'est un découpage logique, indépendant de la notion de machine :
			\begin{itemize}[<+->]
				\item Client et serveur peuvent être sur la même machine, ou sur des machines différentes.
				\item Dans un réseau P2P, une machine est tour à tour client puis serveur.
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Architecture client/serveur}
	\begin{center}
		\includegraphics[scale=.4]{../docs/images/archi-client-serveur-restaurant}
	\end{center}
	{\scriptsize\url{https://www.manutan.com/blog/medias/file_bank/Images/2019/12_D\%C3\%A9cembre/thumbs/863_Infographie-API-restaurant-FR-800-100.jpg}}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Architecture 2-tiers (mainframe)}
	\begin{center}
		\includegraphics[scale=.5]{../docs/images/archi-2tiers-1}
	\end{center}
	\begin{itemize}
		\item L'application est sur un serveur.
		\item Le client est une application légère.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Architecture 2-tiers (client lourd)}
	\begin{center}
		\includegraphics[scale=.5]{../docs/images/archi-2tiers-2}
	\end{center}
	\begin{itemize}
		\item L'application est sur le client.
		\item Les données sont sur un serveur.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Architecture 2-tiers (client lourd)}
	\begin{center}
		\includegraphics[scale=.5]{../docs/images/archi-2tiers-3}
	\end{center}
	\begin{itemize}
		\item Le cœur de l'application est sur un serveur.
		\item La couche présentation est sur le client.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Architecture 3-tiers (client léger)}
	\begin{center}
		\includegraphics[scale=.5]{../docs/images/archi-3tiers}
	\end{center}
	\begin{itemize}
		\item L'application est sur un serveur.
		\item Les données sont sur un autre serveur.
		\item Le client est une application légère (navigateur Web).
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Architecture n-tiers (client léger)}
	\begin{center}
		\includegraphics[scale=.5]{../docs/images/archi-4tiers}
	\end{center}
	\begin{itemize}
		\item Généralisation des modèles précédents.
		\item Remarque : un serveur peut être le client d'un autre serveur !
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Stratification}
	\begin{itemize}[<+->]
		\item Ce concept est utilisé dans beaucoup de domaine de l'informatique : réseaux, systèmes d'exploitation, ...
		\item Avantages
			\begin{itemize}[<+->]
				\item On peut maîtriser tous les détails d'une couche sans forcement connaitre les détails des autres couches.
				\item On peut remplacer l'implémentation d'une couche (si on laisse les services fourni inchangés) sans toucher les autres couches.
				\item La division en couches rend plus facile la standardisation.
				\item La division en couches rend plus facile la réutilisation.
				\item Plus le découpage en couche est fin, plus il est facile de faire intervenir différents profils sur le projet : développeur, graphiste, ...
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitlepagenumber{Stratification}
	\begin{itemize}[<+->]
		\item Inconvénient
			\begin{itemize}[<+->]
				\item S'il y a trop de couches dans l'application, il peut y avoir des problèmes de performance.
				\item Il peut y avoir des technologies très variées : nécessite d'utiliser des frameworks pour gérer l'hétérogénéité.
				\item De manière générale, rend le code et le fonctionnement plus complexe.
			\end{itemize}
	\end{itemize}
\end{frame}
