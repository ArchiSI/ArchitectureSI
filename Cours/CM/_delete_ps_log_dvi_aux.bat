@ECHO OFF

del ".\*.ps"
del ".\*.log"
del ".\*.dvi"
del ".\*.aux"
del ".\*.nav"
del ".\*.out"
del ".\*.snm"
del ".\*.toc"
del ".\*.gz"
del ".\*.blg"
del ".\*.bbl"
del ".\*.synctex*"
del ".\*.fdb_latexmk"
del ".\*.fls"
del ".\*.vrb"

exit
