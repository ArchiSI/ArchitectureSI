# TD 1 - Premier projet Web

L'objectif de ce TD est de créer un premier projet Web, de bien comprendre les requêtes HTTP GET et POST et l'utilisation des _Servlets_.


## Exercice 1 - L'environnement de développement

### 1. Installation de l'environnement de développement

Première chose à faire avant de se lancer dans le développement : installer l'environnement de développement. Pour cela, créer quelque part sur votre poste un dossier 📂`jee` dans lequel nous allons placer tous les outils nécessaires au developpement de notre projet. Pour cette première version de notre site, nous avons besoin de trois choses :

1. La dernière version d'**eclipse**.<br>On la trouve ici : [https://www.eclipse.org/downloads/](https://www.eclipse.org/downloads/). Attention, il faut bien sélectionner "Eclipse IDE for Entreprise Java and Web Developers" comme dans la capture d'écran ci-dessous :
    <center>![1er test](images/install_eclipse.png)</center>
1. La dernière version de la **jdk**.<br>On la trouve ici : [https://www.oracle.com/java/technologies/downloads](https://www.oracle.com/java/technologies/downloads).
1. Une version de **tomcat**.<br>Ici, nous n'allons pas télécharger la dernière version (la 10 actuellement), car elle implémente Jakarta EE 9. Nous allons télécharger la version tomcat9 (disponible ici : [https://tomcat.apache.org/download-90.cgi](https://tomcat.apache.org/download-90.cgi)), qui implémente Java EE 8. De manière générale, il peut être judicieux de ne pas utiliser les toutes dernières versions des produits, qui peuvent contenir des bugs non encore connus (de sécurité notamment) ...<br/>
    Il suffit de télécharger le zip (si on travaille avec windows), et de le dézipper dans le dossier 📂`jee`.


### 2. Configuration de l'environnement de développement

1. On commence par lancer eclipse, en indiquant comme espace de travail (workspace) 📂`<jee>/ws_archisi`, en remplaçant `<jee>` par l'emplacement du dossier 📂`jee` que vous avez créé plus tôt.

    ??? note "Remarque"

        On pourra placer tous les projets que nous allons créer dans ce cours dans ce _workspace_. Ils auront tous un nom différent, donc cela ne posera pas de problème.

2. Dans eclipse, on sélectionne la dernière _jdk_ comme _jre_ par défaut.
    * Pour cela, on va dans le menu :fontawesome-solid-bars:`Window > Preferences`, puis dans l'arborescance :material-file-tree:`Java > Installed JREs`.
    * On clique sur <bouton><u>A</u>dd...</bouton>, on sélectionne _Standard VM_, on clique sur <bouton>Direct<u>o</u>ry...</bouton> pour indiquer le dossier où est présente la _jdk_ et enfin sur <bouton>Finish</bouton>.
    * Dans la liste des _jres_, on coche la case correspondant à cette _jdk_ pour que ce soit celle par défaut.
2. Nous allons développer une application JEE, déployée sur un serveut tomcat 9. Eclipse nous permet de définir un **environnement d'éxécution** (_**runtime environment**_ en anglais). Ainsi, en associant un projet donné à un _runtime_ donné, toutes les bibliothèques nécessaires sont automatiques ajoutées au projet.
    * Pour cela, dans eclipse, on va dans le menu :fontawesome-solid-bars:`Window > Preferences`, puis dans l'arborescance :material-file-tree:`Server > Runtime Environments`, puis <bouton><u>A</u>dd...</bouton>.
    * Dans la liste, sélectionner :material-file-tree:`Apache > Apache Tomcat v9.0` puis <bouton><u>N</u>ext ></bouton> (décocher la case `Create a new local server`).
    * Cliquer sur <bouton>Browse...</bouton> pour indiquer le dosser où tomcat a été dézippé.
    * On peut enfin cliquer sur <bouton>Finish</bouton>.

## Exercice 2 - Création d'un projet Web dynamique

Eclipse offre la possibilité de créer plusieurs types de projets Java. Pour pouvoir créer et déployer des _Servlets_, il faut créer un **_projet Web dynamique_** (_Dynamic Web Project_ en anglais).

1. On commence donc par créer un projet Web dynamic :<br>
    Dans eclipse, on se place dans la vue `Project Explorer`. Clic droit avec la souris, puis :material-mouse:`New > Project`. Il faut enfin aller dans :material-file-tree:`Web > Dynamic Web Project` comme dans la capture d'écran ci-dessous :
    <center>![Nouveau projet Web dynamique](images/newDynamicWebProject-a.png)</center>
2. Il faut ensuite donner un nom au projet (**_PremierProjet_** ici), indiquer l'environnement d'exécution (pour l'instant, il n'y en a qu'un, c'est tomcat9 - :warning: il y en aura bientôt plusieurs, et il faudra faire bien attention !), puis cliquer sur <bouton>Next ></bouton> comme sur la capture d'écran ci-dessous :
    <center>![Nouveau projet Web dynamique](images/newDynamicWebProject-b2.png)</center>
3. :warning: :warning: Il faut ensuite cliquer une fois sur <bouton>Next ></bouton> à nouveau, **mais attention à bien cocher la case _Generate web.xml deployment descriptor_ dans le dernier écran**. On peut ensuite cliquer sur `Finish`.
    <center>![Nouveau projet Web dynamique](images/newDynamicWebProject-c2.png)</center>

    ??? bug "Pas de web.xml ?"

        Si vous avez oublié de cocher cette case, il faudra soit supprimer ce projet et recommencer à l'étape 1, soit ajouter manuellement le fichier 📄`web.xml`, **en respectant la structure attendue**.

???+ note "Racine de l'application Web"

    Par défaut, le _context root_ de l'application est le nom du projet, ici **_PremierProjet_**.<br>
    Cela signifie que pour accéder au contenu de l'application depuis un navigateur Web, une fois qu'elle sera déployée sur un serveur, toutes les URLs seront du type [http://localhost:8080/PremierProjet/...](http://localhost:8080/PremierProjet/...).

    Cela signifie également qu'on peut déployer autant de projet Web que souhaité sur le serveur tomcat, puisqu'ils auront tous un _context root_ différent.

???+ note "Arborescence d'un projet Web dynamique"

    Par défaut, voici l'arborescence d'un projet Web dynamique (on peut la modifier à la création du projet dans eclipse, mais c'est déconseillé) :

    <pre style="line-height: 1.2;">
    PremierProjet/
    └── src/
        └── main/
            ├── java/
            │   └── ici, il y a les packages (qui sont des dossiers) et les classes Java.
            └── webapp/ (c'est le contenu Web de l'application)
                ├── META-INF/
                │   └── MANIFEST.MF
                ├── WEB-INF/
                │   └── lib/
                │   └── web.xml
                └── ici, il y a toutes les pages HTML, JSP, CSS, JavaScript ... éventuellement dans des sous-dossiers.
    </pre>

    Ainsi, l'URL [http://localhost:8080/PremierProjet/](http://localhost:8080/PremierProjet/) pointe vers le dossier 📂`webapp`. On peut accéder, par exemple, à un sous-dossier 📂`css` contenant le script 📄`style.css` via l'URL [http://localhost:8080/PremierProjet/css/style.css](http://localhost:8080/PremierProjet/css/style.css).


## Exercice 3 - Requêtes HTTP

Maintenant que nous avons créé ce projet Web, nous allons le déployer et le tester.

### 1. Récupération d'un contenu Web

Récupérer le fichier 📄`testHttp.html` et les dossier 📂`css` et 📂`javascript` présents sur [Celene](https://celene.univ-tours.fr/mod/folder/view.php?id=628763) et les déposer dans le dossier 📂`webapp` du projet.

### 2. Création d'un serveur local<a name="creation-serveur"></a>

Pour tester notre application, il faut la déployer sur un serveur (tomcat9), et il faut donc au préalable créer une instance de tomcat9 sur notre poste.

Pour cela :

1. Il faut aller dans la vue `Servers` dans _eclipse_ (si elle n'est pas affichée, il faut aller dans :fontawesome-solid-bars:`Window > Show View > Servers`).
2. Dans la vue `Servers`, faire un clic droit, puis :material-mouse:`New > Server`.
3. Naviguer vers :material-file-tree:`Apache > Tomcat v9.0 Server`.
4. :warning: **Vérifier** qu'il y a bien la bonne _Server runtime environment_ (encore une fois, pour l'instant, il n'y en a qu'une, mais il y en aura bientôt plusieurs !)
5. On peut directement cliquer sur <bouton>Finish</bouton>.
6. :warning: Dans la vue `Servers`, double-cliquer sur le serveur Tomcat, dans la partie `Server Locations`, sélectionner `Use Tomcat Installation`, et enregistrer.
7. Le serveur peut maintenant être démarré :
    <center>
      ![Page d'accueil](images/demarrer_serveur.png)
    </center>
    <center>(1) pour le démarrer normalement, (2) pour le démarrer en mode debug.</center>
8. On vérifie dans les _logs_ du serveur que tout s'est bien passé.


### 3. Déploiement de l'application sur ce serveur<a name="deploiement-application"></a>

Pour déployer notre application **_PremierProjet_** sur ce serveur (on aurait pu le faire directement en le créant), il faut faire un clic droit sur le serveur, puis :material-mouse:`Add and Remove...`, puis ajouter le projet **_PremierProjet_** (le mettre à droite), et enfin cliquer sur <bouton>Finish</bouton>.

On vérifie dans les _logs_ du serveur que tout s'est bien passé.


### 4. Études des requêtes HTTP

Notre application est disponible à l'adresse [http://localhost:8080/PremierProjet/testHttp.html](http://localhost:8080/PremierProjet/testHttp.html).

Accéder à cette page HTML depuis Firefox, puis ouvrir la console de Firefox (++ctrl+shift+"K"++), aller dans le menu :fontawesome-solid-bars:`Réseau`, puis rafraîchir la page (++ctrl+shift+"R"++)

1. Il doit y avoir 5 lignes (sinon rafraîchir à nouveau). Chacune correspond à une requête HTTP.<br>
    Indiquer l'origine de chacune de ces requêtes par rapport au fichier HTML.
2. La première colonne donne le code de retour. Il y a une erreur. La corriger.
3. La page HTML contient un formulaire dont la méthode est GET.<br>
    Vérifier qu'à sa validation, le mot de passe entré apparaît "en clair" dans l'URL.
    
    1. Que faut-il modifier pour que ça ne soit plus le cas ?
    2. Combien l'URL contient-elle de paramètres ?
    3. Pour analyser une requête HTTP, il faut cliquer sur la ligne qui correspond. On obtient alors plusieurs onglets, qui permettent notamment d'obtenir les en-têtes (de la question et de la réponse), les cookies, la requête, et la réponse.<br>
        Avec la modification apportée à la question précédente, où sont maintenant indiqués les différents paramètres (`leChampTexte`, `leMotDePasse`, ...) ?





## Exercice 4 - Une première _Servlet_

Maintenant que notre premier projet Web dynamique est créé, nous allons pouvoir créer des _Servlets_.

1. Pour en créer une, il faut aller dans la vue `Project Explorer`, sur le projet **_PremierProjet_**, faire un clic droit, puis :material-mouse:`New > Other`. Dans l'arborescence, naviguer ensuite vers :material-file-tree:`Java > Class`.
2. Cliquer sur <bouton><u>N</u>ext ></bouton>.
3. Indiquer le package : `edu.polytech.archi.servlet`.

    ???+ warning

        On ne laisse jamais le package par défaut lorsqu'on développe une application avec JEE.

4. Indiquer le nom de classe : `MaPremiereServlet`.

    ???+ success "Remarque"

        Il est pratique d'indiquer ce qu'est la classe dans son nom, ici, une _Servlet_.

4. Indiquer que la classe hérite de `HttpServlet` de l'API `servlet` (en cliquant sur <bouton>Brow<u>s</u>e</bouton> en face de _Superclass_), puis clique sur <bouton><u>F</u>inish</bouton>.
5. Recopier le contenu du cours, et corriger les éventuelles erreurs et warnings :

    ``` java title="☕ Code Java - Servlet MaPremiereServlet" linenums="1"
    public void init() {
        super.init();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter(); //(1)
        out.println("<html><head></head><body>Hello World !</body></html>"); //(2)
    }

    public void destroy() {
        super.destroy();
    }
    ```

    1. Cet objet va nous permettre d'écrire dans la réponse HTTP.
    2. On écrit directement le code HTML dans la réponse, qui va s'afficher dans le navigateur.

8. C'est bon, notre _Servlet_ est créée ! Il n'y a plus qu'à la déployer. Pour cela, on va la recenser dans le descripteur de déploiement :
    ``` xml title="Le fichier 📄 web.xml" linenums="1"
    <?xml version="1.0" encoding="UTF-8"?>
    <web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	          xmlns="http://xmlns.jcp.org/xml/ns/javaee"
	          xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
	          id="WebApp_ID" version="4.0">
        <display-name>PremierProjet</display-name>
        <servlet><!--(1)-->
          <servlet-name>maPremiereServlet</servlet-name>
          <servlet-class>edu.polytech.archi.servlet.MaPremiereServlet</servlet-class>
        </servlet>
        
        <servlet-mapping><!--(2)-->
          <servlet-name>maPremiereServlet</servlet-name>
          <url-pattern>/bonjour</url-pattern>
        </servlet-mapping>
    </web-app>
    ```

    1. Ici, on recense la _servlet_.<br><br>
            - `<servlet-name>` : on lui donne un nom.<br><br>
            - `<servlet-class>` : on indique la classe correspondante.
            
    2. La balise `<servlet-mapping>` permet d'indiquer avec quelle(s) URL(s) on peut accéder à la _Servlet_ :<br><br>
            - `<servlet-name>` indique de quelle _servlet_ on parle,<br><br>
            - `<url-pattern>` indique quelle(s) URL(s) y accède. Ici, l'URL [http://localhost:8080/PremierProjet/bonjour](http://localhost:8080/PremierProjet/bonjour).
  
    ???+ note

        Le descripteur de déploiement peut être vu comme une sorte d'annuaire, dans lequel nous allons recenser

        1. toutes les servlets définies dans l'application,
        2. quelles sont les URL qu'il faut saisir pour y accéder.

9. Vérifier que "Hello World" est bien affiché à cette [URL](http://localhost:8080/PremierProjet/bonjour).

???+ success "Prise en compte des modifications"

    Pour être sûr que les modifications effectuées sont bien prises en compte, le plus simple est de redémarrer le serveur Tomcat après chaque modification.



## Exercice 5 - Une première _Servlet_ avec des paramètres

On reprend ici la _Servlet_ de l'exercice précédent. Nous allons modifier la méthode `doGet` pour qu'elle redirige vers une page HTML contenant un formulaire. Ce formulaire, à sa soumission, déclenchera une requête HTTP POST, avec un paramètre.

1. Modifier la méthode `doGet` pour qu'elle ne fasse que la redirection vers la page 📄`premierFormulaire.html` :
    ``` java title="☕ Code Java - MaPremiereServlet.doGet" linenums="1"
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("premierFormulaire.html"); //(1)
        dispatcher.forward(request, response); //(2)
    }
    ```

    1. Indique vers quelle ressource la requête devra être redirigée, ici vers 📄`premierFormulaire.html`.
    2. Effectue la redirection (toutes les informations de la requête et de la réponse sont redirigées).

2. Créer la page 📄`premierFormulaire.html` (dans le dossier 📂`webapp`) :
    ``` html linenums="1"
    <html>
      <head>
        <title>1er formulaire</title>
      </head>
      <body>
        <form method="post" action="bonjour"><!--(1)-->
          <input type="text" name="prenom"/><!--(2)-->
          <input type="password" name="mdp"/><!--(3)-->
          <input type="submit" value="Valider"/><!--(4)-->
        </form>
      </body>
    </html>
    ```

    1. Le formulaire HTML (balise `<form>`) permet au client d'envoyer des informations au serveur via une requête HTTP.<br>
        Ici, ce sera une requête POST (attribut `method`), qui exécutera l'URL [/bonjour](/bonjour) (attribut `action`).
    2. Les balises `<input type="xxx">` permettent  à l'utilisateur de saisir des informations dans le formulaire. Elle seront placées en paramètres de la requête, avec comme clef la valeur de l'attribut `name`.
    3. Les balises `<input type="xxx">` permettent  à l'utilisateur de saisir des informations dans le formulaire. Elle seront placées en paramètres de la requête, avec comme clef la valeur de l'attribut `name`.
    4. La balise `<input type="submit">` ajoute un bouton qui va permettre de soumettre le formulaire, c'est-à-dire d'exécuter la requête correspondant à ce qui a été indiqué dans la balise `<form>`.

3. Dans la _Servlet_ `MaPremiereServlet`, ajouter la méthode `doPost` :
    ``` java title="☕ Code Java - MaPremiereServlet.doPost" linenums="1"
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        // Récupération de la valeur du champ dont l'attribut "name" est "prenom".
        String prenom = request.getParameter("prenom"); //(1)
        // Génération du code HTML.
        PrintWriter out = response.getWriter();
        out.println("<html><head></head><body>Salut " + prenom + " !</body></html>");
    }
    ```

    1. Il faut qu'une des balise `<input type="xxx">` du formulaire est _prenom_ comme valeur pour l'attribut `name`.

4. Vérifier le bon fonctionnement à cette [URL](http://localhost:8080/PremierProjet/bonjour).

???+ warning

    En pratique, on n'écrira **jamais** de code HTML dans une _Servlet_, via l'objet `PrintWriter`. On redirigera **toujours** la requête vers une page _JSP_.

    C'est un peu l'idée du paradigme MVC, on ne mélange pas le contrôleur (la _Servlet_) et la vue (les pages HTML et les _JSP_ dont nous allons parler juste après).


## Exercice 6 - Une première _JSP_

Le but de cet exercice est de créer une première JSP, qui sera accessible via une _Servlet_.

Nous allons créer un formulaire, contenant un seul champ de type _nombre_ (`<input type="number"/>`), qui permet, lorsqu'on clique sur <bouton>Valider</bouton> d'afficher "_Bonjour !_" autant de fois que la valeur de ce champ.

1. Créer une _Servlet_ `ListeServlet`, dont la méthode `doGet` créé une liste de `String` vide, placée dans la requête avec la clef `LISTE`, et qui redirige vers `afficheListe.jsp`
    ``` java linenums="1"
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("LISTE", new ArrayList<String>()); //(1)
        request.getRequestDispatcher("afficheListe.jsp").forward(request, response);
    }
    ```

    1. Dans la _JSP_ `afficheListe.jsp`, nous allons utiliser une liste. Pour ne pas avoir d'erreur, on place dans la requête une liste vide au premier affichage.

    ???+ note "Paramètres et Attributs en requête"

        On peut placer deux types d'objets dans la requête :

        1. Depuis un formulaire, on peut placer des `parameter`. Ces sont des `String`. Il n'est pas possible de placer un paramètre en requête via l'objet Java (on ne peut pas faire `request.setParameter(clef, valeur)`).
        2. Dynamiquement, dans une _Servlet_ par exemple, on peut placer des `attributes`. Ces sont des `Objects`, qu'il faut donc _caster_ lorsqu'on les manipule. On place des attributs en requête via l'objet Java : `request.setAttribute(clef, valeur)`.

2. Déployer cette _Servlet_ dans le descripteur de déploiement :
    ``` xml title="Le fichier 📄 web.xml" linenums="1"
    <?xml version="1.0" encoding="UTF-8"?>
    <servlet>
      <servlet-name>listeS</servlet-name>
      <servlet-class>edu.polytech.archi.servlet.ListeServlet</servlet-class>
    </servlet>

    <servlet-mapping>
      <servlet-name>listeS</servlet-name>
      <url-pattern>/liste</url-pattern>
    </servlet-mapping>
    ```
3. Créer la page _JSP_ `afficheListe.jsp`, qui contient le formulaire avec le champ nombre, dont le `name` est `taille` (c'est le nom du paramètre en requête). Pour cela, faire un clic droit sur le projet, puis :material-mouse:`New > Other`, puis :material-file-tree:`Web > JSP File`, puis <bouton><u>N</u>ext ></bouton>. Saisir le nom de la _JSP_ en indiquant son emplacement (📂`src/main/webapp`), et enfin cliquer sur <bouton><u>F</u>inish</bouton>.
    ``` jsp linenums="1"
    <%@page import="java.util.List"%>
    <html><head><title>Affiche une liste dynamique</title></head>
    <body>
      <form action="liste" method="post">
        <input type="number" name="taille" value="<%=((List<String>)request.getAttribute("LISTE")).size()%>"/><!--(1)-->
        <input type="submit" name="Valider"/>
      </form>
      <table>
        <%
        List<String> liste = (List<String>)request.getAttribute("LISTE"); //(2)
        for (String mot : liste) {
        %>
          <tr><td><%=mot %></td></tr>
        <% } %>
      </table>
    </body>
    </html>
    ```

    1. L'attribut `value` permet de donner une valeur à ce champ à l'affichage de la page. Ici, la valeur sera 0 au premier affichage (la taille de la liste vide), puis celle saisie précédemment aux suivants.
    2. On récupère la liste placée en attribut de la requête, et on affiche autant de ligne dans le tableau qu'il y a d'éléments dans la liste.

4. Enfin, dans la _Servlet_ ajouter la méthode `doPost`, qui gère le clic de l'utilisateur sur <bouton>Valider</bouton> :
    ``` java linenums="1"
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer taille = Integer.parseInt(request.getParameter("taille"));
        List<String> liste = new ArrayList<String>();
        for (int i = 0; i < taille; i++) {
            liste.add("Bonjour !"); //(1)
        }
        request.setAttribute("LISTE", liste); //(2)
        request.getRequestDispatcher("afficheListe.jsp").forward(request, response); //(3)
    }
    ```

    1. On génère la liste avec autant de _Bonjour !_ que souhaité.
    2. On place cette liste en attribut de la requête, pour pouvoir la récupérer dans la _JSP_.
    3. On redirige la requête vers la _JSP_.

5. Vérifier que l'application est fonctionnelle via [http://localhost:8080/PremierProjet/liste](http://localhost:8080/PremierProjet/liste).
