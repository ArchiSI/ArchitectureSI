# TP1 - Web Services SOAP - Introduction

???+abstract "Rappels du cours"

	1. Il est demandé ici de créer des Web Services **SOAP**. Pour cela, il faut créer des projets "Java Web Dynamique".
	2. De manière générale, lorsqu'il vous est demandé de créer une application Web, demandez-vous quelles sont les briques, par rapport au schéma ci-dessous, que vous devez mettre en place :
		<center>
			![Archi appli Web](../images/schema_archi_V5.png)
		</center>
	3. Pour nous simplifier la vie, sauf si le contraire est explicitement demandé, nos projets ne seront pas constitués d'un projet EAR, d'un projet Web et d'un projet EJB, mais uniquement d'un projet Web.
	


???+exercice "Exercice 1 - Sécurité sociale"

	1. Créer un Web Service permettant de vérifier qu'un numéro de sécurité sociale est correct, c'est-à-dire que la clef de contrôle (les deux derniers chiffres) est correcte.
	2. Tester ce Web Service avec SoapUI.
	3. Créer un client Java qui consomme ce Web Service.<br>
		Attention, ce client doit lui-même être un (autre) projet Web dynamique. En effet, vous devez créer une page Web contenant un champ où indiquer un numéro de sécurité sociale et un bouton "Valider".<br>
		Lorsque l'utilisateur clique sur "Valider", un message apparaît à côté du bouton "Valider" et indique si le numéro de sécurité sociale est correct ou pas.
	
	
???+abstract "Implémenter un Web Service SOAP en JEE"

    Nous allons utiliser `JaxWS` (pour _Web Services_) et `JaxB` (pour _Binding_, pour gérer l'_OXM_).
    
    1. `JaxWS` nous fourni des annotations qui permettent de créer simplement un Web Service :
    
        - `@WebService` : à mettre sur la classe. Indique qu'il s'agit d'un Web Service. On rajoute la propriété `serviceName` pour indiquer son nom.
        - `@WebMethod` : indique qu'il s'agit d'un service exposé par le Web Service. On peut ajouter la propriété `operationName`, si on veut que le nom du service exposé ne soit pas celui de la méthode.
        - `@WebParam` : indique qu'il s'agit d'un paramètre du service exposé. Il **faut** utiliser la propriété `name`, sinon les paramètres sont nommés `arg0`, `arg1`, ...
    
    2. `JaxB` fourni également quelques annotations pour le _OXM_ :
    
        - `@XmlRootElement` : Associer une classe à un élément XML.
        - `@XmlSchema` : Associer un espace de nom à un package.
        - `@XmlTransient` : Ne pas sérialiser une entité.
        - `@XmlAttribute` : Convertir une variable en un attribut XML.
        - `@XmlElement` : Convertir une variable en un élément XML.
        - `@XmlAccessorType` : Préciser quels éléments de la classe sont exposés dans le document XML.
        - `@XmlNs` : Associer un préfixe d’un espace de noms à un URI.
        
        Pour l'instant, nous n'utiliserons que `@XmlTransient` et `@XmlAccessorType`.
	
	
???+abstract "Générer un client SOAP Java"

	L'exécutable `wsimport`, fourni de base avec la JDK (si la version est inférieure ou égale à 1.8), permet de générer le code Java pour créer un client SOAP.
	
	Deux options sont importantes :
	
	1. `-s` : permet d'indiquer où les sources vont être générées. Le plus simple est de se placer dans le dossier "source" du projet concerné, et d'indiquer `-s .` (ce qui signifie "générer les sources ici)".
	2. `-p` : (optionnel) permet d'indiquer le package dans lequel les classes générées vont être placées.
	
	Par exemple, après s'être placé dans le dossier 📂`src/main/java` du projet Web client, on pourra écrire
	```bash
	wsimport -s . -p edu.polytech.secu.service.ws http://localhost:8100/SecuSocialeWebServiceCorr/SecuWebService?wsdl
	```
	
	En effet, le client SOAP va permettre d'implémenter un service. C'est pourquoi on place ces sources dans le package `service`.
	
	Parfois, le client SOAP sera la source de données. Dans ce cas là, on pourra placer les sources dans le package `dao`.


???+exercice "Exercice 2 - Remboursement d'un prêt"

	1. Créer un Web Service qui permet de déterminer le montant de la mensualité d'un crédit en fonction du montant emprunté, de la durée de remboursement et du taux d'intérêt annuel
	
		On pourra s'aider de la formule suivante :
		
		\[\text{mensualité} = \dfrac{\text{capital}\times{}\text{tauxmensuel}}{1-\left(1+\text{tauxmensuel}\right)^{-\text{nbMensualités}}}\]
        
        où :
        
        - \(\text{capital}\) correspond au montant emprunté,
        - \(\text{nbMensualités}\) correspond au nombre de mensualités (par exemple, 240 pour 20 ans),
        - et où le taux mensuel est calculé à partir du taux annuel (celui indiqué par la banque ou l'organisme de prêt) avec cette formule :
        
            \[\text{tauxmensuel} = \left(1+\text{tauxannuel}\right)^{\frac{1}{12}}-1\]
        
	2. Tester ce Web Service avec SoapUI.
	3. Créer un client Java, cette fois-ci un projet Java classique, permettant de tester ce web service.


