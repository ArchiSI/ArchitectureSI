# TP3

## Structures des requêtes HTTP (Exercice 1)

```
GET /locations (application/json & application/xml)
GET /locations/{id} (application/json & application/xml)
DELETE /locations
DELETE /locations/{id}
POST /locations (application/x-www-form-urlencoded)
POST /locations (application/json & application/xml)
PUT /locations/{id} (application/json & application/xml)
PATCH /locations/{id} (application/json & application/xml)
```


## Couche DAO

???+info "Voici les modifications à apporter dans la couche _DAO_ :"

    ```java title="☕ Code Java - Interface Dao"
    /**
     * 
     * @param filtre La ville sur laquelle filtrer les locations.
     * @return La liste des locations dans la ville indiquée en filtre.
     */
    public List<LocationBean> getLocations(String filtre);

    /**
     * Supprime la location d'identifiant indiqué.
     * 
     * @param id L'identifiant de la location à supprimer.
     */
    public void deleteLocation(Integer id);

    /**
     * Supprime toutes les locations !
     */
    public void deleteLocations();

    /**
     * Met à jour une location. La location mise à jour est celle d'identifiant
     * {@link LocationBean#getId()} du {@link LocationBean} en paramètre. Toute les
     * autres propriétés du bean sont mises à jour en BDD.
     * 
     * @param bean La location à mettre à jour.
     */
    public void updateLocation(LocationBean bean);
    ```

???+info "Implémentation de la couche _DAO_, _JPA_ fait (presque) tout pour nous !"

    ```java title="☕ Code Java - Implémentation de l'interface Dao"
    @Override
    public List<LocationBean> getLocations(String filtre) {
        Query request = em.createQuery("select l from LocationBean l where l.city=?1");
        request.setParameter(1, filtre);
        return request.getResultList();
    }

    @Override
    public void deleteLocation(Integer id) {
        em.remove(getLocation(id));
    }

    @Override
    public void deleteLocations() {
        for (LocationBean bean : getLocations()) {
            em.remove(bean);
        }
    }

    @Override
    public void updateLocation(LocationBean bean) {
        em.merge(bean);
    }
    ```


## Couche métier

???+ info "Voici les modifications à apporter dans la couche métier - `getLocations`"

    La signature de la méthode `getLocations()` est modifiée, et prend maintenant en paramètre une chaîne de caractère, qui permet de ne renvoyer que les locations présentes dans cette ville.

    ```java title="☕ Code Java - Interfaces EJB (local et remote)"
    /**
     * 
     * @param filtre Permet de filtrer sur les villes.
     * @return La liste des locations dans la ville "filtre", si "filtre" est non
     *         <code>null</code>, toutes les locations sinon.
     */
    public List<LocationBean> getLocations(String filtre);
    ```

???+info "Implémentation de la couche métier - `getLocations`"

    ```java title="☕ Code Java - Implémentation de l'EJB"
    @Override
    public List<LocationBean> getLocations(String tri) {
        if (null == tri || "".equals(tri)) {
            return locationDao.getLocations();
        } else {
            return locationDao.getLocations(tri);
        }
    }
    ```

???+ info "Voici les modifications à apporter dans la couche métier - suite"

    ```java title="☕ Code Java - Interfaces EJB (local et remote)"
    public void deleteLocation(Integer id);

    public void deleteLocations();

    /**
     * Mise à jour (totale) de la location.
     * 
     * @param bean La location à mettre à jour.
     */
    public void updateLocation(LocationBean bean);

    /**
     * Mise à jour (partielle) de la location.
     * 
     * @param oldBean     La location à mettre à jour (seul son id nous intéresse).
     * @param patchedBean Les nouvelles valeurs (ne seront mis à jour que les champs
     *                    renseignés dans ce bean).
     */
    public void patchLocation(LocationBean oldBean, LocationBean patchedBean);
    ```

???+info "Implémentation de la couche métier - suite"

    ```java title="☕ Code Java - Implémentation de l'EJB"
    @Override
    public void deleteLocation(Integer id) {
        this.locationDao.deleteLocation(id);
    }

    @Override
    public void deleteLocations() {
        this.locationDao.deleteLocations();
    }

    @Override
    public void updateLocation(LocationBean bean) {
        this.locationDao.updateLocation(bean);
    }

    @Override
    public void patchLocation(LocationBean oldBean, LocationBean patchedBean) {
        if (null != patchedBean.getAddress()) {
            oldBean.setAddress(patchedBean.getAddress());
        }
        if (null != patchedBean.getCity()) {
            oldBean.setCity(patchedBean.getCity());
        }
        if (null != patchedBean.getZipCode()) {
            oldBean.setZipCode(patchedBean.getCity());
        }
        if (null != patchedBean.getNightPrice()) {
            oldBean.setNightPrice(patchedBean.getNightPrice());
        }
        this.locationDao.updateLocation(oldBean);
    }
    ```

## Couche modèle

Pour générer du JSON à partir de notre bean `LocationBean`, il n'y a rien à faire, JaxRS s'occupe de tout par défaut. En revanche, pour générer (et gérer) du XML, il faut indiquer que nous allons utiliser JaxB.

Pour cela, il suffit d'ajouter l'annotation `@XmlRootElement` sur cette classe.

Par ailleurs, pour indiquer qu'on ne souhaite pas exposer certains champs de ce bean, on ajoute l'annotation `@XmlTransient` sur ces champs (c'est le cas ici du champ `picture`, qu'on ne veut pas exposer). Pour cela, il faut indiquer à JaxB qu'on place les annotations sur les champs, et pas sur les _getters/setters_, il faut donc ajouter l'annotation `@XmlAccessorType(XmlAccessType.FIELD)` sur la classe.

???+info "En résumé, voici les modifications à apporter à `LocationBean`"

    ```java title="☕ Code Java - LocationBean"
    package edu.polytech.location.model;

    import ...

    /**
     * Entity implementation class for Entity: LocationBean
     *
     */
    @XmlRootElement // Indique qu'on utilise JaxB pour l'OXM.
    @Entity
    @XmlAccessorType(XmlAccessType.FIELD) // Indique qu'on place des annotations sur les champs, et pas sur les
                                          // getters/setters.
    public class LocationBean implements Serializable {

        ...
        @Lob
        @XmlTransient // Indique qu'on ne souhaite pas exposer ce champ à l'exterieur (de notre
                      // appli).
        private byte[] picture;
        ...
    }
    ```

## Couche Web

???+info "Déclaration du contexte"

    ```java
    package edu.polytech.location.ws.rest;

    import javax.ws.rs.ApplicationPath;
    import javax.ws.rs.core.Application;

    @ApplicationPath("api")
    public class WebApp extends Application {

    }
    ```

???+info "Création du Web Service - La classe correspondante"

    ```java
    package edu.polytech.location.ws.rest;

    import ...

    @Stateless
    @Path("v1")
    /**
     * Remarque : le nom de la classe n'est pas important. C'est seulement le
     * "@Path" qui compte.
     * 
     * Même remarque pour les méthodes.
     *
     */
    public class LocationRest {

        @EJB
        private BusinessLocal businessLocal;
        
        ...
    }
    ```
    
    ```java title="☕ Code Java - Les verbes GET"
    /**
     * Service n°1 : Renvoie toutes les locations.
     */
    @GET
    @Path("/locations")
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public List<LocationBean> getLocations(@QueryParam("tri") String tri, @QueryParam("filtre") String filtre) {
        // getLocations prend un paramètre, qui peut être "null", qui permet de filtrer
        // les locations sur une ville données.
        // Si la valeur du fitre est "null", toutes les locations sont renvoyées.
        List<LocationBean> locations = this.businessLocal.getLocations(filtre);
        if (null != tri) {
            // On tri les locations si c'est demandé.
            if ("asc".equals(tri)) {
                Collections.sort(locations,
                        (l1, l2) -> l1.getId() > l2.getId() ? 1 : l1.getId() == l2.getId() ? 0 : -1);
            } else if ("desc".equals(tri)) {
                Collections.sort(locations,
                        (l1, l2) -> l1.getId() < l2.getId() ? 1 : l1.getId() == l2.getId() ? 0 : -1);
            }
        }
        return locations;
    }

    /**
     * Service n°2 : Renvoie la location dont l'identifiant est spécifié en "path
     * param".
     * 
     * Remarque : On pourrait choisir de renvoyer une erreur 404 si la location
     * n'existe pas.
     */
    @GET
    @Path("/locations/{id}")
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public LocationBean getLocation(@PathParam("id") Integer idLoc) {
        return this.businessLocal.getLocation(idLoc);
    }
    ```
    
    Pour la suite, nous allons avoir besoin de vérifier que l'utilisateur est correctement authentifié. Pour cela, on vérifie "juste" que la clef "Authorization" est présente dans l'en-tête de la requête, avec la valeur 42 :
    
    ```java title="☕ Code Java - Vérifier que l'utilisateur est authentifié"

    /**
     * Vérifie que l'utilisateur est authentifié, c'est-à-dire que la clef
     * "Authorization" de l'en-tête de la requête HTTP contient la valeur 42.
     * 
     * @param authorization : La valeur (éventuelle) de la clef "Authorization" de
     *                      l'en-tête de la requête.
     * @return Le status (401 ou 403) s'il y a eu un problème d'authentification,
     *         <code>null</code> si tout va bien.
     */
    private Status checkAuthorization(String authorization) {
        if (null == authorization) {
            return Status.UNAUTHORIZED;
        }
        if (!"42".equals(authorization)) {
            return Status.FORBIDDEN;
        }
        return null;
    }
    ```
    
    On peut maintenant gérer les requêtes de suppression (toute les locations, ou une seule avec son identifiant).
    
    ```java title="☕ Code Java - Les verbes DELETE"
    /**
     * Service n°3 : supprime la location dont l'identifiant est spécifié en "path
     * param".
     */
    @DELETE
    @Path("/locations/{id}")
    public Response deleteLocation(@PathParam("id") Integer idLoc,
            @HeaderParam(HttpHeaders.AUTHORIZATION) String authorization) {
        // On vérifie que l'utilisateur est correction authentifié.
        Status status = checkAuthorization(authorization);
        if (null != status) {
            return Response.status(status).build();
        }

        // Si la location n'existe pas en bas, on renvoie une erreur 404 ...
        if (null == this.businessLocal.getLocation(idLoc)) {
            return Response.status(Status.NOT_FOUND).build();
        }
        // ... sinon, on la supprime.
        this.businessLocal.deleteLocation(idLoc);
        return Response.ok().build();
    }

    /**
     * Service n°4 : Supprime toutes les locations présentes en BDD.
     */
    @DELETE
    @Path("/locations")
    public Response deleteLocations(@HeaderParam(HttpHeaders.AUTHORIZATION) String authorization) {
        // On vérifie que l'utilisateur est correction authentifié.
        Status status = checkAuthorization(authorization);
        if (null != status) {
            return Response.status(status).build();
        }

        this.businessLocal.deleteLocations();
        return Response.ok().build();
    }
    ```
    
    On peut maintenant gérer les requêtes de création d'une location, soit à partir d'un formulaire HTML (on utilise alors des "form parameters"), soit en envoyant directement l'objet `LocationBean`, en JSON ou en XML :
    
    ```java title="☕ Code Java - Les verbes POST"

    /**
     * Service n°5 : Permet de créer une location, via un formulaire Web.
     * 
     * Dans postman, on peut tester cette méthode en ajoutant les paramètres dans le
     * "body", en sélectionnant "x-www-form-urlencoded".
     */
    @POST
    @Path("/locations")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createLocation(@HeaderParam(HttpHeaders.AUTHORIZATION) String authorization,
            @FormParam("price") Double nightPrice, @FormParam("address") String address,
            @FormParam(value = "city") String city, @FormParam("zipCode") String zipCode) {
        // On vérifie que l'utilisateur est correction authentifié.
        Status status = checkAuthorization(authorization);
        if (null != status) {
            return Response.status(status).build();
        }

        this.businessLocal.addLocation(new LocationBean(nightPrice, address, city, zipCode));

        return Response.ok().build();
    }

    /**
     * Service n°5 : Permet de créer une location, en envoyant directement le "bean"
     * en XML ou en JSON dans le corps de la requête.
     * 
     * Dans postman, on peut tester cette requête en sélectionnant "raw" (dans
     * l'onglet "body"), puis JSON ou XML selon le cas (cf TP3).
     */
    @POST
    @Path("/locations")
    @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response createLocation(@HeaderParam(HttpHeaders.AUTHORIZATION) String authorization, LocationBean bean) {
        // On vérifie que l'utilisateur est correction authentifié.
        Status status = checkAuthorization(authorization);
        if (null != status) {
            return Response.status(status).build();
        }

        this.businessLocal.addLocation(bean);

        return Response.ok().build();
    }
    ```
    
    On peut maintenant gérer les requêtes de mise d'une location (totale, c'est-à-dire tous les champs, ou partielle, c'est-à-dire seulement certains champs).
    
    ```java title="☕ Code Java - Les verbes PUT et PATCH"

    /**
     * Service n°6 : Mise à jour totale.
     * 
     * On indique l'identifiant (en "path param") de la location qu'on souhaite
     * mettre à jour, ainsi que le bean, qui contient les nouvelles valeurs. Tous
     * les champs en BDD vont être mis à jour.
     */
    @PUT
    @Path("/locations/{id}")
    @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response updateLocation(@HeaderParam(HttpHeaders.AUTHORIZATION) String authorization,
            @PathParam("id") Integer id, LocationBean bean) {
        // On vérifie que l'utilisateur est correction authentifié.
        Status status = checkAuthorization(authorization);
        if (null != status) {
            return Response.status(status).build();
        }

        LocationBean oldBean = this.businessLocal.getLocation(id);
        if (null == oldBean) {
            return Response.status(Status.NOT_FOUND).build();
        }
        bean.setId(id);

        this.businessLocal.updateLocation(bean);

        return Response.ok().build();
    }

    /**
     * Service n°7 : Mise à jour partielle.
     * 
     * On indique l'identifiant (en "path param") de la location qu'on souhaite
     * mettre à jour, ainsi que le bean, qui contient les nouvelles valeurs. Seule
     * les valeurs indiquées (les propriétés non nulles du bean) vont être mises à
     * jour.
     */
    @PATCH
    @Path("/locations/{id}")
    @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response updateLocationPatch(@HeaderParam(HttpHeaders.AUTHORIZATION) String authorization,
            @PathParam("id") Integer id, LocationBean bean) {
        // On vérifie que l'utilisateur est correction authentifié.
        Status status = checkAuthorization(authorization);
        if (null != status) {
            return Response.status(status).build();
        }

        LocationBean oldBean = this.businessLocal.getLocation(id);
        if (null == oldBean) {
            return Response.status(Status.NOT_FOUND).build();
        }
        this.businessLocal.patchLocation(oldBean, bean);

        return Response.ok().build();
    }
    ```