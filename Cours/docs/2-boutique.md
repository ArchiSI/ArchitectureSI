# Deuxième application Web : Ajout d'une connexion à une base de données

**Prérequis** : Savoir installer une base de données MySQL sur son poste de travail, et y accéder (par exemple avec [WAMP](https://www.wampserver.com/)).

Notre première application (boutique du TD2) était très simpliste, puisqu'on ne pouvait sélectionner que trois produits, et que cette liste de produit était figée. Dans cette deuxième version, nous allons ajouter la possibilité d'ajouter de nouveaux produits, ou d'en supprimer, afin d'avoir une liste dynamique. Nous devons donc mettre en place de la persistence, et nous allons sauvegarder ces différents produits dans une base de données. Côté Java, nous allons utiliser l'API JPA (pour _Java Persistence API_), qui va nous permettre de manipuler les objets Java correspondants au données en base sans nous soucier de la gestion des transactions (on pourrait tout gérer manuellement en utilisant les API JDBC, pour _Java Database Connectivity_, et JTA, pour _Java Transaction API_).

Pour utiliser JPA, il va nous falloir un serveur d'application. Tomcat n'en est pas un, c'est uniquement un serveur Web, contenant un **conteneur de _servlet_**. Nous allons pour cela utiliser **WildFly**.

??? note

    WildFly est le nouveau nom de JBoss depuis la version 8.<br><br>
    Il existe de nombreux serveurs d'application, qui offre les mêmes fonctionnalités, Weblogic d'Oracle, Websphere d'IBM, Glassfish ...


## 1. L'architecture de notre application

Voici l'architecture que nous allons mettre en place dans ce projet :

<center>
    ![Architecture Boutique V3](images/schema_archi_V3.png)
</center>

Il s'agit donc d'une architecture 3-tiers. On retrouve en effet le schéma suivant vu en cours :

<center>
    ![Architecture Boutique V3](images/archi-3tiers.png)
</center>

On retrouve le paradigme MVC :

- Le **contrôleur** : ce sont les _servlets_.
- Le **modèle** : c'est la couche métier et la couche BDD.
- La **vue** : ce sont les JSP (et les pages HTML).

Nous allons donc avoir quatre projets Java à créer :

1. Un projet **EJB** pour la couche métier.
2. Un projet **Java** pour le client Java.
3. Un projet **Web dynamique** pour la couche présentation.
4. Un projet **EAR**, qui contiendra le module Web et le module EJB.

## 2. Installation de l'environnement de développement

### 2.1. Création du serveur WildFly

Il faut commencer par installer le serveur WildFly. Pour cela, il faut télécharger la dernière version ici : [https://www.wildfly.org/](https://www.wildfly.org/). Il suffit de cliquer sur **_DOWNLOAD THE ZIP_**, puis de le dézipper dans le dossier 📂`jee`. Dans la suite, on appelera 📂`<WILDFLY_HOME>` le dossier où WildFly a été dézippé (c'est-à-dire 📂`jee/wildfly-x.y.z.Final` normalement).

??? note

    Pour chaque version, WildFly propose deux environnements :

    1. "Jakarta EE 8 Full & Web Distribution", qui implémente Java EE 8 (packages en `javax.*`).
    2. "WildFly Preview EE 9.1 Distribution", qui implémente Jakarta EE 9 (packages en `jakarta.*`).

    On choisit ici d'utiliser le premier, qui implémenente JEE8 (c'est la version téléchargée si on clique directement sur **_DOWNLOAD THE ZIP_**).

Il faut maintenant créer l'**environnement d'éxécution** (_**runtime environment**_ en anglais) correspondant :

1. Aller dans le menu :fontawesome-solid-bars:`Help > Eclipse Marketplace...`. Sélectionner **_Jboss_** dans le champ de recherche, et installer les outils Jboss en cliquant sur <bouton>Install</bouton> :
    <center>
        ![Installation JBOSS 1/3](images/install_jboss1.png)
    </center>
2. Dans le menu suivant, cliquer sur <bouton><u>C</u>onfirm ></bouton> :
    <center>
        ![Installation JBOSS 2/3](images/install_jboss2.png)
    </center>
3. Valider l'installation en acceptant et en cliquant sur _Install anyway_.
4. Il est maintenant possible d'ajouter une _runtime_ Jboss. Aller dans :fontawesome-solid-bars:`Window > Preferences` puis :material-file-tree:`Server > Runtime Environments`.
3. Cliquer sur <bouton>Add...</bouton>, puis dans l'arborescence, aller dans :material-file-tree:`JBoss Community > WildFly 24+ Runtime`.
4. Cocher "Create a new local server", puis cliquer sur <bouton><u>N</u>ext ></bouton> (l'instance du serveur va ainsi directement être disponible dans la vue _Servers_ d'eclipse).
5. Dans _Home Directory_, indiquer 📂`<WILDFLY_HOME>` (le dossier ou le ZIP a été dézippé), sélectionner la jdk installée, puis cliquer sur <bouton><u>N</u>ext ></bouton> et enfin sur <bouton><u>F</u>inish</bouton>.

??? note

    Si on a oublié de cocher la case "Create a new local server", il suffit d'aller dans la vue _server_ dans eclipse, et de le créer manuellement, en indiquant la _runtime_ qu'on vient de créer.

???+ note
    Le serveur apparaît maintenant dans la vue _server_. En ouvrant l'arborescence, dans :material-file-tree:`Filesets > Configuration File`, on constate la présence du fichier 📄`standalone.xml`, qui permet de configurer le serveur :
    <center>
        ![Installation JBOSS 3/3](images/install_jboss3.png)
    </center>
    Par la suite, on aura besoin de modifier ce fichier, il suffira de double cliquer ici pour l'ouvrir dans eclipse.

### 2.2. Accès à la console d'administration

On peut maintenant démarrer le serveur depuis eclipse. Attention à bien vérifier que le serveur tomcat est arrêté. Les deux étant accessibles via le même port par dafaut (8080), il y aura un problème si le tomcat est démarré. Une fois le serveur démarré, on accède à sa page d'accueil via [http://localhost:8080](http://localhost:8080). Lorsqu'on clique sur le lien **_Administration Console_**, un nom d'utilisateur et un mot de passe sont demandés. Il n'y en a pas par défaut, il va falloir en créer un. Pour cela, il faut exécuter le script 📄`<WILDFLY_HOME>/bin/add-user.bat` sous windows, ou 📄`<WILDFLY_HOME>/bin/add-user.sh` sous linux. Nous allons créer un _Management User_ (a), `admin/admin`. Pour des raisons de sécurité, cet utilisateur/mot de passe est **fortement** déconseillé, mais ici, il s'agit uniquement de développement, donc nous allons tout de même le faire. Ce script nous demandera de ce fait de confirmer notre choix. Il faut donc entrer, dans l'ordre, `a / admin / a / admin / yes / admin / <vide> / yes`, comme ci-dessous :
<center>
    ![Add user](images/WildFly-adduser.png)
</center>

On peut maintenant accéder à la console d'administration (même sans redémarrer le serveur).

### 2.3. Configuration de la connexion à la base de données

Nous allons maintenant configurer la _datasource_. Il faut biensûr pour cela qu'une base de données soit accessible. On supposera ici qu'elle l'est sur `localhost` sur le port `3306`. **On suppose également qu'un schéma _boutique_ est créé sur cette instance.**

#### 2.3.1. Configuration du pilote

Il faut commencer par installer le pilote MySQL. Par défaut, il n'y a que le pilote h2. Pour cela, dans le dossier 📂`<WILDFLY_HOME>/modules/system/layers/base/com`, créer un dossier 📂`mysql` puis un dossier 📂`main` dans celui-ci. Il y a deux choses à mettre dans ce dossier :

1. **Le connecteur java-mysql (jar)**
    Il est disponible [ici](https://dev.mysql.com/downloads/connector/j/) [(https://dev.mysql.com/downloads/connector/j/)](https://dev.mysql.com/downloads/connector/j/). À ce lien, sélectionner "_Plateform Independent_", puis télécharger le zip (il n'est pas nécessaire de se connecter). Dans ce zip, seul le fichier 📄`mysql-connector-java-a.b.c.jar` nous interesse, et doit être placé dans 📂`<WILDFLY_HOME>/modules/system/layers/base/com/mysql/main`.
2. **Le fichier `module.xml`**
    Créer ce fichier dans le dossier, en recopiant le contenu ci-dessous dans son contenu (:warning:**attention à adapter la version du jar**) :
    ``` xml title="📄 module.xml" linenums="1"
    <module xmlns="urn:jboss:module:1.5" name="com.mysql">
        <resources>
            <resource-root path="mysql-connector-java-8.0.28.jar" /><!--(1)-->
        </resources>
        <dependencies>
            <module name="javax.api"/>
            <module name="javax.transaction.api"/>
        </dependencies>
    </module>
    ```

    1. Il faut indiquer ici le même nom que celui du jar présent dans ce dossier.

Enfin, ouvrir le fichier 📄`standalone.xml` (il est accessible depuis la vue _server_, dans :material-file-tree:`Filesets > Configuration File`). Dans ce fichier, chercher la balise `<drivers>`. Elle contient uniquement le driver `h2` pour le moment. Nous allons ajouter notre driver `MySQL`. Pour cela, recopier le code suivant :
``` xml title="📄 standalone.xml" linenums="1"
<driver name="mysql" module="com.mysql"/>
```

Le serveur doit maintenant être redémarré. Dans la console d'administration, vérifier que le pilote a bien été installé en allant dans :fontawesome-solid-bars:`Configuration > Subsystems > Datasources & Drivers > JDBC Drivers`. Il doit maintenant y avoir les deux pilotes : `h2` et `mysql`.

#### 2.3.2. Configuration de la source de données

Nous pouvons maintenant crééer la _datasource_. Dans la console d'administration, aller dans :fontawesome-solid-bars:`Configuration > Subsystems > Datasources & Drivers > Datasources`, et cliquer sur "Add Datasource". Il y a 6 écran de configuration :

1. Sélectionner "MySQL".
2. Indiquer `MySqlBoutique` comme Name et `java:/MySqlBoutique` comme JNDI Name.
3. Ne rien changer.
4. Indiquer `jdbc:mysql://localhost:3306/boutique` dans _Connection URL_ (il faut adapter cette URL si le port est différent, et si le schéma créé sur la BDD n'est pas `boutique`), et saisir le _User Name_ et l'éventuel _Password_.
5. Tester la connexion.
6. Valider la création de la _datasource_.


## 3. Développement de l'application

Comme nous l'avons vu précédemment, nous avons trois projets Java à créer.

### 3.1. Le projet EJB

Nous allons créer le projet qui va permettre d'implémenter la couche métier.

1. Dans le vue _Project Explorer_, cliquer sur le bouton droit de la souris, puis aller dans :material-mouse:`New > Project...`, puis dans :material-file-tree:`EJB > EJB Project`.
2. Entrer le nom du projet (**_BoutiqueEJB_**), sélectionner la bonne _runtime_, décocher "Add Project to an EAR", puis cliquer sur <bouton><u>F</u>inish</bouton> :
    <center>
        ![Création BoutiqueEJB](images/BoutiqueEJB-1.png)
    </center>

Pour pouvoir utiliser _JPA_, il faut l'indiquer dans les "facettes" du projet. Pour cela, il faut faire un clic droit sur le projet **_BoutiqueEJB_**, cliquer sur :material-mouse:`Properties` et aller dans :material-file-tree:`Project Facets`, cocher la case _JPA_ et enfin cliquer sur <bouton>Apply and Close</bouton> :
<center>
    ![Création BoutiqueEJB](images/BoutiqueEJB-2.png)
</center>

Dans la vue _Entreprise Explorer_, le fichier 📄`persistence.xml` apparaît. Il va nous permettre de gérer la persistence des _beans_ Java.

#### 3.1.1. Création du modèle (première partie)

Nous allons créer un _bean_, une entité JPA plus précisément, pour représenter un produit. Il va contenir trois champs :

1. `id` : un `Integer`, qui permet de l'identifier de manière unique.
2. `name` : une `String` codant le nom du produit.
3. `price` : un `Double` contenant le prix de ce produit.

Pour créer cet objet, il suffit de faire un clic droit sur le projet, :material-mouse:`New > JPA Entity`. Indiquer `edu.boutique.model` comme package, et `ProductBean` comme classe, puis cliquer sur <bouton><u>N</u>ext ></bouton>.
<center>
    ![Création BoutiqueEJB](images/BoutiqueEJB-JPA-1.png)
</center>
Ajouter ensuite les trois champs listés ci-dessus en utilisant la bouton <bouton><u>A</u>dd...</bouton> (attention à bien cocher le champ `id` comme _key_), puis clique sur <bouton><u>F</u>inih</bouton>
<center>
    ![Création BoutiqueEJB](images/BoutiqueEJB-JPA-2.png)
</center>
Nous allons également préciser la stratégie de gestion du champ `id`. On souhaite qu'il soit automatiquement incrémenté (d'un en un). On ajoute donc, juste en dessous de l'annotation `@id`, l'annotation `@GeneratedValue(strategy = GenerationType.IDENTITY)`. On a donc :
``` java title="☕ Code Java - Entité ProductBean" linenums="1"
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;
```

Dans le fichier 📄`persistence.xml`, la ligne ci-dessous a été automatiquement ajoutée :
``` xml title="📄 persistence.xml" linenums="1"
<class>edu.boutique.model.ProductBean</class>
```

Toujours dans ce fichier :

1. Nous allons indiquer que nous utilisons l'API JTA pour gérer les transactions. Pour cela, il faut ajouter l'attribut `transaction-type="JTA"` dans la balise `<persistence-unit>`.
2. À l'intérieur de cette balise `<persistence-unit>`, nous allons ajouter la balise fille `<jta-data-source>java:/MySqlBoutique</jta-data-source>` pour faire le lien avec la _datasource_ créée précédemment (on indique le nom JNDI).
3. Enfin, on indique la stratégie utilisée au redémarrage du serveur. Ici, nous sommes en mode "développement", nous allons donc supprimer et re-créer la table correspondante à chaque redémarrage. Pour cela, aller dans l'onglet _Schema Generation_, et indiquer _Drop and Create_ dans la propriété _Database action_.

Le fichier doit maintenant ressembler à cela :
``` xml title="📄 persistence.xml" linenums="1"
<?xml version="1.0" encoding="UTF-8"?>
<persistence version="2.2"
	xmlns="http://xmlns.jcp.org/xml/ns/persistence"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence http://xmlns.jcp.org/xml/ns/persistence/persistence_2_2.xsd">
	<persistence-unit name="BoutiqueEJB" transaction-type="JTA"><!--(1)-->
		<jta-data-source>java:/MySqlBoutique</jta-data-source><!--(2)-->
		<class>edu.boutique.model.ProductBean</class>
		<properties>
			<property
				name="javax.persistence.schema-generation.database.action"
				value="drop-and-create" /><!--(3)-->
		</properties>
	</persistence-unit>
</persistence>
```

1. Plus d'infos sur `transaction-type` [ici](https://stackoverflow.com/questions/17331024/persistence-xml-different-transaction-type-attributes).
2. Ici, il faut indiquer le même nom JNDI que celui précisé à la création de la _Data Source_ précédemment.
3. Cela signifie qu'à chaque démarrage du serveur, les tables correspondantes sont supprimées et recréées. On évitera bien sûr de laisser cette option en environnement de production ...

Il est maintenant temps de tester. Pour cela, on démarre le serveur (si ce n'est pas déjà fait), puis on publie le projet EJB sur le serveur. Pour cela, on fait un clic droit sur le serveur, :material-mouse:`Add and Remove...`, et on déploie **_BoutiqueEJB_**. On vérifie que la table `ProductBean` est bien créée en BDD (et vide), avec les trois champs indiqués.

#### 3.1.2. Création du modèle (2ème partie)

Il faut maintenant reprendre le modèle que nous avions dans la première version, en le faisant évoluer pour pouvoir choisir des produits dans une liste dynamique. Comme dans la première version, nous allons créer un _bean_ `CartBean`. Il contiendra toujours les champs `cartPrice`, `shippingCost` et `totalPrice`, mais les champs `penNumber`, `feltNumber` et `rubberNumber` vont être remplacés par une `Map` dont la clef est un objet `ProductBean`, et la valeur un `Integer` indiquant le nombre de sélection de l'article en question. Pour cela, nous allons devoir surcharger les méthodes `equals` et `hashCode` de `ProductBean` :

``` java title="☕ Code Java - Entité ProductBean" linenums="1"
@Override
public boolean equals(Object obj) {
    return this.getId().equals(((ProductBean) obj).getId()); //(1)
}

@Override
public int hashCode() {
    return this.id;
}
```

1. Deux `CartBean` sont égaux si, et seulement si, leurs identifiants sont égaux.

Voici les champs du _bean_ `CartBean` (il faut également ajouter les `getters/setters` sauf pour `serialVersionUID`) :

``` java title="☕ Code Java - Bean CartBean" linenums="1"
private static final long serialVersionUID = 1L;

private Map<ProductBean, Integer> products = new HashMap<ProductBean, Integer>(); //(1)
private Double cartPrice = 0D;
private Double shippingCost = 0D;
private Double totalPrice = 0D;
```

1. Pour rappel, grossièrement, une `Map` est une liste de couple (clef, valeur). Les clefs sont ici les produits, et les valeurs le nombre de fois où ils ont été sélectionnés.

Nous allons également ajouter une méthode `initProducts`, qui va pendre en paramètre une liste de `ProductBean` (que nous allons récupérer en BDD), afin d'initialiser le panier (avec des 0 pour chaque produit, car il n'a pas encore été sélectionné.) :

``` java title="☕ Code Java - Bean CartBean - Méthode initCart" linenums="1"
public void initProducts(List<ProductBean> productList) {
    this.products = new HashMap<ProductBean, Integer>();
    for (ProductBean product : productList) {
        this.products.put(product, 0);
    }
}
```


#### 3.1.3. Création d'un EJB session

Nous avons fini de créer les objets de notre modèle, nous allons maintenant pouvoir développer les _EJB session_, c'est-à-dire les services métier.

Créer un EJB revient à créer trois choses :

1. Une interface _remote_. C'est elle qui est utilisée lorsque l'EJB est appelé via RMI (_Remote Method Invocation_), par exemple dans notre cas par le client Java.<br>
    ➡️ Créer l'interface `edu.boutique.ejb.BoutiqueRemote` :

    ``` java title="☕ Code Java - EJB interface remote" linenums="1"
    package edu.boutique.ejb;

    import java.util.List;

    import javax.ejb.Remote;

    import edu.boutique.model.CartBean;
    import edu.boutique.model.ProductBean;

    @Remote
    public interface BoutiqueRemote {
        // Permet de créer un produit en BDD.
        public ProductBean createProduct(ProductBean product);

        // Permet de récupérer un produit avec sont id.
        public ProductBean getProduct(Integer id); //(1)

        // Permet de récupérer la liste de tous les produits en BDD.
        public List<ProductBean> getProducts();
	
        // Permet de modifier un produit donné (son nom et/ou son prix).
        public ProductBean modifyProduct(Integer id, String name, Double price);

        // Permet de calculer les diférents coûts sur un panier donné.
        public CartBean computePrices(CartBean cart);
    }
    ```

    1. On peut se demander pourquoi `getProduct` renvoie un `ProductBean` et pas un `void`. La raison est que la valeur de l'`id` est donnée à la création de l'objet en base. Ainsi, l'objet renvoyé est complété avec cette valeur.

    L'annotation `@Remote` indique qu'il s'agit de l'interface _remote_.

    Cette inteface permet de lister tous les services métiers fournis par cet _EJB_.

2. Une interface _local_. C'est elle qui est utilisée lorsque l'EJB est appelé en local, par exemple dans notre cas par le projet Web (qui est sur le même serveur WildFly que le projet EJB).<br>
    ➡️ Créer l'interface `edu.boutique.ejb.BoutiqueLocal` : Elle contient le même code (les mêmes signatures des mêmes méthodes), la seule différence est l'utilisation de l'annotation `@Local` à la place de `@Remote`.

3. Une classe implémentant ces deux interfaces. C'est le code métier.<br>
    ➡️ Créer la classe `edu.boutique.ejb.BoutiqueEJBSession`, qui implémente les deux interfaces ci-dessus.<br>
    On ajoute l'annotation `@Stateless` (notre EJB n'a pas besoin de mémoire ici). C'est bon, notre EJB est créé, il n'y a plus qu'à l'implémenter.<br>
    Pour effecter les requêtes en BDD, nous allons utiliser l'objet `EntityManager` de l'API JPA. Pour faire le lien avec le 📄`persistence.xml`, il faut ajouter l'annotation `@PersistenceContext(unitName = "BoutiqueEJB")`. La valeur de `unitName` doit être la même que celle dans le fichier 📄`persistence.xml` (on peut très bien avoir pluisuers _datasources_ vers plusieurs bases de données dans un même projet).<br>
    Sur cet objet, nous allons utiliser les méthodes `persist`, `find` et `createQuery` qui permettent respectivement de créer un objet en base, de récupérer un objet avec son identifiant et de créer une requête (ici pour récupérer tous les produits). Voici donc le code de cette classe :
    ``` java title="☕ Code Java - EJB BoutiqueEJBSession" linenums="1"
    package edu.boutique.ejb;

    import java.util.List;

    import javax.ejb.Stateless;
    import javax.persistence.EntityManager;
    import javax.persistence.PersistenceContext;
    import javax.persistence.Query;

    import edu.boutique.model.CartBean;
    import edu.boutique.model.ProductBean;

    @Stateless
    public class BoutiqueEJBSession implements BoutiqueLocal, BoutiqueRemote {

        @PersistenceContext(unitName = "BoutiqueEJB")
        private EntityManager em;

        @Override
        public ProductBean createProduct(ProductBean product) {
            em.persist(product);
            return product;
        }

        @Override
        public ProductBean getProduct(Integer id) {
            ProductBean product = em.find(ProductBean.class, id);
            if (null == product) {
                throw new RuntimeException("Ce produit n'existe pas en base.");
            }
            return product;
        }

        @SuppressWarnings("unchecked")
        @Override
        public List<ProductBean> getProducts() {
            Query requete = em.createQuery("select p from ProductBean p");
            return requete.getResultList();
        }

        @Override
        public ProductBean modifyProduct(Integer id, String name, Double price) {
            ProductBean product = getProduct(id);
            product.setName(name);
            product.setPrice(price);
            return product;
        }

        @Override
        public CartBean computePrices(CartBean cart) {
            // Calcul du prix du panier.
            Double cartPrice = 0D;
            for (ProductBean product : cart.getProducts().keySet()) {
                cartPrice += product.getPrice() * cart.getProducts().get(product);
            }

            // Calcul des frais de livraison.
            Double shippingCost = 5D;
            if (cartPrice >= 10D) {
                shippingCost = 0D;
            }

            // Calcul du prix total.
            Double totalPrice = cartPrice + shippingCost;

            // Mise à jour du panier.
            cart.setCartPrice(cartPrice);
            cart.setShippingCost(shippingCost);
            cart.setTotalPrice(totalPrice);
            return cart;
        }

    }
    ```

On peut maintenant déployer à nouveau le projet sur le serveur, et vérifier dans les logs que l'EJB session a bien été déployé. Il doit y avoir une ligne ressemblant à cela :

``` title="📋 Logs du serveur WildFly"
ejb:/BoutiqueEJB/BoutiqueEJBSession!edu.boutique.ejb.BoutiqueRemote
```

??? note

    Les projets déployés sur le seveur sont disponibles dans le dossier suivant : 📂`<WILDFLY_HOME>/standalone/deployments`. Pour chaque projet déployé (il peut y en avoir plusieurs), il y a un dossier à son nom, ainsi qu'un fichier avec l'extention `.deployed` si tout s'est bien passé.<br><br>
    Lorsqu'on créé un nouveau _workspace_ dans eclipse, il peut rester des anciens projets déployés ici. On peut venir les supprimer manuellement pour repartir d'un environnement "propre".

Ceci correspond à l'identifant JNDI avec lequel nous pourrons appeler l'EJB depuis l'exterieur.


### 3.2. Le projet Java client

Nous allons maintenant créer un projet Java indépendant, qui va appeler cet EJB, et plus précisément l'interface _remote_.

#### 3.2.1. Création du client

Depuis la vue _Enterprise Explorer_, créer un nouveau Projet Java (sélectionner _Java Project_) nommé **_ClientJava_**, sélectionner la bonne _JDK_, décocher la case "_Create module-info.java file_", et enfin cliquer sur <bouton>Finish</bouton>.

Nous allons avoir besoin du modèle (c'est-à-dire des _beans_), ainsi que de l'interface _remote_. Pour cela, nous allons ajouter le projet **_BoutiqueEJB_** dans le _classpath_ du projet **_ClientJava_**. Nous verrons dans une prochaine version comment faire pour ne pas avoir à importer **tout** le projet EJB :

<center>
    ![Classpath projet ClientJava](images/ClientJava-1.png)
</center>

Dans ce projet, créer la classe `edu.boutique.client.Client`, **en cochant la case pour avoir la méthode** `main`. Cette classe va contenir une méthode `getBusiness` permettant de manipuler l'EJB session, via son nom JNDI. La méthode `main` va appeler cette méthode, et effectuer quelques opérations pour tester le bon fonctionnement de l'EJB. Voici le code à copier :

``` java title="☕ Code Java - Client" linenums="1"
package edu.boutique.client;

import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import edu.boutique.ejb.BoutiqueRemote;
import edu.boutique.model.CartBean;
import edu.boutique.model.ProductBean;

public class Client {

	public static void main(String[] args) throws NamingException {
		BoutiqueRemote business = getBusiness();

		// Ajout du stylo en BDD.
		ProductBean stylo = new ProductBean();
		stylo.setName("Stylo");
		stylo.setPrice(1.2D);
		business.createProduct(stylo);

		// Ajout du feutre en BDD.
		ProductBean feutre = new ProductBean();
		feutre.setName("Feutre");
		feutre.setPrice(1.8D);
		business.createProduct(feutre);

		// Liste des produits :
		System.out.println("Liste des produits :");
		List<ProductBean> products = business.getProducts();
		for (ProductBean product : products) {
			System.out.println(product.getName() + " coûte " + product.getPrice());
		}

		// On modifie un produit :
		business.modifyProduct(1, "Stylo 4 couleurs", 3.2D);

		// Liste des produits :
		System.out.println("Liste des produits :");
		products = business.getProducts();
		for (ProductBean product : products) {
			System.out.println(product.getName() + " coûte " + product.getPrice());
		}

		// On simule un panier.
		CartBean cart = new CartBean();
		cart.getProducts().put(stylo, 2);
		cart.getProducts().put(feutre, 4);
		cart = business.computePrices(cart);
		System.out.println("Panier : " + cart.getCartPrice());
		System.out.println("Frais de livraison : " + cart.getShippingCost());
		System.out.println("Total : " + cart.getTotalPrice());
	}

	/**
	 * 
	 * @return L'EJB session pour manipulation.
	 * @throws NamingException Si l'EJB n'est pas trouvé dans l'annuaire JNDI.
	 */
	private static BoutiqueRemote getBusiness() throws NamingException {
		Properties jndiProperties = new Properties();
		jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
		jndiProperties.put(Context.PROVIDER_URL, "remote+http://localhost:8080");
		Context ctx = new InitialContext(jndiProperties);

		String appName = "/";
		String projectName = "BoutiqueEJB/";
		String className = "BoutiqueEJBSession!";
		String remote = BoutiqueRemote.class.getName();

		String url = "ejb:" + appName + projectName + className + remote;

		BoutiqueRemote business = (BoutiqueRemote) ctx.lookup(url);

		return business;
	}
}
```

#### 3.2.2. Test de l'EJB session avec le projet ClientJava

Pour tester ce client, il suffit de faire un clic droit sur la classe `Client`, et de :material-mouse:`Run As > Java Application`. En faisant cela, on obtient l'erreur suivante :

``` title="📋 Logs du serveur WildFly"
java.lang.ClassNotFoundException: org.wildfly.naming.client.WildFlyInitialContextFactory
```

En effet, si on utilise comme client de l'EJB une projet Java classique, il faut importer un JAR fourni par WildFly. Dans le _classpath_ du projet **_ClientJava_**, il faut ajouter le JAR présent ici : 📄`<WILDFLY_HOME>/bin/client/jboss-client.jar` (il faut cliquer sur <bouton>Add E<u>x</u>ternal JARs...</bouton>).

Lorsqu'on relance le test, on obtient le résultat suivant :

``` title="📋 Logs du projet ClientJava (main de la classe Client)"
Liste des produits :
Stylo coûte 1.2
Feutre coûte 1.8
Liste des produits :
Stylo 4 couleurs coûte 3.2
Feutre coûte 1.8
Panier : 9.6
Frais de livraison : 5.0
Total : 14.6
```

On peut également vérifier que les deux produits ont bien été créés dans la table en BDD.

:warning: Attention, à chaque fois qu'on relance le client Java, un stylo et un feutre sont ajoutés dans la liste des produits disponibles.

### 3.3. Le projet Web dynamique

Nous allons maintenant créer le projet Web, qui permet de gérer le site web. Dans la vue _Entreprise Explorer_, créer un nouveau _Dynamic Web Project_, nommé **_BoutiqueWeb_**, vérifier la _runtime_ utilisée, décocher "_Add project to an EAR_", cliquer sur <bouton><u>N</u>ext</bouton> deux fois, cocher "_Generate web.xml deployment descriptor_" puis cliquer sur <bouton><u>F</u>inish</bouton>.

<center>
    ![Création projet Web](images/BoutiqueWeb-1.png)
</center>

Tout comme le client Java créé précédemment, nous allons avoir besoin d'accéder aux _beans_ du modèle ainsi qu'à l'interface de l'EJB (la _local_ cette fois-ci). Il faut donc ajouter une dépendance vers **_BoutiqueEJB_** dans le _classpath_ du projet.

Nous allons déployer une _servlet_. Créer une classe `StoreServlet`, qui hérite de `HttpServlet` de l'API _Servlet_. Nous allons nommer notre _servlet_ "storeServlet" et l'appeler via l'URL "e-store.fr". Il faut donc ajouter l'annotation suivante pour la déployer :

``` java
@WebServlet(name = "store", urlPatterns = {"/store.fr"})
```

Surcharger la méthode `doGet`, qui va correspondre au premier affichage de la page. Il faut donc utiliser l'EJB pour récupérer la liste des produits présents en BDD. Pour cela, nous allons créer un champ correspondant à l'EJB :

``` java
@EJB
private BoutiqueLocal metier;
```

L'annotation `@EJB` permet d'effectuer l'injection de dépendances, et d'utiliser notre EJB. Ici, comme notre projet Web va être sur le même serveur que le projet EJB, il n'y a pas besoin de passer par l'annuaire JNDI.

La méthode `doGet` contient donc :

``` java title="☕ Code Java - Servlet - doGet" linenums="1"
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    // Récupération de la liste des produits en BDD.
    List<ProductBean> products = metier.getProducts();

    // Création du panier, avec la liste des produits sélectionnables
    CartBean cart = new CartBean();
    cart.initProducts(products);

    // On place le panier dans la requête pour pouvoir l'utiliser dans la JSP.
    request.setAttribute("USER_CART", cart);

    // On redirige vers la bonne vue : store.jsp
    RequestDispatcher dispatcher = request.getRequestDispatcher("store.jsp");
    dispatcher.forward(request, response);
}
```

Il ne nous reste maintenant plus qu'à créer la vue. Pour cela, on peut commencer par récupérer le fichier `store.jsp` que nous avions créé dans le projet `Boutique`. Il y a uniquement la partie _Sélection des produits_ à modifier, puisque nous n'avons plus une liste statique de 3 objets, mais une liste dynamique récupérée en BDD. Nous allons utiliser la balise `<c:forEach>` fourni par la bibilothèque [`core`](https://www.jmdoudoux.fr/java/dej/chap-jstl.htm#jstl-3) de la _JSTL_, qui permet d'effectuer une boucle sur une liste d'objets (ou sur une `Map`). Nous utiliserons également la balise `<c:out value=${variable}>` de cette même bibliothèque, qui permet d'afficher le contenu de `variable`. Pour utiliser cette bibliothèque `core`, il faut ajouter la ligne suivante au début de la JSP :

``` jsp
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><!--(1)-->
```

1. On utilise les balises `c:forEach` et `c:out` car le préfixe indiqué à l'import de la bibliothèqe est `c`. C'est la valeur "classique" de ce préfixe.

``` jsp
<c:forEach var="product" items="${requestScope.USER_CART.cart}">
```

- L'attribut `items` indique la liste (dans l'exemple ci-dessus, on va donc appeler la méthode `getCart` sur le bean `CartBean` présent en requête avec la clef `USER_CART`).
- L'attribut `var` permet de donner un nom à chaque objet de la liste. Ici, la liste est une `Map`, donc `var` correspond à un couple (produit, nombre de sélection). On accède au produit en faisant `${product.key}` et au nombre de sélection en faisant `${product.value}`.

Pour chaque produit disponible, il faut ajouter un champ `<input type="integer">`, identifier par l'attribut `name`. Cet attibut aura pour valeur `nb_of_product_1` pour le premier produit, `nb_of_product_2` pour le deuxième, etc.

En résumé, la partie "_Liste des produits_" devient donc :

``` jsp
<form action="e-store.fr" method="post">
    <fieldset>
        <legend>Liste des produits</legend>
        <table>
            <tr>
                <th>Produit</th>
                <th>Prix</th>
                <th></th>
            </tr>
            <c:forEach var="product" items="${requestScope.USER_CART.products}">
                <tr>
                    <td><c:out value="${product.key.name}" /></td>
                    <td><c:out value="${product.key.price}" /></td>
                    <td><input type="number"
                        name="nb_of_product_${product.key.id}" value="${product.value}" />
                </tr>
            </c:forEach>
        </table>
    </fieldset>
    <input type="submit" value="Valider" />
</form>
```

Il nous reste à implémenter la méthode `doPost` de la servlet `StoreServlet`, qui sera appelée lorsque l'utilisateur clique sur <bouton>Valider</bouton>. Cette méthode commence et se termine de la même façon que la méthode `doGet`. On commence par récupérer la liste des produits en BDD, avec laquelle on initialise le panier, puis on termine en plaçant ce panier dans la requête, et en redirigeant l'utilisateur vers la bonne vue. Il y a cependant un traitement important au milieu :

1. On récupère tous les paramètres de la requête.
2. Parmi tout ceux-ci, on ne va traiter que ceux qui nous intéresse, c'est-à-dire ceux qui commencent par `nb_of_product_`.
3. Pour ceux-ci, on récupère leurs valeurs, c'est le nombre de sélection de cet objet.
4. À partir de la clef `nb_of_product_X`, on récupère la valeur de `X`, c'est-à-dire l'identifiant du produit.
5. On met à jour la `Map` products du panier avec ces deux valeurs (la clef est le produit correspondant à `X` et la valeur est le nombre de sélection).
6. Une fois tous les produits sélectionnés traités, on calcule les différents prix (avec la méthode `computePrices` de l'_EJB_).

On obtient donc le code suivant, pour la méthode `StoreServlet.doPost()` :

``` java title="☕ Code Java - Servlet - doPost" linenums="1"
@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    // Récupération de la liste des produits en BDD.
    List<ProductBean> products = metier.getProducts();

    // Création du panier, avec la liste des produits sélectionnables
    CartBean cart = new CartBean();
    cart.initProducts(products);

    // On récupère tous les paramètres de la requête.
    Map<String, String[]> parameters = request.getParameterMap();
    for (String parameter : parameters.keySet()) {
        // On parcourt ces paramètres, en récupérant tout ceux commençant par
        // "nb_of_product_" :
        if (parameter.startsWith("nb_of_product_")) {
            // On récupère le nombre de sélection pour l'objet courant.
            Integer numberOfProduct = Integer.parseInt(parameters.get(parameter)[0]);
            // On récupère l'id du produit ...
            Integer id = Integer.parseInt(parameter.replaceAll("nb_of_product_", ""));
            // ... à partir duquel on créé un objet ProductBean, pour le retrouver dans la
            // Map :
            ProductBean product = new ProductBean();
            product.setId(id);
            // On ajoute ce produit dans le panier :
            cart.getProducts().replace(product, numberOfProduct);
        }
    }

    // Calcul de coûts du panier :
    cart = metier.computePrices(cart);

    // On place le panier dans la requête pour pouvoir l'utiliser dans la JSP.
    request.setAttribute("USER_CART", cart);

    // On redirige vers la bonne vue : store.jsp
    RequestDispatcher dispatcher = request.getRequestDispatcher("store.jsp");
    dispatcher.forward(request, response);
}
```

Nous en avons terminé avec la partie Web, nous pouvons passer à l'EAR.


### 3.4. Le projet EAR

Nous avons créé un module _Web_ et un module _EJB_ qui font partie du même projet. Pour les déployer ensemble sur le serveur, il faut créer un projet _EAR_.

??? note

    Pour être déployés, les projets Java doivent être mis sous forme d'archives. Il existe principalement trois types d'archives, qui sont en fait des fichiers ZIP :

    1. Les **_JAR_**, pour **_Java ARchive_** : ils contiennent des classes Java et des métadonnées. Ils permettent de déployer des projets Java classiques ou des projets _EJB_.
    2. Les **_WAR_**, pour **_Web application ARchive_** : ils contiennent des classes Java, des métadonnées, et tout le contenu Web (HTML, CSS, JavaScript, images, ...). Ils permettent de déployer des projets _Web_.
    3. Les **_EAR_**, pour **_Entreprise Application aRchive_** : ils contiennent plusieurs modules (au moins un). Il peut y avoir un ou plusieurs modules _Web_, un ou plusieurs modules _EJB_ et un ou plusieurs modules Java.

Dans le menu _Entreprise Explorer_, faire un clic droit, puis :material-mouse:`New > Project...`, puis :material-file-tree:`Java EE > Entreprise Application Project`, puis <bouton><u>N</u>ext ></bouton>. Nommer le projet **_BoutiqueEAR_**, vérifier la _runtime_ utilisée, puis cliquer sur <bouton><u>N</u>ext ></bouton> :

<center>
    ![Création projet EAR](images/BoutiqueEAR-1.png)
</center>

Sélectionner les projets **_BoutiqueEJB_** et **_BoutiqueWeb_**, puis cliquer sur <bouton><u>F</u>inish</bouton>. Il faut maintenant déployer l'_EAR_. Pour cela, faire un clic droit sur le serveur, :material-mouse:`Add and Remove...`. Supprimer le projet **_BoutiqueEJB_**, et ajouter le projet **_BoutiqueEAR_**, puis cliquer sur <bouton><u>F</u>inish</bouton>.

Dans les logs du serveur, vérifier que l'_EJB_ a bien été déployé, en vérifiant la présence de la ligne suivante dans les logs :

``` title="📋 Logs du serveur WildFly"
ejb:BoutiqueEAR/BoutiqueEJB/BoutiqueEJBSession!edu.boutique.ejb.BoutiqueRemote
```

On constate au passage que le nom JNDI a légèrement changé, puisqu'on voit maintenant apparaître le nom du projet _EAR_.

TODO :

1. Expliquer le nom JNDI de l'EJB
2. Dire qu'il faut maintenant ajouter le projet EAR dans le nom JNDI pour que le client fonctionne.
