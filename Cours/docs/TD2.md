# TD2 - Première application Web : Une boutique en ligne

L'objectif de ce TD est de créer un premier projet Web, qui respecte le paradigme MVC (modèle, vue, contrôleur).

## 1. Présentation du projet

L'idée est de créer un site web dynamique, en fait une boutique en ligne, où l'on peut sélectionner différents objets (dans cette première version, uniquement des stylos, des feutres et des gommes), en quantité souhaitée. Notre application indique ensuite le coût du panier, les éventuels frais de livraison, et le coût total de la commande. Trois règles de gestions (RG) doivent pour cela être mises en place :

* RG1 : Calcul du prix du panier, sachant que :
    * un styo coûte 1,20€,
    * un feutre coûte 1,80€,
    * une gomme coûte 2€.
* RG2 : Les frais de livraison sont de 5€ si la commande est de moins de 15€, et sont offerts sinon.
* RG3 : Le prix total de la commande est la somme du prix du panier et des évenutels frais de livraison.

Voici la page (il n'y en a qu'une) de notre site :
<center>
    ![Page d'accueil](images/BoutiqueWebV1_a.png)
</center>

Le client peut sélectionner le nombre de chaque objet désiré. Lorsqu'il clique sur <bouton>Valider</bouton>, la partie `Récapitulatif du panier` se met à jour, en appliquant les 3 RG définies ci-dessus :
<center>
    ![Page de l'application](images/BoutiqueWebV1_b80.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    ![Page de l'application](images/BoutiqueWebV1_c2-80.png)
</center>




## 2. Conception de l'application

Notre application doit respecter le paradigme MVC. Ici, elle est très simple, et respecter ce paradigme va rendre notre code plus complexe. Mais cela nous fera gagner beaucoup de temps par la suite, lorsqu'elle va évoluer ! Si tout le monde respecte les mêmes normes, le code est _rangé_ de la même façon pour tout le monde, et on s'y retrouve donc beaucoup plus rapidement et facilement lorsqu'il faut ajouter une fonctionnalité ou corriger un bug dans une application développée par quelqu'un d'autre.

Pour rappel, voici un schéma de ce paradigme :

<figure markdown>
  ![Schéma MVC](images/schema_MVC_5.png)
  <figcaption>Schéma du paradigme MVC</figcaption>
</figure>


Pour l'instant, il faut donc déterminer ce qui fait partie du modèle, de la vue, et du controlêur :

1. **Le modèle**<br/>
  Il n'y a pour l'instant qu'une seule classe. C'est le panier. C'est un **_bean_** Java, qui contient plusieurs attributs (et les _getters_/_setters_ associés) :
    * le nombre de stylos commandés,
    * le nombre de feutres commandés,
    * le nombre de gommes commandées,
    * le prix du panier,
    * les frais de livraison,
    * et le prix total de la commande.
2. **La vue**<br/>
  C'est l'affichage de la page Web permettant de sélectionner les quantités désirées de ces trois produits, et qui indique le prix du panier, les frais de livraison et le coût total de la commande.
3. **Le contrôleur**<br/>
  C'est lui qui affiche la vue (avec aucun produit sélectionné initialement) la première fois, et qui met à jour le modèle, avec les informations saisies par l'utilisateur, puis qui ré-affiche la vue mise à jour.<br/>
4. Il y a en fait une 4ème couche à notre application : **La partie métier**<br/>
  Elle est appelée par le contrôleur lorsque l'utilisateur a saisi des données. Elle s'occupe de l'implémentation des trois règles de gestions citées plus haut.<br>
  Dans le paradigme MVC, on peut considérer que cette couche métier fait partie du modèle, puisque son rôle et de mettre ce dernier à jour.


## 3. Développement de l'application - V1.0

Ca y est, on peut commencer à développer notre application.

### 3.1 Création du projet dans eclipse

Il faut commencer par créer un projet Web dynamic, qu'on appelera **_Boutique_**. Il faut penser à vérifier le _runtime_ utilisé (tomcat9) et à cocher la case _Generate web.xml deployment descriptor_ :

<center>![Nouveau projet Web dynamique](images/newDynamicWebProject-b.png)</center>

??? bug "Pas de web.xml ?"

    Si vous avez oublié de cocher cette case, il faudra soit supprimer ce projet et recommencer à l'étape 1, soit ajouter manuellement le fichier 📄`web.xml`, **en respectant la structure attendue**.

### 3.2. Implémentation du modèle

Dans cette première version, le modèle n'est constitué que d'une seule classe, qui modéliser le panier. Il s'agit d'un _bean_.

1. Dans la vue `Project Explorer`, sur le projet **_Boutique_**, faire un clic droit, puis :material-mouse:`New > Other`. Dans l'arborescence, naviguer ensuite vers :material-file-tree:`Java > Class`.
2. Cliquer sur <bouton>Next ></bouton>.
3. Indiquer le package : `edu.boutique.model`.

    ??? warning

        On ne laisse jamais le package par défaut lorsqu'on développe une application avec JEE.

4. Indiquer le nom de classe : `CartBean`.

    ???+ success "Remarque"

        Il est pratique d'indiquer ce qu'est la classe dans son nom, ici, un _bean_.

5. Cette classe est un _bean_, c'est-à-dire qu'elle implémente l'interface `java.io.Serializable`. Il faut donc l'ajouter (avec le bouton <bouton>Add...</bouton>).
6. Dans ce _bean_, il faut créer tous les attributs :
  ``` java linenums="1"
  private Integer penNumber;
  private Integer feltNumber;
  private Integer rubberNumber;
  private Double cartPrice;
  private Double shippingCost;
  private Double totalPrice;
  ```
7. Il faut ensuite générer les _getters/setters_ en faisant un clic droit avec la souris, puis :material-mouse:`Source > Generate Getters and Setters...`

    ???+ success "Raccourci"

        Il peut être très utile de mémoriser le raccourci qui amène directement à :material-mouse:`Source` : c'est ++alt+shift+"S"++.

8. Enfin, pour ne pas avoir de _warning_, on ajoute le champ static tout au début de la classe :
  ``` java linenums="1"
  private static final long serialVersionUID = 1L;
  ```

???+ success "Astuce : bonnes pratiques"

    Prenez l'habitude, à chaque modification d'une classe, de faire deux choses :

    1. ++ctrl+shift+"O"++ : Permet de faire du ménage dans les imports pour ne conserver que ceux utiles.<br>Les `import java.io.*` par exemple sont remplacés par l'import des classes utilisées uniquement.
    2. ++ctrl+shift+"F"++ : Permet de formater le code, de la même façon pour tout le monde.<br>C'est vraiment un plus si le code de tout le monde est formaté de la même façon. Cela permet de le comprendre beaucoup plus rapidement !

???+ success "Ne pas utiliser de tabulations"

    De manière générale, quel que soit le langage (Java, Python, XML, ...) il faut remplacer les ++tab++ par des espaces. Ce [site](https://readlearncode.com/code-and-stuff/how-do-i-change-eclipse-to-use-spaces-instead-of-tabs/) vous indique comment configurer eclipse pour faire cela.


### 3.3. Implémentation de la couche métier

Par rapport au paradigme MVC, on peut considérer que cette couche fait partie du **modèle**. En effet, son rôle est de le mettre à jour.

<red>Cette couche est la couche **centrale** de l'application. Lorsqu'on la développe, on se demande quels sont les services métiers qu'elle doit fournir. Ici, il y en a un seul, il s'agit de calculer les différents prix affichés sur le panier, c'est-à-dire d'appliquer les 3 règles de gestions.</red>

<red>La couche métier est toujours accessible via une interface. Cette interface indique la liste des services disponibles. Depuis l'extérieur, c'est-à-dire depuis le contrôleur, on souhaite savoir quels sont les services offerts (l'interface), mais on ne veut pas savoir comment ils sont implémentés (la classe qui implémente cette interface).</red>

<red>Nous allons donc créer une interface, proposant une méthode `computePrices`, qui va prendre le _panier_ en paramètre, appliquer les 3 règles de gestions, et renvoyer le _panier_ mis à jour. Cette interface va être implémentée par une classe.</red>


1. Dans la vue `Project Explorer`, sur le projet **_Boutique_**, faire un clic droit, puis :material-mouse:`New > Other`. Dans l'arborescence, naviguer ensuite vers :material-file-tree:`Java > Interface`.
2. Cliquer sur <bouton>Next ></bouton>.
3. Indiquer le package : `edu.boutique.business`.
4. Indiquer le nom de l'interface : `StoreBusiness`, puis cliquer sur <bouton>Finish</bouton>.
5. Elle contient une méthode `computePrices`, qui prend un objet `CartBean` (le panier) en paramètre, et qui renvoie ce même objet modifié :
    ``` java linenums="1"
    public CartBean computePrices(CartBean cart);
    ```
6. <red>En effectuant la même opération, créer une classe `StoreBusinessImpl`, dans ce même package `edu.boutique.business`, qui implémente cette interface.</red>
7. Cette classe va contenir 3 champs statiques, qui correspondent au prix de nos trois produits :
    ``` java linenums="1"
    private final static Double PEN_PRICE = Double.parseDouble("1.2");
    private final static Double FELT_PRICE = Double.parseDouble("1.8");
    private final static Double RUBBER_PRICE = Double.parseDouble("2");
    ```
8. Voici l'implémentation de `computePrices` :
    ``` java title="☕ Code Java - Méthode computePrices de StoreBusinessImpl" linenums="1"
    public CartBean computePrices(CartBean cart) {
        // Application de la RG1 :
        Double cartPrice = cart.getPenNumber() * PEN_PRICE + ...;
        // Application de la RG2 :
        Double shippingCost = 5D;
        if (cartPrice > ...) {
            shippingCost = ...;
        }
        // Application de la RG3 :
        Double totalPrice = ...;

        // Mise à jour du panier :
        cart.setCartPrice(cartPrice);
        ...
        ...

        return cart;
    }
    ```
  
    ???+ success "Remarque"
  
        Lorsqu'on fait un copier/coller du code, les imports ne sont pas toujours faits. S'il y a des erreurs (`CartBean` est par exemple inconnu), pensez à faire un ++ctrl+shift+"O"++ pour les mettre à jour !

9. Compléter les `...` dans la méthode `computePrices` pour appliquer correctement les 3 RG.
    
    ???+ note

        À chaque fois que vous verrez des `...` dans le code fourni, il faudra compléter ce code ...


C'est bon, notre modèle est en place.


### 3.4. Implémentation du contrôleur

Le contrôleur est une _**servlet**_. Une _servlet_ est une classe qui hérite de `HttpServlet` de l'API `servlet`. Il y a ensuite deux façons de déployer une _servlet_ :

??? note "Nouvelle version de JEE"

    JEE, pour **Java Entreprise Edition**, a changé de nom après la version 8. À partir de la version 9, JEE signifie **Jakarta Entreprise Edition**.<br><br>
    Il n'y a pas que le nom qui a changé, tous les packages qui commencait par `javax` commencent maintenant par `jakarta`. Par exemple, `javax.servlet.http.HttpSession` a été renommé en `jakarta.servlet.http.HttpSession`.<br><br>
    C'est pour cela que nous travaillons avec tomcat9 (qui implémente JEE 8), et pas tomcat10 (qui implémente JEE 9) ... 😫 Vous suivez toujours ?<br><br>
    Bref, utiliser JEE 9 présente quelques risques, puisque c'est récent, et il y a souvent des configurations à aller changer manuellement (aller trouver des `javax` cachés pour les modifier en `jakarta`) ...

- Via le descrtipteur de déploiement, c'est-à-dire le fichier 📄`web.xml` (c'est ce que nous allons faire dans cette première version).
- Via des **annotations** (c'est ce que nous ferons juste après).

???+ warning "Remarque importante !"

    Dans tous les cas, on choisit **une seule** de ces deux méthodes, jamais les deux.

1. On commence par créer la servlet : dans la vue `Project Explorer`, sur le projet **_Boutique_**, faire un clic droit, puis :material-mouse:`New > Other`. Dans l'arborescence, naviguer ensuite vers :material-file-tree:`Java > Class`.
2. Cliquer sur `Next`.
3. Indiquer le package : `edu.boutique.controller`.
4. Indiquer le nom de classe : `StoreServlet`.
4. Indiquer que la classe hérite de `HttpServlet` de l'API `servlet` (en cliquant sur <bouton>Brow<u>s</u>e</bouton> en face de _Superclass_), puis cliquer sur <bouton><u>F</u>inish</bouton>.
5. Depuis `Source` (qu'on peut obtenir avec ++alt+shift+"S"++), sélectionner :material-mouse:`Sources > Override/Implement Methods...`, puis cocher uniquement `doGet` (de `HttpServlet`) pour le moment.

    ???+ note

        La _servlet_ n'implémentant que cette méthode, cela signifie qu'elle ne va pouvoir intercepter que les requêtes HTTP dont la méthode est _GET_. Comme nous le verrons juste après, si nous envoyons une requête HTTP _POST_, nous obtiendrons une erreur.
    
    ???+ note "Rappel sur les méthodes HTTP"

        - La méthode _GET_ est la méthode par défaut lorsqu'on exécute une requête HTTP depuis son navigateur, c'est-à-dire lorsqu'on saisit l'URL dans la barre du navigateur et qu'on appuie sur ++"Entrée"++.<br>
            On peut envoyer des paramètres au serveur qui traite cette requete en ajoutant les couples (clef, valeur) dans l'URL, après un `?`, séparés par des `&`.<br>
            Par exemple : `http://url_de_mon_appli?param1=valeur1&param2=valeur2&...`
        - La méthode _POST_ est appelée lorsqu'on soumet un formulaire, afin d'envoyer des valeurs au serveur, c'est-à-dire lorsqu'on clique sur un bouton. Les paramètres sont alors placés dans le corps de la requête.
        
        Seule la requête pour afficher la page d'accueil (c'est-à-dire à la première connexion) sera un _GET_. Lorsque l'utilisateur cliquera sur le bouton `Valider` après avoir sélectionné ses produits, une méthode _POST_ sera exécutée. Nous verrons un peu plus tard comment faire ce choix de méthode.<br>
        C'est cela qui nous permet de différencier le premier affichage des autres, sans avoir à définir plusieurs _servlets_.

6. Renommer les deux paramètres de cette méthode `doGet` en `request` et `response`

    ???+ success "Remarque"

        Il faut toujours donner des noms **explicites** aux variables.

7. Cette méthode `doGet` ne va pour l'instant faire qu'une seule chose : rediriger vers la **vue**, `store.jsp`. Nous allons pour cela utiliser l'objet `RequestDispatcher` de l'API `servlet` :
    ``` java linenums="1"
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("store.jsp"); // (1)
        dispatcher.forward(request, response); // (2)
    }
    ```

    1. On indique vers quelle vue l'utilisateur va être redirigé.
    2. La méthode `dispatcher.forward()` permet de transférer la requête (avec toutes les informations qu'elle contient) et la réponse à la vue souhaitée (ou éventuellement à une autre _servlet_).

8. <a name="web.xml"></a>C'est bon, notre servlet est créée ! Il n'y a plus qu'à la déployer. Pour cela, on va la recenser dans le descripteur de déploiement :
    ``` xml title="Le fichier 📄 web.xml" linenums="1"
    <?xml version="1.0" encoding="UTF-8"?>
    <web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	          xmlns="http://xmlns.jcp.org/xml/ns/javaee"
	          xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
	          id="WebApp_ID" version="4.0">
        <display-name>Boutique</display-name>
        <servlet><!--(1)-->
            <servlet-name>controleur</servlet-name>
            <servlet-class>edu.boutique.controller.StoreServlet</servlet-class>
        </servlet>
        <servlet-mapping><!--(2)-->
            <servlet-name>controleur</servlet-name>
            <url-pattern>*.do</url-pattern>
        </servlet-mapping>
        <servlet-mapping><!--(3)-->
            <servlet-name>controleur</servlet-name>
            <url-pattern>/controleur.fr</url-pattern>
        </servlet-mapping>
    </web-app>
    ```

    1. Ici, on recense la _servlet_.<br><br>
            - `<servlet-name>` : on lui donne un nom.<br><br>
            - `<servlet-class>` : on indique la classe correspondante.
            
    2. La balise `<servlet-mapping>` permet d'indiquer avec quelle(s) URL(s) on peut accéder à la servlet :<br><br>
            - `<servlet-name>` indique de quelle _servlet_ on parle,<br><br>
            - `<url-pattern>` indique quelle(s) URL(s) y accède. Ici, n'importe quelle URL du type `http://localhost:8080/Boutique/*.do`

    3. Ici, on indique que cette servlet est également accessible depuis l'URL `http://localhost:8080/Boutique/controleur.fr`.


### 3.5. Implémentation de la vue

Pour l'instant, il n'y a qu'une seule vue. C'est 📄`store.jsp`. Cette _JSP_ ne contient pour l'instant que du code HTML :

??? question "Où placer les _JSP_ ?"

    Les JSP (ainsi que les pages HTML, le javascript, le CSS, ...) doivent être placées dans le dossier **contenu web**. Ce dossier est défini lorsqu'on créée le projet web dynamique. Par défaut, c'est le dossier 📂`src/main/webapp` :

    <center>![Contente directory](images/newDynamicWebProject-d.png)</center>

``` jsp linenums="1"
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Boutique en ligne</title>
</head>
<body>
	<h1>Sélection des produits</h1>
	<form action="controleur.fr" method="post"><!--(1)-->
		<fieldset>
			<legend>Liste des produits</legend>
			<table>
				<tr>
					<td>Stylo</td>
					<td><input type="number" name="penNb"/>
					<td></td>
				</tr>
				<tr>
					<td>Feutre</td>
					<td><input type="number" name="feltNb"/>
					<td></td>
				</tr>
				<tr>
					<td>Gomme</td>
					<td><input type="number" name="rubberNb"/>
					<td></td>
				</tr>
			</table>
		</fieldset>
		<input type="submit" value="Valider"/>
	</form>
	
	<fieldset>
		<legend>Récapitulatif du panier</legend>
		<table>
			<tr>
				<td>Panier :</td>
				<td></td>
			</tr>
			<tr>
				<td>Frais livraison :</td>
				<td></td>
			</tr>
			<tr>
				<td>Prix total :</td>
				<td></td>
			</tr>
		</table>
	</fieldset>
</body>
</html>
```

1. Le formulaire HTML (balise `<form>`) permet à l'utilisateur de saisir des informations, et de les envoyer au serveur en cliquant sur le bouton valider (on dit qu'on "soumet" le formulaire.)<br><br>
    Toutes les balises `<input type="*">` permettent d'envoyer une information au serveur, que nous allons pouvoir récupérer grace à la valeur de l'attribut `name`.<br><br>
    La balise `<action>` permet d'indiquer quelle _URL_ va être exécutée à la soumission du formulaire (on fait le lien avec le `<url-pattern>` dans le 📄`web.xml`)<br><br>
    Enfin, la balise `<method>` indique avec quelle méthode la requête HTTP est envoyée. Ici, c'est _POST_, ce qui nous permettra de différencier le premier affichage de la page (méthode _GET_), et la soumission du formulaire.


## 4. Déploiement de l'application et tests

### 4.1 - Création d'un serveur local

Pour tester notre application, il faut la déployer sur un serveur (tomcat9), et il faut donc au préalable créer une instance de tomcat9 sur notre poste. On utilise celle créée en [TD](/ArchitectureSI/TD1/#creation-serveur){target=_blank}.

### 4.2. Déploiement de l'application sur ce serveur

On déploie ensuite l'application **_Boutique_** sur le serveur, comme en [TD](/ArchitectureSI/TD1/#deploiement-application){target=_blank}.

Notre application est disponible à l'adresse [http://localhost:8080/Boutique/controleur.fr](http://localhost:8080/Boutique/controleur.fr) (par exemple, on pourrait aussi changer la fin en *.do).


### 4.3. Test de l'application

Lorsqu'on va ici : [http://localhost:8080/Boutique/controleur.fr](http://localhost:8080/Boutique/controleur.fr) (méthode _GET_), la page s'affiche correctement. Mais lorsqu'on clique sur valider (méthode _POST_), une erreur apparaît :
<center>
  ![Erreur méthode POST](images/boutique_v1.0_erreur.png)
</center>

C'est normal, dans notre _servlet_, nous n'avons implémenté que la méthode `doGet`, donc les requêtes _GET_ (premier affichage de la page) sont gérées. Mais nous n'avons pas implémenté la méthode `doPost`, ce que nous allons faire maintenant ... pour gérer la soumission du formulaire par l'utlisateur

## 5. Amélioration de l'application

### 5.1. Développement de la V1.1

Nous allons donc maintenant rajouter l'implémentation de la méthode `doPost` dans notre _servlet_ pour gérer cette requête :

1. Dans la classe `StoreServlet`, ouvrir le menu :material-mouse:`Sources > Override/Implement Methods...`, et surcharger la méthode `doPost` de `HttpServlet`.

    ??? success "Astuce : surcharge rapide"

        Pour surcharger une méthode, on peut également écrire le début de son nom, ici le début de `doPost`, et appuyer sur ++ctrl+"Entrée"++ pour faire de l'autocomplétion. On obtient le même résultat, mais plus rapidement.

2. Comme pour la méthode `doGet`, renommer les paramètres en `request` et `response` respectivement.
3. Dans cette méthode, nous allons récupérer les données saisies par l'utilisateur depuis le formulaire HTML. Ces données sont en paramètre de la requête HTTP, que la méthode utilisée soit _GET_ ou _POST_. Nous allons donc faire `request.getParameter(<attribut_name>)`.

    ``` java title="☕ Code Java - Méthode doPost - 1/4" linenums="1"
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String penNumber = request.getParameter("penNb"); // (1)
        String feltNumber = ...;
        String rubberNumber = ...;


    }
    ```

    1. Le nom du paramètre, ici `penNb`, doit correspondre à l'attribut `name` de la balise `<input type="*">` du formulaire HTML.

4. Ces données saisies par l'utilisateur doivent être "envoyées" au modèle. Dans cette méthode, on rajoute donc :

    ``` java title="☕ Code Java - Méthode doPost - 2/4" linenums="1"
    CartBean cart = new CartBean();

    cart.setPenNumber(Integer.parseInt(penNumber)); // (1)
    ...
    ...
    ```

    1. Les objets récupérés en paramètre de la requête sont des `String`. Ici, il faut donc le transformer en `Integer`.

5. Ensuite, on appelle la couche métier, pour mettre à jour les données du panier (les 3 RG) en fonction de ce que l'utilisateur a sélectionné. Dans cette méthode, on rajoute donc :

    ``` java title="☕ Code Java - Méthode doPost - 3/4" linenums="1"
    StoreBusiness business = new StoreBusinessImpl();

    cart = business.computePrices(cart);
    ```

    ??? success "Bonne pratique"

        Cela permet d'avoir, depuis le contrôleur (la _Servlet_), une connexion à la couche métier fonctionnelle. Mais cela signifie qu'à chaque exécution du contrôleur, une nouvelle instance de `StoreBusinessImpl` est créée. Il serait préférable, comme nous l'avons vu en cours, de rajouter un champ `private` à la _Servlet_, de type `StoreBusiness`, instancié une seule fois, dans la méthode `init` de la _Servlet_.

    ??? note

        Pour être plus respectueux des _design pattern_ usuels, il faudrait créer une instance implémentant `StoreBusiness` avec une `Factory` ...

6. Enfin, on redirige l'utilisateur vers la bonne vue :

    ``` java title="☕ Code Java - Méthode doPost - 4/4" linenums="1"
    RequestDispatcher dispatcher = request.getRequestDispatcher("store.jsp");
    dispatcher.forward(request, response);
    ```

### 5.2. Test de la V1.1

En testant à nouveau (pour cela, il suffit de redémarrer le serveur), on constate maintenant :


1. Qu'au premier affichage, le nombre de chaque article est vide, et qu'il y a une erreur si on ne met pas des nombres partout ...
2. Qu'il ne se passe rien si on saisit des nombres partout et qu'on cliquer sur <bouton>Valider</bouton> ...

### 5.3. Développement de la V1.2

Nous allons commencer par traiter le deuxième problème.

Le comportement observé est en fait normal. Le modèle (ici, le _bean_ `CartBean`) n'est pas "envoyé" à la vue par le contrôleur.<br>
➡️ Pour cela, nous allons l'ajouter dans la requête. À la fin de la méthode `doPost` de la servlet, juste avant l'appel du `RequestDispatcher`, il faut ajouter :

``` java title="☕ Code Java - StoreServlet.doPost" linenums="1"
request.setAttribute("USER_CART", cart); // (1)
```

1. `setAttribute` permet d'ajouter un **attribut** à la requête (à ne pas confondre avec un **paramètre**).<br><br>
    Le premier paramètre, `"USER_CART"` est la clef : c'est ce qui va nous permettre de récupérer le _bean_ dans la vue.<br><br>
    Le deuxième paramètre, `cart`, est le _bean_.

Le _bean_ est maintenant envoyé à la vue, il faut le récupérer avec le méthode `HttpRequest.getAttribute()`. On rajoute le code correspondant juste avant la balise `<!DOCTYPE html>` :

``` jsp linenums="1"
<% // (1)
CartBean cart = (CartBean) request.getAttribute("USER_CART");
%>
```

1. Les balises `<% %>` permettent d'ajouter des **_scriptlet_**, c'est-à-dire du code Java à l'intérieur du code HTML.

La classe Java `CartBean` doit être importée dans la JSP. Il suffit d'utiliser l'auto-complétion (++ctrl+space++), lorsqu'on écrit _CartBean_, et l'import va être ajouté automatiquement tout en haut du fichier :

``` jsp linenums="1"
<%@ page import="edu.boutique.model.CartBean"%> <!--(1)-->
```

1. Ceci s'appelle une directive.

On peut maintenant afficher les informations présentes dans le _bean_ et calculées par la couche métier. Nous allons modifier la partie "Récapitulatif du panier" en ajoutant les appels au _bean_ :

``` jsp linenums="1"
<fieldset>
    <legend>Récapitulatif du panier</legend>
    <table>
        <tr>
            <td>Panier :</td>
            <td><%=cart.getCartPrice()%></td><!--(1)-->
        </tr>
        <tr>
            <td>Frais livraison :</td>
            <td><%=cart.getShippingCost()%></td>
        </tr>
        <tr>
            <td>Prix total :</td>
            <td><%=cart.getTotalPrice()%></td>
        </tr>
    </table>
</fieldset>
```

1. Le code Java entre `<%= %>` s'appelle une _expression_. Cela permet d'afficher la valeur de l'expression.<br><br>
    Le code `<%=variable %>` est équivalent à `<% out.println(variable) %>`, il affiche le contenu de `variable`.

### 5.4. Test de la V1.2

Si on teste à nouveau notre application, on a maintenant une erreur au premier affichage 😫 :
<center>
  ![Erreur de la V1.1](images/boutique_v1.1_erreur.png)
</center>

Cela vient du fait qu'au premier affichage, dans le _scriptlet_ dans la JSP, on récupère dans la requête un objet que nous n'avons pas encore placé ... On obtient donc un `null`, d'où l'erreur :<br>
`java.lang.NullPointerException: Cannot invoke "edu.boutique.model.CartBean.getCartPrice()" because "cart" is null`


### 5.5. Développement de la V1.3

Il y a plusieurs façons de gérer cette erreur. Ce que nous allons faire, c'est de placer un objet `CartBean` vide (mais pas `null`) dans la requête au premier affichage, c'est-à-dire dans la méthode `doGet`. On a donc maintenant :

``` java linenums="1"
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

    CartBean cart = new CartBean(); // (1)
    request.setAttribute("USER_CART", cart); // (2)

    RequestDispatcher dispatcher = request.getRequestDispatcher("store.jsp");
    dispatcher.forward(request, response);
}
```

1. On créée un objet vide, mais pas `null`.
2. Il y a maintenant un objet dans la requête au premier affichage.

### 5.6. Test de la V1.3

Le premier affichage fonctionne désormais. Il nous reste à mieux initialiser notre formulaire, notamment pour ne plus avoir d'erreur lorsqu'on clique sur <bouton>Valider</bouton> sans avoir changé les valeurs par défaut :
<center>
  ![Affichage de la V1.2](images/boutique_v1.2.png)
</center>


### 5.7. Développement de la V1.4

Le problème vient du fait qu'on "_caste_" une chaîne de caractère vide en `Integer` ... Une façon de ne plus avoir ce problème est d'initialiser tous les champs du _bean_ `CartBean` à 0 (on supprime ainsi également l'affichage des _null_ dans la partie panier) :

``` java title="☕ Code Java - classe CartBean" linenums="1"
private Integer penNumber = 0;
private Integer feltNumber = 0;
private Integer rubberNumber = 0;
private Double cartPrice = 0D;
private Double shippingCost = 0D;
private Double totalPrice = 0D;
```

Enfin, dans la vue, il faut récupérer les nombres d'articles sélectionnés. Pour cela, il faut ajouter l'attribut `value` aux balises HTML `<input type="number">`. Cela permet d'avoir une valeur par défaut, qui va être celle stockée dans le _bean_ présent dans la requête. Dans 📄`store.jsp`, la partie _Liste des produits_ devient donc :

``` jsp title="📄 store.jsp" linenums="1"
<form action="controleur.fr" method="post">
    <fieldset>
        <legend>Liste des produits</legend>
        <table>
            <tr>
                <td>Stylo</td>
                <td><input type="number" name="penNb" value="<%=cart.getPenNumber()%>"/>
                <td></td>
            </tr>
            <tr>
                <td>Feutre</td>
                <td><input type="number" name="feltNb" value="<%=cart.getFeltNumber()%>"/>
                <td></td>
            </tr>
            <tr>
                <td>Gomme</td>
                <td><input type="number" name="rubberNb" value="<%=cart.getRubberNumber()%>"/>
                <td></td>
            </tr>
        </table>
    </fieldset>
    <input type="submit" value="Valider"/>
</form>
```

## 6. Version 2

### 6.1. Suppression du code Java dans la vue

Notre application est fonctionnelle, mais elle ne respecte pas les bonnes pratiques. En effet, il ne faut pas mettre de code Java dans la vue. Pour le supprimer, nous allon sutiliser la [**_JSTL_**](https://www.jmdoudoux.fr/java/dej/chap-jstl.htm) : la **_JavaServer pages Standard Tag Library_**. Plus précisément, nous allons utiliser le langage [_EL_](https://www.jmdoudoux.fr/java/dej/chap-jstl.htm#jstl-2) (pour _Expression Language_) offert par la _JSTL_, qui permet d'accéder aux objets Java présents dans la requête (ainsi que dans la session, entre autre). Les parties suivantes peuvent donc être supprimées :

``` jsp linenums="1"
<%
CartBean cart = (CartBean) request.getAttribute("USER_CART");
%>
```

``` jsp linenums="1"
<%@ page import="edu.boutique.model.CartBean"%>
```

Le code suivant, qui nous permettait d'accéder aux propriétés du _bean_ :

``` jsp linenums="1"
<%=cart.getCartPrice()%>
```

va être remplacé par

``` jsp linenums="1"
${requestScope.USER_CART.cartPrice} <!--(1)-->
```

1. Précisions :<br><br>
    `requestScope` indique que l'objet en question est présent dans la requête (on utilise `sessionScope` si l'objet est présent dans la session).<br><br>
    `USER_CART` est la clef avec laquelle l'objet a été placé dans la requête (ou dans la session).<br><br>
    `cartPrice` est le champ du bean à afficher.

??? note "Explication sur le champ du bean"

    Concrétement, la méthode dont le nom est obtenu en mettant le premier caractère en majuscule (ce qui donne `CartPrice`) et en préfixant par `get`. Ici la méthode `getCartPrice()` (sans paramètre) est appelée.

La vue 📄`store.jsp` devient donc :

``` jsp title="📄 store.jsp" linenums="1"
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Liste des produits</title>
</head>
<body>
	<h1>Sélection des produits</h1>
	
	<form action="controleur.fr" method="post">
		<fieldset>
			<legend>Liste des produits</legend>
			<table>
				<tr>
					<td>Stylo</td>
					<td><input type="number" name="penNb" value="${requestScope.USER_CART.penNumber}"/>
					<td></td>
				</tr>
				<tr>
					<td>Feutre</td>
					<td><input type="number" name="feltNb" value="${requestScope.USER_CART.feltNumber}"/>
					<td></td>
				</tr>
				<tr>
					<td>Gomme</td>
					<td><input type="number" name="rubberNb" value="${requestScope.USER_CART.rubberNumber}"/>
					<td></td>
				</tr>
			</table>
		</fieldset>
		<input type="submit" value="Valider"/>
	</form>
	
	<fieldset>
		<legend>Récapitulatif du panier</legend>
		<table>
			<tr>
				<td>Panier :</td>
				<td>${requestScope.USER_CART.cartPrice}</td>
			</tr>
			<tr>
				<td>Frais livraison :</td>
				<td>${requestScope.USER_CART.shippingCost}</td>
			</tr>
			<tr>
				<td>Prix total :</td>
				<td>${requestScope.USER_CART.totalPrice}</td>
			</tr>
		</table>
	</fieldset>
</body>
</html>
```

### 6.2. Utilisation des annotations pour déployer une _servlet_

Nous allons maintenant voir une deuxième façon de déployer une _servlet_, beaucoup plus simple car il n'y a plus besoin de renseigner le descripteur de déploiement (c'est-à-dire le fichier 📄`web.xml`).

Nous allons donc supprimer dans ce fichier tout ce que nous avions fait [plus tôt](#web.xml), et le fichier 📄`web.xml` doit maintenant ressembler à ça :

``` xml linenums="1"
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns="http://xmlns.jcp.org/xml/ns/javaee"
          xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
          id="WebApp_ID" version="4.0">
    <display-name>Boutique</display-name>
</web-app>
```

Pour déployer la _servlet_, il suffit de rajouter l'annotation suivante juste au dessus de la déclaration de la classe `StoreServlet` :

``` java linenums="1"
...
@WebServlet(name = "controleur", urlPatterns = { "*.do", "/controleur.fr" })
public class StoreServlet extends HttpServlet {
...
```

Il suffit de redémarrer le serveur, et l'application est disponible depuis la même URL.

➡️ Ça y est, nous avons terminé le développement de notre première application Web respectant le paradigme MVC.
