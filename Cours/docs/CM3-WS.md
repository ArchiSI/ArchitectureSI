# CM3 - Web Services

## Pré-requis

1. Java 1.8.
2. Eclipse avec JBoss Tools.
3. SaopUI open source : [https://www.soapui.org/downloads/soapui/](https://www.soapui.org/downloads/soapui/)
4. Postman (il faut créer un compte) : [https://www.postman.com/downloads/](https://www.postman.com/downloads/)
5. Eclipse avec "Spring tools 4 (aka Spring tool suite 4")


## Web Services SOAP

1. Rappels sur l'appli existante.
2. Création d'une classe côté Web qui contient :
    ```java
    @WebService(serviceName = "WsLocationSoap")
    public class LocationSoap {
    ```
3. Cette classe contient les méthodes :
    ```java
    @WebMethod
    public List<LocationBean> locations() {
        return businessLocal.getLocations();
    }

    @WebMethod
    public LocationBean getLocation(@WebParam Integer id) {
        return this.businessLocal.getLocation(id);
    }

    @WebMethod(operationName = "supprime")
    public void deleteLocation(@WebParam(name = "idLoc") Integer id) {
        this.businessLocal.deleteLocation(id);
    }

    @WebMethod
    public void addLocation(@WebParam String adresse, @WebParam String city, @WebParam Double price,
            @WebParam String zipCode) {
        LocationBean bean = new LocationBean();
        bean.setAddress(adresse);
        bean.setCity(city);
        bean.setNightPrice(price);
        bean.setZipCode(zipCode);
        this.businessLocal.addLocation(bean);
    }
    ```
    :warning: Si pas de `name` dans le `@WebParam`, ils s'appellent `arg0`, `arg1`, ...
4. On peut ne pas sérializer certaines propriétés :
    ```java
    @XmlTransient
    private byte[] picture;
    ```
    mais il faut indiquer d'abord qu'on met les annotations sur les champs et pas sur les mutateurs :
    ```java
    @Entity
    @XmlAccessorType(XmlAccessType.FIELD)
    public class LocationBean implements Serializable {
    ```

## Client WS Soap

1. Création d'un projet Java (classique) : `LocationWsClient`
2. On se place dans le dossier src, puis
    ```bat
    wsimport.exe -s . http://localhost:8080/LocationCorrWeb/WsLocationSoap?wsdl
    ```
    on peut modifier le nom du package où sont générées les classes :
    ```bat
    wsimport.exe -s . http://localhost:8080/LocationCorrWeb/WsLocationSoap?wsdl -p edu.polytech.location.dao.soapclient
    ```
3. Création d'une classe `edu.polytech.location.ws.soap.client.ClientSoap`
4. Récupération du proxy :
    ```java
    LocationSoap proxy = new WsLocationSoap().getLocationSoapPort();
    ```


## Client WS Soap .net

1. Création projet "Application console (.Net framework)"
2. Dans "Références", clic droit "Ajouter une référence de service"
3. Code
    ```cs
    ServiceReference1.LocationSoapClient proxy = new ServiceReference1.LocationSoapClient();
    Console.WriteLine(proxy.getLocation(1).adress);
    Console.read();
    ```


## WS REST

JaxRS est fourni avec WildFly

1. Création d'une classe `edu.polytech.location.ws.rest.WebApp` :
    ```java
    @ApplicationPath("/context")
    public class WebApp extends Application {

    }
    ```
2. Création de la classe `edu.polytech.location.ws.rest.LocationRest` avec le path du WS :
    ```java
    @Path("v1")
    public class LocationRest {
    ```
3. Utilisation de `@Path`, `@GET`, `@Produces`
4. Pour que l'EJB puisse être injecté, il faut rajouter `@Stateless`
5. Json fonctionne bien, mais pas XML ... Il faut ajouter `@XmlRootElement` pour indiquer qu'on utilise JaxB pour l'OXM.
6. On peut rajouter les méthodes de suppressions sur le verbe `@DELETE`.
7. On peut rajouter les méthodes de création sur le verbe `@POST`.
8. On peut rajouter les méthodes de maj sur le verbe `@PUT`.

```java
@Stateless
@Path("v1")
public class LocationRest {

    @EJB
    private BusinessLocal businessLocal;

    @GET
    @Path("/locations")
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public List<LocationBean> getLocations() {
        return this.businessLocal.getLocations();
    }

    @GET
    @Path("/locations/{id}")
    public LocationBean getLocation(@PathParam("id") Integer idLoc,
            @HeaderParam("Authorization") String authorization) {
        return this.businessLocal.getLocation(idLoc);
    }

    @DELETE
    @Path("/locations")
    public void deleteLocations() {
        this.businessLocal.deleteLocations();
    }

    @DELETE
    @Path("/locations/{id}")
    public void deleteLocation(@PathParam("id") Integer idLoc) {
        this.businessLocal.deleteLocation(idLoc);
    }

    @POST
    @Path("/locations")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void createLocations(@FormParam("price") Double nightPrice, @FormParam("adress") String address,
            @FormParam(value = "city") String city, @FormParam("zipCode") String zipCode) {
        System.out.println("city : " + city);
        this.businessLocal.addLocation(new LocationBean(nightPrice, address, city, zipCode));
    }

    @POST
    @Path("/locations")
    @Consumes(MediaType.APPLICATION_JSON)
    public void createLocations(LocationBean bean) {
        this.businessLocal.addLocation(bean);
    }
}
```

Remarque : Il faut `jackson-annotations-2.13.2.jar` dans le `WEB-INF/lib` pour que `@XmlTransient` fonctionne en JSON ...


## WS REST Client

```xml
<properties>
    <maven.compiler.target>1.8</maven.compiler.target>
    <maven.compiler.source>1.8</maven.compiler.source>
</properties>

<dependencies>
    <dependency>
        <groupId>org.glassfish.jersey.core</groupId>
        <artifactId>jersey-client</artifactId>
        <version>2.25.1</version>
    </dependency>
    <dependency>
        <groupId>org.glassfish.jersey.media</groupId>
        <artifactId>jersey-media-json-jackson</artifactId>
        <version>2.25.1</version>
    </dependency>
</dependencies>
```

```java
public class RestClient {

    private static final String REST_URI = "http://localhost:8100/LocationCorrWeb/api/v1/locations";

    public static LocationBean getLocation(Integer id) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(REST_URI);
        target = target.path(String.valueOf(id));
        Builder request = target.request(MediaType.APPLICATION_JSON);
        return request.get(LocationBean.class);
    }

    public static void getLocations() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(REST_URI);
        Builder request = target.request(MediaType.APPLICATION_JSON);
        List<LocationBean> locations = request.get(new GenericType<List<LocationBean>>() {});
        for (LocationBean bean : locations) {
            System.out.println(bean.getId() + " - " + bean.getAddress());
        }
    }

    public static void createLocation(LocationBean bean) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(REST_URI);
        Builder request = target.request();
        request.post(Entity.json(bean));
    }

    public static void main(String[] args) {
        LocationBean bean = getLocation(29);
        System.out.println(bean.getAddress());
        System.out.println("----");
        getLocations();
        System.out.println("----");
        createLocation(new LocationBean(123D, "ad", "Tours", "37200"));
        System.out.println("--Fin--");
    }
}
```


## RandomUser




## Java 1.8
1. Télécharger la jdk1.8.0_202.
2. Variables d'environnements `JAVA_HOME` et `JRE_HOME`.
3. Variable `PATH`, rajouter `%JAVA_HOME%/bin` au tout début.
4. Pour eclipse, il faut java >=11. Il suffit d'aller là où eclipse est installé, ouvrir le fichier `eclipse.ini`, et rajouter au tout début
    ```ini
    -vm
    D:/Program Files/Java/jdk-17.0.1/bin
    ```
    