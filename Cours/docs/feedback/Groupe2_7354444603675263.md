# Retours pour le groupe 2

C'est un projet décevant.

Vous pouvez avoir de bonnes raisons de rendre un projet en retard. Il faut cependant prévenir, et demander un délai.

Voici mes remarques :

- Le code fourni n'est pas fonctionnel. J'ai du effectuer des modifications dans votre code pour pouvoir faire des tests.
- Malgré ces modifications, il est impossible de valider une réservation. L'erreur "Méthode POST non supportée" est levée.
- Tout doit être dans des packages `edu.polytech.location.*`. Il n'y a pas besoin de séparer location et reservation.
- Les noms des classes doivent commencer par des majuscules (Cf. les _Servlets_).
- Il est préférable de faire un seul service métier qui calcule tous les prix, cela évite de multiplier les communications entre les couches.
- Trop d'infos sont stockées en BDD pour l'historique des réservations.
