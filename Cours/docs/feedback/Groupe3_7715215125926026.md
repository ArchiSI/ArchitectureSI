# Retours pour le groupe 3

C'est un bon projet.

Voici mes remarques :

- Dans les servlet, il faut faire **très attention** avec les attributs de classe, par exemple avec `idLoc` dans `LocationsReservationServlet` et le _bean_ dans `ReservationsReceiptServlet`. Il faut bien comprendre qu'il n'y a qu'une seule instance de chaque _Servlet_, partagée par tous les utilisateurs effectuant une requête HTTP. Ainsi, si deux utilisateurs effectuent des requêtes en même temps, les deux pourraient se retrouver sur l'appartement du 2ème (le `idLoc` du premier pourrait être écrasé par celui du 2ème), ce qui  peut être embétant !
- Tout doit être dans des packages `edu.polytech.location.*`. Il n'y a pas besoin de séparer location et reservation.
- Les noms de méthodes doivent commencer par une minuscule (ce n'est pas le cas pour les RG*).
- Dans `LocationReservationServlet`, il est préférable d'avoir un seul service métier qui calcule toutes les RGs. Cela permet de ne pas multiplier les communications entre les couches.
- Dans cette même servlet, il faut éviter de placer plein d'informations dans la requête. Il est préférable de tout mettre dans un _bean_, et de placer celui-ci en requête.
- C'est mieux d'avoir un seul _EJB_, qui appelle les deux _DAOs_.
- Trop d'infos sont stockées en BDD pour l'historique des réservations.
- La RG5 n'est pas fonctionnelle (le coût total de la réservation est enlevé lorsqu'elle ne doit pas être appliquée).
- La RG7 n'est pas entièrement implémentée.
