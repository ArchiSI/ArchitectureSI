# Retours pour le groupe 8

C'est un bon projet.

Voici mes remarques :

- Les noms de méthodes doivent commencer par des minuscules (par exemple `Business.Tarification()`).
- La prise en compte du "Ménage" est inversé par rapport au choix de l'utilisateur.
- L'assurance annulation est toujours facturée que la case soit cochée ou non.
- La RG7 n'est pas entièrement implémentée.
