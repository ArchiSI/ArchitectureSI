# Exercice 2 - Implémentation d'un client (lourd) Java

Nous allons maintenant implémenter la brique "Appli Java" du schéma suivant :

<figure markdown>
  ![archi appli](../images/schema_archi_V3.png)
  <figcaption>Architecture de notre application</figcaption>
</figure>

## 1. Création du projet Java

Nous allons pour cela créer un projet Java indépendant, qui va appeler cet _EJB_, et plus précisément l'interface _remote_.

Depuis la vue _Enterprise Explorer_, créer un nouveau Projet Java (sélectionner _Java Project_) nommé **_GestionNotesClient_**, sélectionner la bonne _JDK_, décocher la case "_Create module-info.java file_", et enfin cliquer sur <bouton>Finish</bouton>.

Nous allons avoir besoin du modèle (c'est-à-dire des _beans_), ainsi que de l'interface _remote_. Pour cela, nous allons ajouter le projet **_GestionNotesEJB_** dans le _classpath_ du projet **_GestionNotesClient_**. Nous verrons dans une prochaine version comment faire pour ne pas avoir à importer **tout** le projet _EJB_ :

<figure markdown>
  ![Classpath projet GestionNotesClient](../images/GestionNotesClient.png)
  <figcaption>Gesiton du classpath du client Java</figcaption>
</figure>

Dans ce projet, créer la classe `edu.polytech.note.client.NoteClient`, **en cochant la case pour avoir la méthode** `main`. Cette classe va contenir une méthode `getBusiness` permettant de manipuler l'_EJB_ session, **via son nom JNDI**. La méthode `main` va appeler cette méthode, et effectuer quelques opérations pour tester le bon fonctionnement de l'_EJB_.

Pour accéder à l'annuaire JNDI à distance, il faut créer un object `Context` paramétré comme indiqué ci-dessous. On utilise ensuite sur cet objet la méthode `lookup(url)` qui permet d'accéder à l'objet à distance (depuis une autre JVM, éventuellement sur un autre serveur) uniquement en connaissant son nom JNDI (c'est ce qui est contenu dans la variable `url`).

``` java title="☕ Code Java - NoteClient - La méthode getBusiness()" linenums="1"
/**
	* 
	* @return L'EJB session pour manipulation.
	* @throws NamingException Si l'EJB n'est pas trouvé dans l'annuaire JNDI.
	*/
private static NotesBusinessRemote getBusiness() throws NamingException {
	Properties jndiProperties = new Properties();
	jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
	jndiProperties.put(Context.PROVIDER_URL, "remote+http://localhost:8080");//(1)
	Context ctx = new InitialContext(jndiProperties);

	String appName = "";
	String projectName = "GestionNotesEJB";
	String className = "NotesBusinessImpl";
	String remote = NotesBusinessRemote.class.getName();

	String url = "ejb:" + appName + "/" + projectName + "/" + className + "!" + remote;//(2)

	NotesBusinessRemote business = (NotesBusinessRemote) ctx.lookup(url);

	return business;
}
```

1. Attention à adapter le port ici si vous l'avez modifié.
2. Cette URL doit **exactement** correspondre à celle apparaissant dans les logs du Serveur WildFly.

``` java title="☕ Code Java - NoteClient - La méthode main()" linenums="1"
public static void main(String[] args) throws NamingException {
	// Cette objet business va nous permettre d'appeler les services métiers disponibles,
	// c'est-à-dire ceux définis dans l'interface distante : NotesBusinessRemote.
	NotesBusinessRemote business = getBusiness();

	// 1er étudiant :
	NoteBean bean1 = new NoteBean();
	bean1.setName("Haddock");
	bean1.setFirstName("Capitaine");
	bean1.setNote(10.5D);
	// Appel du service métier d'ajout d'une note :
	business.insertNote(bean1);

	// 2ème étudiant :
	NoteBean bean2 = new NoteBean();
	bean2.setName("Tournesol");
	bean2.setFirstName("Professeur");
	bean2.setNote(20D);
	// Appel du service métier d'ajout d'une note :
	business.insertNote(bean2);

	// On affiche la liste des notes présentes en BDD.
	// Appel du service métier de récupération de toutes les notes :
	List<NoteBean> notesList = business.getNotesList();
	System.out.println("--- Notes ---");
	for (NoteBean bean : notesList) {
		System.out.println(bean.getFirstName() + " " + bean.getName() + " : " + bean.getNote());
	}
	System.out.println("La moyenne est de " + business.computeMean(notesList));
}
```


## 2. Test de l'_EJB_ session avec le projet client

Pour tester ce client, il suffit de faire un clic droit sur la classe `NoteClient`, et de :material-mouse:`Run As > Java Application`. En faisant cela, on obtient l'erreur suivante :

``` title="📋 Logs du projet ClientJava (main de la classe Client)"
java.lang.ClassNotFoundException: org.wildfly.naming.client.WildFlyInitialContextFactory
```

En effet, si on utilise comme client de l'_EJB_ une projet Java classique, il faut importer un JAR fourni par WildFly. Dans le _classpath_ du projet **_ClientJava_**, il faut ajouter le JAR présent ici : 📄`<WILDFLY_HOME>/bin/client/jboss-client.jar` (il faut cliquer sur <bouton>Add E<u>x</u>ternal JARs...</bouton>).

Lorsqu'on relance le test, on obtient le résultat suivant :

``` title="📋 Logs du projet ClientJava (main de la classe Client)"
--- Notes ---
Capitaine Haddock : 10.5
Professeur Tournesol : 20.0
La moyenne est de 15.25
```

On peut également vérifier que les deux notes ont bien été ajoutées dans la table en BDD.

???+ warning

	Attention, à chaque fois qu'on relance le client Java, les notes du Capitaine Haddock et du Professeur Tournesol sont ajoutées dans la liste des notes.

	En revanche, si vous avez laissé la configuration "drop and create" dans le 📄`persistence.xml`, à chaque redémarrage du serveur, ou à chaque fois que l'application est redéployée, la table est vidée.
