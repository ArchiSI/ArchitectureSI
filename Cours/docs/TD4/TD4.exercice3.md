# Exercice 3 - Implémentation de la partie présentation

Nous allons maintenant implémenter la partie "Couche Présentation" du schéma suivant :

<figure markdown>
  ![archi appli](../images/schema_archi_V3.png)
  <figcaption>Architecture de notre application</figcaption>
</figure>

## 1. Création du projet Web

Nous allons maintenant créer le projet Web, qui permet de gérer le site web.

1. Dans la vue _Entreprise Explorer_, créer un nouveau _Dynamic Web Project_, nommé **_GestionNotesWeb_**.
2. Vérifier la _runtime_ utilisée, décocher "_Add project to an EAR_", cliquer sur <bouton><u>N</u>ext</bouton> deux fois.
3. Cocher "_Generate web.xml deployment descriptor_" puis cliquer sur <bouton><u>F</u>inish</bouton>.

???+ warning "Ajout du lien vers le projet _EJB_"

    Tout comme le client Java créé précédemment, nous allons avoir besoin d'accéder aux _beans_ du modèle ainsi qu'à l'interface de l'_EJB_ (la _local_ cette fois-ci). Il faut donc ajouter une dépendance vers **_GestionNotesEJB_** dans le _classpath_ du projet.

## 2. Implémentation de la couche présentation

Le projet contient deux sous-couches :

1. La couche "contrôleur" (les _Servlets_).
2. La couche "vue" (les JSPs).

Les modifications décrites ci-dessous le sont à nouveau par rapport au projet précédent (**_GestionNotesJPA_** du TD3).

### 2.1. Implémentation de la couche "contrôleur"

Il y a deux modifications à effectuer **dans chaque _Servlet_** :

1. La méthode `init` doit être supprimée.
2. En effet, l'instantiation de l'EJB (`NoteBusinessLocal`) ne se fait plus manuellement, mais via injection. De plus, l'interface utilisée ne s'appelle plus `NotesBusiness` mais `NotesBusinessLocal`. Le champ dans chaque _Servlet_ devient donc :

    ``` java
    @EJB
    private NotesBusinessLocal business;
    ```

??? note "Interface _local_ vs _remote_"

    Pour l'instant, les deux projets, **_GestionNotesEJB_** et **_GestionNotesWeb_** sont déployés sur le même serveur, donc sur la même JVM. On peut donc utiliser l'interface _local_.

    Lorsque c'est possible, on utilise toujours l'interface _local_ :

    1. Elle est plus facile à appeller (injection, plutôt que recherche dans l'annuaire JNDI comme dans le client lourd).
    2. Elle est beaucoup moins gourmande en ressources. Avec l'interface _remote_, le protocole RMI est utilisé, qui nécessite la création de _Socket_ et l'utilisation du réseau ...


### 2.2. Implémentation de la couche "vue"

Il n'y a aucune modification à effectuer dans cette couche, qui peut être recopiée sans modification.


Ça y est, notre projet Web est disponible. On ne peut pas le déployer directement, car il est lié au projet _EJB_. Il va donc falloir créer un projet contenant ces deux modules. C'est le rôle du projet d'application d'entreprise (le premier "E" de JEE).
