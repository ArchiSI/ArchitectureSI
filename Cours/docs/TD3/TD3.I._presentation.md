# TD 3 - Partie 1 - Utilisation de JDBC

JDBC (pour _Java DataBase Connectivity_) est une API (interface de programmation) qui permet d'effectuer une connexion à une base de données depuis une application Java.

Dans cette première partie, nous allons implémenter nous même la couche d'accès aux données (couche _DAO_). Dans la suite de ce TD, nous verrons comment utiliser une API pour faire cela (_JPA_). Nous allons donc pour l'instant mettre en place l'architecture suivante :

<figure markdown>
  ![Architecture TD3 - Exercice 1](../images/schema_archi_V1.png)
  <figcaption>Schéma de l'architechture de la partie 1 (JDBC)</figcaption>
</figure>
