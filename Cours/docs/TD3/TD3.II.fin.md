# TD 3 - Partie 2 - Conclusion - Injection des dépendances

Nous avons une application fonctionnelle, mais nous avons des dépendances fortes entre les couches :

1. Dans les _Servlets_, nous avons, dans les méthodes `init`, des `this.business = new NotesBusinessImpl();`.<br>
    Nous indiquons donc explicitement quelle classe (`NotesBusinessImpl`) doit implementer l'interface `NotesBusiness`.
2. Dans la couche "métier" (classe `NotesBusinessImpl`), dans le constructeur, nous avons `this.dao = new NotesDAOImpl();`.<br>
    Nous indiquons donc ici aussi explicitement quelle classe (`NotesDAOImpl`) doit implémenter l'interface `NotesDAO`.
3. Le lien avec les points précédent ne saute pas aux yeux tout de suite, mais la façon dont nous instancions **manuellement** l'objet `EntityManager` dans le _DAO_ (c'est lui qui nous permet de faire le lien avec le fichier 📄`persistence.xml`, et donc avec la base de données) nous oblige à gérer **manuellement** le cycle de vie des entités _JPA_ (les _beans_), c'est-à-dire les transactions.

Pour ne plus avoir ces dépendances fortes (pour les deux premiers points), et pour ne plus avoir à gérer nous-même le cycle de vie des entités, nous allons utiliser de l'inversion de contrôle.

???+ info "L'inversion de contrôle"

    L'inversion de contrôle (_inversion of control_, _IoC_) est un patron d'architecture commun à tous les frameworks (ou cadre de développement et d'exécution). Il fonctionne selon le principe que le flot d'exécution d'un logiciel n'est plus sous le contrôle direct de l'application elle-même mais du framework ou de la couche logicielle sous-jacente.

    L'inversion de contrôle est un terme générique. Selon le problème, il existe différentes formes, ou représentation d'_IoC_, le plus connu étant l'injection de dépendances (_dependency injection_) qui est un patron de conception permettant, en programmation orientée objet, de découpler les dépendances entre objets.

    _Source : [Wikipedia](https://fr.wikipedia.org/wiki/Inversion_de_contr%C3%B4le)_

???+ info "L'injection de dépendances"

    L'injection de dépendances (_dependency injection_ en anglais) est un mécanisme qui permet d'implémenter le principe de l'inversion de contrôle.

    Il consiste à créer dynamiquement (injecter) les dépendances entre les différents objets en s'appuyant sur une description (fichier de configuration ou métadonnées) ou de manière programmatique. Ainsi les dépendances entre composants logiciels ne sont plus exprimées dans le code de manière statique mais déterminées dynamiquement à l'exécution.

    _Source : [Wikipedia](https://fr.wikipedia.org/wiki/Injection_de_d%C3%A9pendances)_

Concrètement, nous allons supprimer le code actuellement dans les constructeurs (`NotesDAOImpl` et `NotesBusinessImpl`) ou dans les `init` des _Servlets_, et les remplacer par des annotations.

Nous n'allons plus expliciter quelles sont les classes implémentant les services définis dans les interfaces (`NotesDAO` et `NotesBusiness`), c'est le framework qui va gérer cela dynamiquement pour nous. De même, nous n'allons plus gérer les transactions dans le _DAO_, c'est _JPA_ qui va le faire pour nous.

Nous verrons comment faire cela, entre autre, dans le prochain TD.
