# Exercice 4 - Utilisation de JPA

Il faut commencer par créer un projet Web Dynamique, appelé **_GestionNotesJPA_**.

## 1. Configuration pour utiliser JPA

Pour pouvoir utiliser _JPA_, il faut l'indiquer dans les "facettes" du projet. Pour cela, il faut faire un clic droit sur le projet **_GestionNotesJPA_**, cliquer sur :material-mouse:`Properties` et aller dans :material-file-tree:`Project Facets`, cocher la case _JPA_ et enfin cliquer sur <bouton>Apply and Close</bouton> :
<center>
    ![Création GestionNotesJPA](../images/GestionNotesJPA.png)
</center>

Dans la vue _Entreprise Explorer_, le fichier 📄`persistence.xml` apparaît. Il va nous permettre de gérer la persistence des _beans_ Java.


## 2. Création du modèle

Nous allons effectuer à nouveau la création du modèle, c'est-à-dire de la classe `NoteBean`, mais nous allons faire autrement. Pour indiquer que nous allons utiliser JPA, il suffit de faire un clic droit sur le projet, puis :material-mouse:`New > JPA Entity`. Indiquer `edu.polytech.note.model` comme package, et `NoteBean` comme classe, puis cliquer sur <bouton><u>N</u>ext ></bouton>.
<center>
    ![Création GestionNotesJPA](../images/GestionNote-JPA-1.png)
</center>
Ajouter ensuite les trois champs listés ci-dessus en utilisant la bouton <bouton><u>A</u>dd...</bouton> (attention à bien cocher le champ `id` comme _key_), puis clique sur <bouton><u>F</u>inish</bouton>
<center>
    ![Création GestionNotesJPA](../images/GestionNote-JPA-2.png)
</center>
Nous allons également préciser la stratégie de gestion du champ `id`. On souhaite qu'il soit automatiquement incrémenté (d'un en un). On ajoute donc, juste en dessous de l'annotation `@id`, l'annotation `@GeneratedValue(strategy = GenerationType.IDENTITY)`. On a donc :
``` java title="☕ Code Java - Entité NoteBean" linenums="1"
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY) //(1)
private Integer id;
```

1. Cela correspond exactement au `auto_increment` que nous avions mis sur le champ `id` de la table `NOTE` dans [l'exercice 1](/ArchitectureSI/TD3/TD3.1.1/#auto-increment) de ce TD.

Dans le fichier 📄`persistence.xml`, la ligne ci-dessous a été automatiquement ajoutée :
``` xml title="📄 persistence.xml" linenums="1"
<class>edu.polytech.note.model.NoteBean</class>
```

Toujours dans ce fichier :

1. Le nom de l'unité de persistance (attribut `name` de la balise `persistence-unit`) est très important, il faut l'indiquer dans les classes _DAO_ pour indiquer sur quelle base de données les requêtes doivent être effectuées.<br>Par défaut, il n'y a qu'une seule unité de persistance, dont le nom est le nom du projet Java (**_GestionNotesJPA_** ici).
2. Nous allons indiquer que nous utilisons l'API JTA pour gérer les transactions. Pour cela, il faut ajouter l'attribut `transaction-type="JTA"` dans la balise `<persistence-unit>`.
3. À l'intérieur de cette balise `<persistence-unit>`, nous allons ajouter la balise fille `<jta-data-source>java:/MySqlGestionNotesJPA</jta-data-source>` pour faire le lien avec la _datasource_ créée précédemment (on indique le nom JNDI).
4. Enfin, on indique la stratégie utilisée au redémarrage du serveur. Ici, nous sommes en mode "développement", nous allons donc supprimer et re-créer la table correspondante à chaque redémarrage. Pour cela, aller dans l'onglet _Schema Generation_, et indiquer _Drop and Create_ dans la propriété _Database action_.

Le fichier doit maintenant ressembler à cela :
``` xml title="📄 persistence.xml" linenums="1"
<?xml version="1.0" encoding="UTF-8"?>
<persistence version="2.2"
	xmlns="http://xmlns.jcp.org/xml/ns/persistence"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence http://xmlns.jcp.org/xml/ns/persistence/persistence_2_2.xsd">
	<persistence-unit name="GestionNotesJPA" transaction-type="JTA"><!--(1)-->
		<jta-data-source>java:/MySqlGestionNotesJPA</jta-data-source><!--(2)-->
		<class>edu.polytech.note.model.NoteBean</class>
		<properties>
			<property
				name="javax.persistence.schema-generation.database.action"
				value="drop-and-create" /><!--(3)-->
		</properties>
	</persistence-unit>
</persistence>
```

1. Dans les `DAOs`, on aura besoin du nom de l'unité de persistance pour savoir sur quelle base de données envoyer les requêtes.<br><br>Plus d'infos sur `transaction-type` [ici](https://stackoverflow.com/questions/17331024/persistence-xml-different-transaction-type-attributes).
2. Ici, il faut indiquer le même nom JNDI que celui précisé à la création de la _Data Source_ précédemment.
3. Cela signifie qu'à chaque démarrage du serveur, les tables correspondantes sont supprimées et recréées. On évitera bien sûr de laisser cette option en environnement de production ...

??? note

    Dans eclipse, il est possible de faire toute ces modifications depuis les onglets `General`, `Connection` et `Schema Generation` de l'éditeur de fichier XML (on n'est pas obligé de modifier la source directement).

Il est maintenant temps de tester. Pour cela, on démarre le serveur (si ce n'est pas déjà fait), puis on publie le projet (**_GestionNotesJPA_**) sur le serveur. Pour cela, on fait un clic droit sur le serveur, :material-mouse:`Add and Remove...`, et on déploie **_GestionNotesJPA_**.

???+ success "Vérification en base de données"

    On vérifie que la table `NoteBean` est bien créée en BDD (et vide), avec les quatre champs indiqués.

## 3. Création de la couche DAO

Il faut repartir de la couche DAO de la première partie. Il y a cependant des modifications à apporter, puisque l'implémentation de cette couche (c'est-à-dire la classe `NotesDAOImpl`) est complètement modifiée. Maintenant,

1. On crée un objet `EntityManager`, avec le nom de l'unité de persistance en paramètre (celle indiquée dans l'attribut `name` de la balise `persistence-unit` dans le 📄`persistence.xml`).
2. Cet objet gère pour nous les requêtes en BDD, avec le langage HQL (pour _Hibernate Query Language_) - appelé également JPAQL - et l'utilisation des méthodes `find`, `persist`, `createQuery`, ...

Voici maintenant à quoi ressemble la classe `NotesDAOImpl` :

``` java title="☕ Code Java - Classe NotesDAOImpl" linenums="1"
public class NotesDAOImpl implements NotesDAO {

    // L'objet EntityManager qui va permettre d'effectuer les requêtes en BDD.
    private EntityManager em;

    public NotesDAOImpl() {
        // Le constructeur public sans argument a pour rôle d'instancier l'objet EntityManager.
        // Pour cela, on utilise une "Factory" qui a besoin du nom de l'unité de persistance.
        // On la retrouve dans le fichier persistence.xml, dans l'attribute "name"
        // de la balise "persistence-unit".
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestionNotesJPA");
        this.em = emf.createEntityManager();
    }

    @Override
    public List<NoteBean> getNotesList() {
        // Exemple de requête HQL (ou JPAQL).
        Query requete = em.createQuery("select n from NoteBean n");
        return requete.getResultList();
    }

    @Override
    public NoteBean getNote(Integer id) {
        // Récupération d'un enregistrement avec sa clef.
        NoteBean note = (NoteBean) em.find(NoteBean.class, id);
        return note;
    }

    @Override
    public void insertNote(NoteBean note) {
        // Insertion d'un enregistrement en BDD.
        em.getTransaction().begin();
        em.persist(note);
        em.getTransaction().commit();
    }
}
```

## 4. Création de la couche métier

Il faut repartir de la couche métier de la première partie. Aucune modification n'est pour le moment à apporter.


## 5. Récupération de la couche présentation

Tout comme pour la couche métier, on recopie la couche contrôleur (la _Servlet_) et la couche vue (la _JSP_), sans aucune modification pour le moment.

???+ success "Vérification"
    Vérifier maintenant que cette nouvelle version de l'application est bien fonctionnelle.
