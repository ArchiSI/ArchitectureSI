# Exercice 2 - Installation de l'environnement de développement

## 1. Création du serveur WildFly

Il faut commencer par installer le serveur WildFly. Pour cela, il faut télécharger la dernière version ici : [https://www.wildfly.org/](https://www.wildfly.org/). Il suffit de cliquer sur **_DOWNLOAD THE ZIP_**, puis de le dézipper dans le dossier 📂`jee`. Dans la suite, on appelera 📂`<WILDFLY_HOME>` le dossier où WildFly a été dézippé (c'est-à-dire 📂`jee/wildfly-x.y.z.Final` normalement).

??? note

    Pour chaque version, WildFly propose deux environnements :

    1. "Jakarta EE 8 Full & Web Distribution", qui implémente Java EE 8 (packages en `javax.*`).
    2. "WildFly Preview EE 9.1 Distribution", qui implémente Jakarta EE 9 (packages en `jakarta.*`).

    On choisit ici d'utiliser le premier, qui implémenente JEE8 (c'est la version téléchargée si on clique directement sur **_DOWNLOAD THE ZIP_**).

Il faut maintenant créer l'**environnement d'éxécution** (_**runtime environment**_ en anglais) correspondant :

1. Aller dans le menu :fontawesome-solid-bars:`Help > Eclipse Marketplace...`. Sélectionner **_Jboss_** dans le champ de recherche, et installer les outils Jboss en cliquant sur <bouton>Install</bouton> :
    <center>
        ![Installation JBOSS 1/3](../images/install_jboss1.png)
    </center>
2. Dans le menu suivant, cliquer sur <bouton><u>C</u>onfirm ></bouton> :
    <center>
        ![Installation JBOSS 2/3](../images/install_jboss2.png)
    </center>
3. Valider l'installation en acceptant et en cliquant sur _Install anyway_.
4. Il est maintenant possible d'ajouter une _runtime_ Jboss. Aller dans :fontawesome-solid-bars:`Window > Preferences` puis :material-file-tree:`Server > Runtime Environments`.
3. Cliquer sur <bouton>Add...</bouton>, puis dans l'arborescence, aller dans :material-file-tree:`JBoss Community > WildFly 24+ Runtime`.
4. Cocher "Create a new local server", puis cliquer sur <bouton><u>N</u>ext ></bouton> (l'instance du serveur va ainsi directement être disponible dans la vue _Servers_ d'eclipse).
5. Dans _Home Directory_, indiquer 📂`<WILDFLY_HOME>` (le dossier ou le ZIP a été dézippé), sélectionner la jdk installée, puis cliquer sur <bouton><u>N</u>ext ></bouton> et enfin sur <bouton><u>F</u>inish</bouton>.

??? note

    Si on a oublié de cocher la case "Create a new local server", il suffit d'aller dans la vue _server_ dans eclipse, et de le créer manuellement, en indiquant la _runtime_ qu'on vient de créer.

???+ note
    Le serveur apparaît maintenant dans la vue _server_. En ouvrant l'arborescence, dans :material-file-tree:`Filesets > Configuration File`, on constate la présence du fichier 📄`standalone.xml`, qui permet de configurer le serveur :
    <center>
        ![Installation JBOSS 3/3](../images/install_jboss3.png)
    </center>
    Par la suite, on aura besoin de modifier ce fichier, il suffira de double cliquer ici pour l'ouvrir dans eclipse.

## 2. Accès à la console d'administration du serveur

On peut maintenant démarrer le serveur depuis eclipse. Attention à bien vérifier que le serveur tomcat est arrêté. Les deux étant accessibles via le même port par dafaut (8080), il y aura un problème si le tomcat est démarré. Une fois le serveur démarré, on accède à sa page d'accueil via [http://localhost:8080](http://localhost:8080). Lorsqu'on clique sur le lien **_Administration Console_**, un nom d'utilisateur et un mot de passe sont demandés. Il n'y en a pas par défaut, il va falloir en créer un. Pour cela, il faut exécuter le script 📄`<WILDFLY_HOME>/bin/add-user.bat` sous windows, ou 📄`<WILDFLY_HOME>/bin/add-user.sh` sous linux. Nous allons créer un _Management User_ (a), `admin/admin`. Pour des raisons de sécurité, cet utilisateur/mot de passe est **fortement** déconseillé, mais ici, il s'agit uniquement de développement, donc nous allons tout de même le faire. Ce script nous demandera de ce fait de confirmer notre choix. Il faut donc entrer, dans l'ordre, `a / admin / a / admin / yes / admin / <vide> / yes`, comme ci-dessous :
<center>
    ![Add user](../images/WildFly-adduser.png)
</center>

On peut maintenant accéder à la console d'administration (même sans redémarrer le serveur).
