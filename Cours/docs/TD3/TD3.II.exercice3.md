# Exercice 3 - Configuration de la connexion à la base de données

Nous allons maintenant configurer la _datasource_. Il faut biensûr pour cela qu'une base de données soit accessible. On supposera ici qu'elle l'est sur `localhost` sur le port `3306`. **On suppose également qu'un schéma _gestion_notes_jpa_ est créé sur cette instance.**

## 1. Configuration du pilote

Il faut commencer par installer le pilote MySQL. Par défaut, il n'y a que le pilote h2. Pour cela, dans le dossier 📂`<WILDFLY_HOME>/modules/system/layers/base/com`, créer un dossier 📂`mysql` puis un dossier 📂`main` dans celui-ci. Il y a deux choses à mettre dans ce dossier :

1. **Le connecteur java-mysql (jar)**
    Il est disponible [ici](https://dev.mysql.com/downloads/connector/j/) [(https://dev.mysql.com/downloads/connector/j/)](https://dev.mysql.com/downloads/connector/j/). À ce lien, sélectionner "_Plateform Independent_", puis télécharger le zip (il n'est pas nécessaire de se connecter). Dans ce zip, seul le fichier 📄`mysql-connector-java-a.b.c.jar` nous interesse, et doit être placé dans 📂`<WILDFLY_HOME>/modules/system/layers/base/com/mysql/main`.
2. **Le fichier `module.xml`**
    Créer ce fichier dans le dossier, en recopiant le contenu ci-dessous dans son contenu (:warning:**attention à adapter la version du jar**) :
    ``` xml title="📄 module.xml" linenums="1"
    <module xmlns="urn:jboss:module:1.5" name="com.mysql">
        <resources>
            <resource-root path="mysql-connector-java-8.0.28.jar" /><!--(1)-->
        </resources>
        <dependencies>
            <module name="javax.api"/>
            <module name="javax.transaction.api"/>
        </dependencies>
    </module>
    ```

    1. Il faut indiquer ici le même nom que celui du jar présent dans ce dossier.

Enfin, ouvrir le fichier 📄`standalone.xml` (il est accessible depuis la vue _server_, dans :material-file-tree:`Filesets > Configuration File`). Dans ce fichier, chercher la balise `<drivers>`. Elle contient uniquement le driver `h2` pour le moment. Nous allons ajouter notre driver `MySQL`. Pour cela, recopier le code suivant :
``` xml title="📄 standalone.xml" linenums="1"
<driver name="mysql" module="com.mysql"/>
```

Le serveur doit maintenant être redémarré. Dans la console d'administration, vérifier que le pilote a bien été installé en allant dans :fontawesome-solid-bars:`Configuration > Subsystems > Datasources & Drivers > JDBC Drivers`. Il doit maintenant y avoir les deux pilotes : `h2` et `mysql`.

## 2. Configuration de la source de données

Nous pouvons maintenant crééer la _datasource_. Dans la console d'administration, aller dans :fontawesome-solid-bars:`Configuration > Subsystems > Datasources & Drivers > Datasources`, et cliquer sur "Add Datasource". Il y a 6 écran de configuration :

1. Sélectionner "MySQL".
2. Indiquer `MySqlGestionNotesJPA` comme Name et `java:/MySqlGestionNotesJPA` comme JNDI Name.
3. Ne rien changer.
4. Indiquer `jdbc:mysql://localhost:3306/gestion_notes_JPA` dans _Connection URL_ (il faut adapter cette URL si le port est différent, et si le schéma créé sur la BDD n'est pas `gestion_notes_JPA`), et saisir le _User Name_ et l'éventuel _Password_.
5. Tester la connexion.
6. Valider la création de la _datasource_.

???+ note

    Ce que nous venons de faire remplace la classe `DBConnection` du projet `GestionNotes`.

