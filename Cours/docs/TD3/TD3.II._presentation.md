# TD 3 - Partie 2 - Utilisation de JPA

Nous allons ici reprendre l'application de gestion des notes développée dans la première partie, mais nous allons complétement changer la couche d'accès aux données. Nous n'allons plus faire cela "_à la main_", en écrivant les requêtes SQL et en faisant le _mapping_ entre le `ResultSet` et notre modèle (les _beans_).

Nous allons utiliser _JPA_ (pour _Java Persistence API_). Cela permet (entre autre) de s'abstraire de la base de données. Les requêtes écrites dans la partie 1 sont en SQL. Si on se connecte à une base de données où le langage est différent (par exemple des fichiers XML), il faut tout refaire ...

Pour utiliser JPA, il va nous falloir un serveur d'application JEE. Tomcat n'en est pas un, c'est uniquement un serveur Web, contenant un **conteneur de _servlet_**. Nous allons pour cela utiliser **WildFly**.

??? note

    WildFly est le nouveau nom de JBoss depuis la version 8.<br><br>
    Il existe de nombreux serveurs d'application, qui offre les mêmes fonctionnalités, Weblogic d'Oracle, Websphere d'IBM, Glassfish, TomEE d'Apache ...

Le but de cette deuxième partie est d'implémenter la 2ème architecture vue en cours, présentée entre les pages 64 et 71 du cours (CM2) disponible sur Celene, à savoir :

<figure markdown>
  ![Architecture TD3 - Exercice 2](../images/schema_archi_V2.png)
  <figcaption>Schéma de l'architechture de la partie 2 (JPA)</figcaption>
</figure>
