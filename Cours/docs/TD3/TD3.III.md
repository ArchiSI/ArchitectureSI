# Pour les plus rapides

Voici quelques idées d'amélioration de l'application :

1. Mettre en place la possibilité de modifier une note, en cliquant dessus dans la liste.
2. Mettre en place la possibilité de supprimer une note, en cliquant dessus dans la liste.
3. Il serait préférable d'avoir d'avoir une table contenant uniquement la liste des étudiants, d'avoir une table listant les promotions, d'avoir une table listant les matières, et d'avoir une table ne contenant que les notes. Faire un MCD et un MPD de ce modèle.
4. Modifier l'application pour qu'on puisse créer des évaluations (une matière et une date), et ajouter des notes sur différentes évaluations ...
