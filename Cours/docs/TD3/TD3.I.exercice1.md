# Exercice 1 - Utilisation de JDBC


## 1. En base de données

Nous allons créer un schéma et une table sur celui-ci. Il suffit pour cela d'exécuter le script suivant sur la base de données :
<a name="auto-increment"></a>

``` sql
create database gestion_notes;

CREATE TABLE `gestion_notes`.`note` (
  `id` int NOT NULL auto_increment, /*(1)*/
  `nom` varchar(45) DEFAULT NULL,
  `prenom` varchar(45) DEFAULT NULL,
  `note` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
```

1. Permet d'auto-incrémenter le champ à chaque nouvelle insertion en BDD.

## 2. Création de la _data source_

Tomcat est un serveur très léger, qui par défaut, contient uniquement un conteneur de _Servlet_. Pour nous connecter à une base de données, il faut donc ajouter un _JAR_ prévu à cet effet. Il est disponible [ici](https://dev.mysql.com/downloads/connector/j/) [(https://dev.mysql.com/downloads/connector/j/)](https://dev.mysql.com/downloads/connector/j/). Sur cette page, sélectionner "_Plateform Independent_", puis télécharger le zip (il n'est pas nécessaire de se connecter). Dans ce zip, seul le fichier 📄`mysql-connector-java-a.b.c.jar` nous interesse. Il faut le déposer dans le dossier 📂`webapp/WEB-INF/lib` du projet Web dynamique (on pourra appeler ce projet **_GestionNotes_**).

Ce JAR est nécessaire uniquement à l'exécution, pas à la compilation. Il est donc inutile de l'ajouter dans le _classpath_ du projet.

## 3. Couche "Modèle"

Créer le _bean_ représentant la couche "modèle" de notre application, c'est-à-dire permettant de faire le lien avec la table `NOTE` en base de données.

## 4. Couche "Accès aux données"

Nous allons créer une classe contenant les paramètres de connexion à la BDD. Dans la classe suivante, il faut adapter vos paramètres (lorsqu'on utilise _phpMyAdmin_, il n'y a pas de mot de passe par défaut par exemple).

``` java title="☕ Code Java - Classe edu.polytech.note.dao.DBConnection"
package edu.polytech.note.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	private static final String URL = "jdbc:mysql://localhost:3306/gestion_notes";

	private static final String LOGIN = "root";

	private static final String PASSWORD = "admin";

	/**
	 * 
	 * @return L'object {@link Connection} permettant de se connecter à la BDD.
	 * @throws ClassNotFoundException S'il y a un problème au chargement du pilote
	 *                                com.mysql.cj.jdbc.Driver.
	 * @throws SQLException           S'il y a un problème à l'établissement de la
	 *                                connexion.
	 */
	public static Connection createDBConnexion() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		final Connection con = DriverManager.getConnection(URL, LOGIN, PASSWORD);
		return con;
	}
}
```

Nous allons ensuite pouvoir créer une interface (`edu.polytech.note.dao.NotesDAO`) et une classe (`edu.polytech.note.dao.NotesDAOImpl`) [DAO](https://fr.wikipedia.org/wiki/Objet_d%27acc%C3%A8s_aux_donn%C3%A9es) - **qui implémente cette interface** - pour _Data Access Object_, qui proposera à terme les services suivants :

1. La liste de toutes les notes (`SELECT * from note;`).
2. Une seule note avec son identifiant (`SELECT * from note where id = ?;`)
3. L'insertion d'une note (`INSERT INTO NOTE (id, nom, prenom, note) values (?, ?, ?, ?);`).
4. La mise à jour d'une note (`UPDATE NOTE set nom=?, prenom=?, note=? where id=?;`)

Chacun de ces services est implémenté par une méthode Java. Voici le début :

``` java title="☕ Code Java - Classe edu.polytech.note.dao.NotesDAOImpl"
public class NotesDAOIml implements NotesDAO {

	public List<NoteBean> getNotesList() {
		Statement st = null;
		ResultSet rs = null;
		Connection connection = null;
		List<NoteBean> notesList = null;

		try {
			// Lecture de la table NOTE :
			final String sql = "SELECT * from note;";
			connection = DBConnection.createDBConnexion();
			st = connection.createStatement();
			rs = st.executeQuery(sql);
			notesList = mapResultSetToList(rs);
		} catch (Exception e) {
			// S'il y a eu un problème, on le fait remonter.
			throw new RuntimeException(e);
		} finally {
			try {
				// Dans tous les cas, on ferme tout ce qui doit l'être.
				st.close();
				connection.close();
			} catch (Exception e) {
				// S'il y a eu un problème, on le fait remonter.
				throw new RuntimeException(e);
			}
		}
		return notesList;
	}

	public NoteBean getNote(Integer id) {
		PreparedStatement st = null;
		ResultSet rs = null;
		Connection connection = null;
		List<NoteBean> notesList = null;

		try {
			// Lecture de la table NOTE :
			final String sql = "SELECT * from note where id = ?;";
			connection = DBConnection.createDBConnexion();
			// On utilise ici un PreparedStatement plutôt qu'un Statement pour des raisons de sécurités.
			st = connection.prepareStatement(sql);
			st.setInt(1, id);
			rs = st.executeQuery();
			notesList = mapResultSetToList(rs);
		} catch (Exception e) {
			// S'il y a eu un problème, on le fait remonter.
			throw new RuntimeException(e);
		} finally {
			try {
				// Dans tous les cas, on ferme tout ce qui doit l'être.
				st.close();
				connection.close();
			} catch (Exception e) {
				// S'il y a eu un problème, on le fait remonter.
				throw new RuntimeException(e);
			}
		}
		if (notesList.size() != 1) {
			// Si on n'a pas récupéré exactement 1 seul enregistrement, c'est qu'il y a eu un problème.
			throw new RuntimeException("Problème à la récupération de l'enregistrement " + id);
		}
		return notesList.get(0);
	}

	public void insertNote(NoteBean note) {
		...
		// On utilisera ici des PreparedStatement plutôt que des Statement pour des raisons de sécurités.
	}

	public void updateNote(NoteBean note) {
		...
		// On utilisera ici des PreparedStatement plutôt que des Statement pour des raisons de sécurités.
		// Cette méthode n'est à implémenter que dans un second temps, lorsque ce TD est terminé.
	}

	private final List<NoteBean> mapResultSetToList(final ResultSet rs) throws SQLException {
		List<NoteBean> notesList = new ArrayList<NoteBean>();
		while (rs.next()) {
			// Pour chaque ligne de la table,
			// on instancie un nouveau NotesBean.
			final NoteBean noteBean = new NoteBean();
			noteBean.setId(rs.getInt("id")); // Il faut indiquer le nom du champ en BDD, ici, 'id'.
			noteBean.setName(...);
			...
			...
			// On ajoute ce bean à la liste des résultats.
			notesList.add(noteBean);
		}
		return notesList;
	}
```

Une requête en BDD de type `SELECT` renvoie un objet `ResultSet`. Cette objet doit être parcouru, et chaque ligne qu'il contient doit être transformée en `NoteBean`. C'est l'utilité de la méthode `mapResultSetToList`.

## 5. Couche "Services métiers"

Il faut créer une interface (`edu.polytech.note.business.NotesBusiness`) et une classe (`edu.polytech.note.business.NotesBusinessImpl`)  - **qui implémente cette interface** - permettant de définir les services métier.

``` java title="☕ Code Java - Classe edu.polytech.note.business.NotesBusinessImpl"
public class NotesBusinessImpl implements NotesBusiness {

    private NotesDAO dao;

    public NotesBusinessImpl() {
        this.dao = new NotesDAOImpl();
    }

    @Override
    public List<NoteBean> getNotesList() {
        return dao.getNotesList();
    }

    @Override
    public NoteBean getNote(Integer id) {
        return dao.getNote(id);
    }

    @Override
    public void insertNote(NoteBean note) {
        dao.insertNote(note);
    }

    @Override
    public void updateNote(NoteBean note) {
        dao.updateNote(note);
    }

    @Override
    public Double computeMean(List<NoteBean> notesList) {
        ...
    }
}
```


## 6. Couche présentation - implémentation de la vue et du contrôleur

Nous avons implémenté le modèle, il reste à implémenter le contrôleur (ce sont les différentes _Servlets_) et la vue (ce sont les 3 _JSPs_).

Depuis le contrôleur, on peut appeler les services métier. Par exemple, pour récupèrer la liste des notes présentes en BDD.

``` java title="☕ Code Java - Dans les Servlets"
NotesBusiness business = new NotesBusinessImpl();
List<NoteBean> notesList = business.getNotesList();
```

??? note

	Il est préférable que l'instanctiation de la classe permettant l'appel à la couche métier, c'est-à-dire `NotesBusiness business = new NotesBusinessImpl();` soit faite dans la méthode `init` de la _Servlet_. En effet, une seule instance de cette classe est suffisante. On a donc :

    ``` java title="☕ Code Java - Dans les Servlets"
    private NotesBusiness business;

    @Override
    public void init() throws ServletException {
        this.business = new NotesBusinessImpl();
    }
    ```

Depuis la vue, il faudra utiliser la _JSTL_ (pour _Java Standard Tag Library_) pour utiliser des boucles "pour" et des instructions conditionnelles "if".

??? note "Utilisation de la _JSTL_"

	Pour cela, il faut ajouter la directive suivante dans les _JSP_ utilisant la _JSTL_ :

	``` jsp
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><!--(1)-->
	```

	1. On utilise les balises `c:forEach`, `c:if`, `c:out`, ... car le préfixe indiqué à l'import de la bibliothèqe est `c`. C'est la valeur "classique" de ce préfixe.

	Enfin, lorsqu'on utilise un serveur Tomcat (ce n'est pas le cas avec un serveur WildFly), il faut ajouter les deux JARs 📄`jakarta.servlet.jsp.jstl-1.2.6.jar` et 📄`jakarta.servlet.jsp.jstl-api-1.2.7.jar` (disponibles sur Celene) dans le dossier 📂`webapp/WEB-INF/lib`.


???+ success "Vérification"
    Vérifier maintenant que l'application est bien fonctionnelle.
